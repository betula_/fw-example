#!/usr/bin/php
<?php
if (!isset($_SERVER['argv'][1])) {
    die('Missing site name.'.PHP_EOL);
}

$init = __DIR__.'/../site/'.$_SERVER['argv'][1].'/init.php';

if (!file_exists($init)) {
    die('Missing init file for site '.$_SERVER['argv'][1].PHP_EOL);
}

require $init;

if (!isset($_SERVER['argv'][2])) {
    Fw\Fw\Env::$app->console->launch();
} else {
    $command = implode(' ',array_slice($_SERVER['argv'], 2));
    echo Fw\Fw\Env::$app->console->exec($command).PHP_EOL;
}
