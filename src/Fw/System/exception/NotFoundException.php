<?php
namespace Fw\System;
use Exception;
class NotFoundException extends GeneralException {

    public function __construct($message='Not Found', $code=404, Exception $prev=null) {
        parent::__construct($message, (int)$code, $prev);
    }

}