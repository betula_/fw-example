<?php
namespace Fw\System;
use Exception;

class ApiLazyLoaderUndefinedException extends GeneralException {
    public function __construct($name, $code=500, Exception $prev=null) {
        parent::__construct("Undefined lazy loader: {$name}", (int)$code, $prev);
    }
}
