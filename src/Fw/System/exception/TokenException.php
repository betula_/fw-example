<?php
namespace Fw\System;
use Exception;

class TokenException extends GeneralException {

    public function __construct($message, $code=403, Exception $prev=null) {
        parent::__construct("Token error: {$message})", $code, $prev);
    }

}