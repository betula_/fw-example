<?php
namespace Fw\System;
use Exception;

class RedirectException extends GeneralException {

    public function __construct($url, $code = 301, Exception $prev=null) {
        parent::__construct($url, (int)$code, $prev);
    }

}
