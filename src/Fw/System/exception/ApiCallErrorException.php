<?php
namespace Fw\System;
use Exception;

class ApiCallErrorException extends GeneralException {

    protected $call;

    public function __construct(ApiCall $call, $message, $code=500, Exception $prev=null) {
        $this->call = $call;
        parent::__construct($message, (int)$code, $prev);
    }

    public function getCall() {
        return $this->call;
    }
}

