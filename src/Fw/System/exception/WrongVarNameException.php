<?php
namespace Fw\System;
use Exception;

class WrongVarNameException extends GeneralException {

    public function __construct($varName, $code=500, Exception $prev=null) {
        parent::__construct('Wrong variable name: '.$varName, (int)$code, $prev);
    }

}
