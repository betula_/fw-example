<?php
namespace Fw\System;
use Exception;

class ApiCallNotFoundException extends GeneralException {
    public function __construct($callName, $code=404, Exception $prev=null) {
        parent::__construct('Api call not found: '.$callName, (int)$code, $prev);
    }
}
