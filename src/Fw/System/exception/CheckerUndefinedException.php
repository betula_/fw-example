<?php
namespace Fw\System;
use Exception;

class CheckerUndefinedException extends GeneralException {

    public function __construct($checkerName, $code=500, Exception $prev=null) {
        parent::__construct('Unknown checker name: '.$checkerName, (int)$code, $prev);
    }

}
