<?php
namespace Fw\System;
use Exception;

class ApiCallInvalidException extends GeneralException {

    protected $invalidData;
    protected $call;

    public function __construct(ApiCall $call, array $invalidData, $code=500, Exception $prev=null) {
        $this->call = $call;
        $this->invalidData = $invalidData;
        parent::__construct('Invalid args in "'.$call->name.'": '.json_encode($this->invalidData), (int)$code, $prev);
    }

    public function getCall() {
        return $this->call;
    }

    public function getInvalidData() {
        return $this->invalidData;
    }
}

