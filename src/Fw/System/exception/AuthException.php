<?php
namespace Fw\System;
use Exception;

class AuthException extends GeneralException {

    public function __construct($message, $code=403, Exception $prev=null) {
        parent::__construct("Auth failed: {$message}", $code, $prev);
    }

}