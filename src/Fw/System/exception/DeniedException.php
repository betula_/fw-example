<?php
namespace Fw\System;
use Exception;
class DeniedException extends GeneralException {

    public function __construct($message='Access denied', $code=403, Exception $prev=null) {
        parent::__construct($message, (int)$code, $prev);
    }

}