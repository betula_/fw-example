<?php
namespace Fw\System;
use Fw\Fw\Env;
use Exception;

class ApiCall {

    /**
     * @var \Fw\System\Api
     */
    protected $api;

    public $name;

    public function __construct($callName, Api $api) {
        $this->api = $api;
        $this->name = $callName;
    }

    protected function makeCacheKey($key) {
        return get_class($this).':'.$key;
    }

    protected function cacheGet($key) {
        return $this->api->cache->get($this->makeCacheKey($key));
    }

    protected function cacheSet($key, $value, $ttl = null) {
        $this->api->cache->set($this->makeCacheKey($key), $value, $ttl);
    }

    public function args() {
        return [];
    }

    public function perform($args) {
        return null;
    }

    public function exec($args) {
        $errors = [];
        $timeStart = microtime(true);
        if (Env::$app->validator->validate($args, $errors, ['dict' => $this->args()])) {
            trigger_error('Invalid api call '.get_class($this).': '.json_encode($errors));
            throw new ApiCallInvalidException($this, $errors);
        }
        $timeValidate = microtime(true);
        try {
            $result = $this->perform($args);
        } catch (Exception $e) {
            $timePerform = microtime(true);
            Env::$app->logger->warn(sprintf(
                "[%6.1f%6.1f] %s->%s %s %s %s",
                ($timeValidate-$timeStart)*1000,
                ($timePerform-$timeValidate)*1000,
                $this->api->namespace,
                $this->name,
                get_class($e),
                $e->getMessage(),
                $this->argsToString($args)
            ));
            throw $e;
        }
        $timePerform = microtime(true);
        Env::$app->logger->info(sprintf(
            "[%6.1f%6.1f] %s <= %s->%s %s",
            ($timeValidate-$timeStart)*1000,
            ($timePerform-$timeValidate)*1000,
            is_array($result)? count($result):gettype($result),
            $this->api->namespace,
            $this->name,
            $this->argsToString($args)
        ));
        return $result;
    }

    protected function argsToString($args) {
        $result = [];
        foreach ($args as $k=>$v) {
            switch (gettype($v)) {
                case 'integer':
                case 'double' :
                    $result[$k] = "$k:$v";
                break;
                case 'string':
                    $result[$k] = $k.':"'.mb_substr($v, 0, 20).'"';
                    if (mb_strlen($v) > 19) $result[$k] .= ':'.mb_strlen($v).'';
                break;
                case 'array':
                    if (count($v)>5) {
                        $result[$k] = $k.':['.count($v).']';
                    } else {
                        $result[$k] = $k.':['.$this->argsToString($v).']';
                    }
                break;
                case 'object':
                    $result[$k] = $k.':'.get_class($v);
                break;
                case 'boolean':
                    $result[$k] = $k.':'.($v?'true':'false');
                break;
                default:
                    $result[$k] = gettype($v);
            }
        }
        return implode(', ', $result);
    }

}
