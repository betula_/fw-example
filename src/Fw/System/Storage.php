<?php
namespace Fw\System;
use Fw\Fw\Env;

class Storage  {

    public $dir;

    public function __construct() {
        $this->dir = Env::$app->dir['data'].'/var';
    }

    public function filename($target) {
        $hash = hash('crc32b', $target);
        return implode('/',array_reverse(str_split($hash, 2))).'/'.$target;
    }

    public function path($target, $prepare = false) {
        $path = $this->dir.'/'.$this->filename($target);
        if ($prepare) {
            mkdir(dirname($path), 0755, true);
            touch($path);
        }
        return $path;
    }

    public function put($src, $target) {
        $dst = $this->path($target, true);
        return copy($src, $dst);
    }

    public function get() {

    }

}
