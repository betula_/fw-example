<?php
namespace Fw\System;
use Fw\Fw\Env;
{
    class Console {

        /**
         * @var \Closure[] Command functions (receives regexp; do something; returns string result)
         */
        public $commands = array();

        /**
         * @var \Closure[] Test functions (receives $string, $index, $input; returns array of strings)
         */
        public $completions = array();

        public function __construct() {
            $fileName = Env::$app->dir['var'].'/consoleHistory.txt';
            readline_read_history($fileName);
            readline_completion_function([$this, 'completionCallback']);

            $this->completions['exit'] = function($string) {
                if (substr('exit', 0, strlen($string)) === $string) return ['exit'];
            };
            $this->completions['quit'] = function($string) {
                if (substr('quit', 0, strlen($string)) === $string) return ['quit'];
            };
            $this->completions['build'] = function() {
                return ['build'];
            };
            $this->commands['~^exit$~'] = function() {
                return 'May The Force be with you!';
            };
            $this->commands['~^quit$~'] = function() {
                return 'May The Force be with you!';
            };
            $this->commands['~^build~'] = function() {
                Env::$app->trigger('build');
                return 'Build event launched, see log.';
            };
        }

        public function launch() {
            while ($command = $this->read()) {
                $result = $this->exec($command);
                echo $result.PHP_EOL;
                if ($command==='exit' || $command==='quit') break;
            }
        }

        private function read() {
            return readline(Env::$app->siteName.' ['.Env::$app->vaultName.']> ');
        }

        public function exec($command) {
            Env::$app->logger->info($command);
            readline_add_history($command);
            $fileName = Env::$app->dir['var'].'/consoleHistory.txt';
            readline_write_history($fileName);
            $match = $args = array();
            foreach ($this->commands as $regex => $commandFunction) {
                if (preg_match($regex, $command, $args)) {
                    $match[] = [$commandFunction, $args];
                }
            }
            if (count($match)>=2) return 'Multiply choices.';
            if (count($match)==0) return 'Unknown command.';
            $commandFunction = $match[0][0];
            return $commandFunction($match[0][1]);
        }

        public function completionCallback($input, $index) {
            $info = readline_info();
            $matches = array();
            foreach ($this->completions as $testFunction) {
                $string = substr($info['line_buffer'], 0, $info['end']);
                if (!$string) $string = '';
                if ($m = $testFunction($string, $index, $input)) {
                    $matches = array_merge($matches, $m);
                }
            }
            return $matches?:false;
        }

    }
}