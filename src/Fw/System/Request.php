<?php
namespace Fw\System;
use Fw\Fw\Env;

class Request {

    public $method = 'GET';
    public $uri = '/';
    public $ip = '0.0.0.0';
    public $get = array();
    public $post = array();
    public $cookie = array();
    public $files = array();
    public $protocol = 'HTTP/1.1';
    public $x_authorization = '';
    public $time = 0;
    public $request_uri = '/';
    public $userAgent = '';
    public $referer = '';
    public $lang = array();
    public $host = '';

    /**
     * @static
     * @return Request
     */
    public static function createFromGlobals() {
        $request = new self();
        $request->method = $_SERVER['REQUEST_METHOD'];
        if (isset($_SERVER['DOCUMENT_URI'])) {
            $request->uri = $_SERVER['DOCUMENT_URI'];
        }
        $request->get = $_GET;
        $request->cookie = $_COOKIE;
        if ($request->method === 'POST') {
            $request->post = $_POST;
            $request->files = $_FILES;
        }
        if (isset($_SERVER['HTTP_X_AUTHORIZATION'])) {
            $request->x_authorization = $_SERVER['HTTP_X_AUTHORIZATION'];
        }
        if (isset($_SERVER['REQUEST_TIME_FLOAT'])) {
            $request->time = $_SERVER['REQUEST_TIME_FLOAT'];
        } elseif(isset($_SERVER['REQUEST_TIME'])) {
            $request->time = $_SERVER['REQUEST_TIME'];
        } else {
            $request->time = microtime();
        }
        if (isset($_SERVER['REQUEST_URI'])) {
            $request->request_uri = $_SERVER['REQUEST_URI'];
        }
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $request->ip = $_SERVER['REMOTE_ADDR'];
        }
        if (isset($_SERVER['HTTP_REFERER'])) {
            $request->referer = $_SERVER['HTTP_REFERER'];
        }
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $request->userAgent = $_SERVER['HTTP_USER_AGENT'];
        }
        if (isset($_SERVER['HTTP_HOST'])) {
            $request->host = $_SERVER['HTTP_HOST'];
        }
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $request->lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }
        $request->log();
        return $request;
    }

    public function __construct() {
    }

    public function log() {
        switch ($this->method) {
            case 'GET': $method = "\033[32m{$this->method}\033[0m"; break;
            case 'POST': $method = "\033[33m{$this->method}\033[0m"; break;
            case 'HEAD': $method = "\033[36m{$this->method}\033[0m"; break;
            default: $method = "\033[35m{$this->method}\033[0m";
        }

        Env::$app->logger->info(sprintf("%-4s \033[34m[%6.3f] %s G%sP%sC%sF%s\n%s %s [%s] %s\033[0m",
            $method,
            (microtime(true)-$this->time)*1000,
            $this->uri,
            json_encode($this->get),
            json_encode($this->post),
            json_encode($this->cookie),
            json_encode($this->files),
            $this->ip,
            $this->userAgent,
            $this->referer,
            $this->x_authorization
        ));

        Env::$app->trigger('request', $this);
    }

    public function getCookie($name) {
        return isset($this->cookie[$name])?$this->cookie[$name]:'';
    }

    public function getPost($name, $default='') {
        return isset($this->post[$name])?$this->post[$name]:$default;
    }

    public function getGet($name, $default='') {
        return isset($this->get[$name])?$this->get[$name]:$default;
    }

}
