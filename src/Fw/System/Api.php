<?php
namespace Fw\System;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use Exception;
use Fw\Fw\EventDispatcherTrait;

/**
 * @method lazyLoaders(Api $api)
 */
class Api {
    use EventDispatcherTrait;

    /**
     * @var ApiCall[]
     */
    private $calls = array();

    private $callMap = array();

    public $namespace;

    public $time;

    /**
     * @var array Closure[]
     */
    public $lazyLoaders = array();

    public function __construct($namespace) {
        $this->time = time();
        $this->namespace = $namespace;
        $this->mapFileName = Env::$app->dir['var'].'/apiCallMap_'.str_replace('\\','_',$namespace).'.php';
        $this->callMap = include $this->mapFileName;
        if (!$this->callMap) {
            $this->build();
        }
    }


    public function __get($fieldName) {
        if (!isset($this->lazyLoaders[$fieldName])) {
            throw new ApiLazyLoaderUndefinedException(get_class($this).':'.$fieldName);
        }
        $loader = $this->lazyLoaders[$fieldName];
        $this->$fieldName = $loader($this);
        $this->trigger($fieldName.'Created', $this->$fieldName);
        return $this->$fieldName;
    }


    private function getCallClass($callName) {
        if (!isset($this->callMap[$callName])) {
            throw new ApiCallNotFoundException($callName);
        }
        return $this->callMap[$callName];
    }

    public function build() {
        $this->callMap = array();
        $scanner = new DirectoryScanner();
        foreach (Env::$app->modules as $module) {
            $directory = $module->getDirectory();
            $scanner->setDirectory($directory);
            $namespace = $module->getNamespace();
            $scanner->scan(function($fileName) use ($directory, $namespace) {
                $searchDir = "{$directory}/{$this->namespace}";
                if (substr($fileName, 0, strlen($searchDir))===$searchDir) {
                    $actualName = substr($fileName, strlen($searchDir));
                    preg_match_all('~/([A-Z][A-z\d]*)~', $actualName, $m);
                    $cn = array();
                    foreach ($m[1] as $k=>$v) $cn[$k] = lcfirst($v);
                    $name = implode('_', $cn);
                    $this->callMap[$name] = $namespace.'\\'.$this->namespace.'\\'.implode('\\', $m[1]);
                }
            });
        }
        Env::$app->logger->info("\033[33mSaving: ".$this->mapFileName."\033[0m");
        file_put_contents($this->mapFileName, '<?php return '.var_export($this->callMap, true).';');
    }

    public function __call($callName, array $args=[]) {
        if (!isset($args[0])) {
            $args[0] = [];
        }
        $result = $this->getCall($callName)->exec($args[0]);
        return $result;
    }

    public function exec($callName, array $args=array()) {
        $result = $this->getCall($callName)->exec($args);
        return $result;
    }

    public function getCall($callName) {
        if (!isset($this->calls[$callName])) {
            $className = $this->getCallClass($callName);
            $this->calls[$callName] = new $className($callName, $this);
        }
        return $this->calls[$callName];
    }

    public function getCallList() {
        return array_keys($this->callMap);
    }

    public function getCallArgList($callName) {
        try {
            return $this->getCall($callName)->args();
        } catch (GeneralException $e) {
            return false;
        }
    }

    public function createUid() {
        return Env::$app->createUid();
    }

    public function createTokenHash() {
        return Env::$app->createToken();
    }

    public function registerValidators($validator) {
    }

    public function registerConsoleCommands($commandName, $console) {
        $console->completions[$commandName] = function($string) use ($commandName) {
            if (substr($commandName, 0, strlen($string)) === $string) {
                return [$commandName.' '];
            }

            if (preg_match('~^'.$commandName.' (?<cmd>[^ ]*)$~', $string, $m)) {
                $calls = $this->getCallList();
                foreach ($calls as $i=>$name) $calls[$i] = $name.' ';
                return $calls;
            }

            if (preg_match('~^'.$commandName.' (?<cmd>[^ ]*) ~', $string, $m)) {
                return array_keys(Env::$app->me->getCallArgList($m['cmd']));
            }

            return array();
        };

        $console->commands['~^'.$commandName.' (?<call>[^ ]*)( (?<string>.*))?$~'] = function($match) {
            if (isset($match['string'])) {
                if ($match['string']==='help') {
                    return json_encode($this->getCallArgList($match['call']), JSON_PRETTY_PRINT);
                }
                parse_str($match['string'], $args);
            } else {
                $args = array();
            }
            try {
                ob_start();
                var_dump($this->exec($match['call'], $args));
                return ob_get_clean();
            } catch (Exception $e) {
                $msg = get_class($e).' ['.$e->getCode().']: '.$e->getMessage();
                Env::$app->logger->info($msg,$e->getFile().':'.$e->getLine());
                return $msg;
            }
        };
    }

}