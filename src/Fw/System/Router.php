<?php
namespace Fw\System;
use SplPriorityQueue;
use SplObserver;
use Fw\Fw\Env;

class Router  {

    const PRIORITY_LOW      = 10000;
    const PRIORITY_NORMAL   = 20000;
    const PRIORITY_HIGH     = 30000;
    const PRIORITY_EXTRA    = 40000;

    /**
     * @var SplPriorityQueue
     */
    private $routes;

    /**
     * @var \Closure[]
     */
    public $path = array();

    public function __construct() {
        $this->routes = new SplPriorityQueue();
    }

    public function path($name) {
        if (array_key_exists($name, $this->path)) {
            $args = func_get_args();
            array_shift($args);
            $func = $this->path[$name];
            return call_user_func_array($func, $args);
        } else {
            return '#';
        }
    }

    public function set($name, $priority, $route, $path) {
        $this->path[$name] = $path;
        static $count = 0;
        $priority += ++ $count;
//        $route = function($request) use ($name, $priority, $route) {
//            Env::$app->logger->info("Route check $name $priority");
//            return $route($request);
//        };
        $this->routes->insert($route, $priority);
    }

    public function exec(Request $request) {
        try {
            $skipped = 0;
            foreach ($this->routes as $callback) {
                $response = $callback($request);
                if ($response) {
                    Env::$app->logger->info('Routes skipped: '.$skipped);
                    return $response;
                }
                $skipped ++;
            }
            throw new NotFoundException();

        } catch (NotFoundException $e) {
            return new Response(404, 'Not found.', [], 1);
        } catch (RedirectException $e) {
            return new Response($e->getCode(), '', array('Location'=>$e->getMessage()));
        } catch (GeneralException $e) {
            Env::$app->logger->ex($e);
            return new Response($e->getCode(), 'General error.');
        }
    }

}
