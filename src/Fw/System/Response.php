<?php
namespace Fw\System;
use Fw\Fw\Env;

class Response {

    protected $protocol;
    protected $headers = array();
    public $cache;
    public $status;
    public $content;
    /**
     * http://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html
     */
    public static $statusText = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
    );

    public function __construct($status = 200, $content = '', $headers = array(), $cache=0) {
        $this->content = $content;
        $this->status = (int)$status;
        foreach ($headers as $name=>$value) {
            $this->header($name, $value);
        }
        $this->protocol = isset($_SERVER['SERVER_PROTOCOL'])?$_SERVER['SERVER_PROTOCOL']:'HTTP/1.1';
        $this->cache = (int)$cache;
    }

    public function cookie($name, $value, $expires = null, $path = null, $domain = null, $http=null, $secure=null) {
        $cookie = "{$name}={$value}";
        if (isset($expires)) {
            $cookie .= "; Expires=".date('r', $expires);
        }
        if (!isset($path)) {
            $path = '/';
        }
        $cookie .= "; Path={$path}";
        if (isset($domain)) {
            $cookie .= "; Domain={$domain}";
        }
        if (isset($secure) && $secure) {
            $cookie .= "; Secure";
        }
        if (isset($http) && $http) {
            $cookie .= "; HttpOnly";
        }
        $this->header('Set-Cookie', $cookie, false);
    }

    public function header($name, $value, $replace=true) {
        if ($replace) {
            if (is_array($value)) $this->headers[$name] = $value;
            else $this->headers[$name] = array((string)$value);
        } else {
            if (is_array($value)) foreach ($value as $h) $this->headers[$name][] = $h;
            else $this->headers[$name][] = (string)$value;;
        }
    }

    public static function getStatusText($status) {
        if (isset(self::$statusText[$status])) return self::$statusText[$status];
        return 'Unknown status';
    }

    protected function sendHeaders() {
        $statusText = self::getStatusText($this->status);
        header("{$this->protocol} {$this->status} {$statusText}");
        header('X-Powered-By: GNU.bash.4.1.2');
        foreach($this->headers as $header=>$values) {
            foreach ($values as $value) {
                header($header.': '.$value);
                Env::$app->logger->info(sprintf(
                    "%s: %s",
                    ucfirst($header),
                    $value
                ));
            }
        }
        if ($this->cache > 0) {
            header("Cache-Control: max-age={$this->cache}");
        }
    }

    protected function getContentLength() {
        return mb_strlen($this->content, 'latin1');
    }

    public function send() {
        $length = 0;//$this->getContentLength();
        $this->sendHeaders();
        echo $this->content;
        $color = '0';
        if ($this->status>=200) $color = '32';
        if ($this->status>=400) $color = '33';
        if ($this->status>=500) $color = '31';
        Env::$app->logger->info(sprintf("\033[{$color}m{$this->status}\033[0m ({$length} bytes) {$this->cache} sec"));
    }
}
