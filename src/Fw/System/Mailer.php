<?php
namespace Fw\System;
use Fw\Fw\Env;

class Mailer {

    public function send($to, $subject=null, $text=null, $from=null, $files=null) {

        $unique = strtoupper(uniqid(time()));
        $head = '';
        if(isset($from)) {
            $head = "From: {$from}\n";
        }
        $head .= "X-Mailer: PHPMail Tool\n";
        if(isset($from)) {
            $head .= "Reply-To: {$from}\n";
        }
        $head .= "Mime-Version: 1.0\n";
        $head .= "Content-Type:multipart/mixed;";
        $head .= "boundary=\"----------".$unique."\"\n\n";
        $body = "------------".$unique."\nContent-Type:text/html;charset=utf-8\n";
        $body .= "Content-Transfer-Encoding: 8bit\n\n".(isset($text)?$text:'')."\n\n";
        if(isset($files) && count($files) > 0){
            foreach ($files as $file) {
                $body .= "------------".$unique."\n";
                $body .= "Content-Type: application/octet-stream;";
                $body .= "name=\"".$file['name']."\"\n";
                $body .= "Content-Transfer-Encoding:base64\n";
                $body .= "Content-Disposition:attachment;";
                $body .= "filename=\"".$file['name']."\"\n\n";
                $body .= chunk_split(base64_encode($file['content']))."\n";
            }
        }
        return mail($to, isset($subject)?'=?utf-8?B?'.base64_encode($subject).'?=':'', $body, $head);
    }
}
