<?php
namespace Fw\System;
use Fw\Fw\Env;

class Cache {

    public $prefix;
    public $ttl = 3600; // One hour

    public function __construct() {
        $this->prefix = __CLASS__.':'.Env::$app->dir['root'].':'.Env::$app->siteName;
    }

    public function get($key) {
        $value = apc_fetch($this->prefix.$key, $success);
        if ($success) {
            return $value;
        }
        return null;
    }

    public function set($key, $value, $ttl = null) {
        if (!isset($ttl)) {
            $ttl = $this->ttl;
        }
        return apc_store($this->prefix.$key, $value, $ttl);
    }

}
