<?php
namespace Fw\System;
use Fw\Fw\Env;
use DateTime;
use Exception;
{
    class Validator {

        const FOREACH_VALUE = '*';
        const FOREACH_KEY = '@';
        const KEY_CHARS = 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890_-.';

        /**
         * @var \Closure
         */
        public $checkers = array();

        public function __construct() {
            $this->checkers = array(
                // 'default' => $value // sets default value
                // 'optional' => null // can be not set
                // 'trash' => null // can be invalid, if invalid will unset or set to default
                'lang'=> function(&$value, &$errors, $config=null, $varName=null) {
                    if (!is_array($value)) {
                        $errors[$varName]['lang.type'] = gettype($value);
                        return 1;
                    }
                    $intersection = array_intersect(array_values($value), Env::$app->cfg['langLegal']);
                    if (count($intersection) !== count($value)) {
                        $errors[$varName]['lang'] = null;
                        return 1;
                    }
                    return 0;
                },
                'mixed'=> function(&$value, &$errors, $config=null, $varName=null) {
                    return 0;
                },
                'uid' => function(&$value, &$errors, $config=null, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['uid.type'] = gettype($value);
                        return 1;
                    }
                    if (!preg_match('~^[\da-f]{19}$~', $value)) {
                        $errors[$varName]['uid'] = null;
                        return 1;
                    }
                    return 0;
                },
                'token'=> function(&$value, &$errors, $config=null, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['token.type'] = null;
                        return 1;
                    }
                    if (!preg_match('~^[A-z\d_\-]{44}$~', $value)) {
                        $errors[$varName]['token'] = null;
                        return 1;
                    }
                    return 0;
                },
                'email' => function ($value, &$errors, $params=null, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['email.type'] = gettype($value);
                        return 1;
                    }
                    $errorCount = 0;
                    if (!preg_match('~^[A-z0-9-_]+(\.[A-z0-9-_]+)*@([A-z0-9-_]+\.)*[A-z0-9-_]+$~',$value)) {
                        $errors[$varName]['email.format'] = null;
                        $errorCount++;
                    }
                    return $errorCount;
                },
                'pass' => function (&$value, &$errors, $params=null, $varName=null) { //tested
                    if (!is_string($value) && !is_numeric($value)) {
                        $errors[$varName]['pass.type'] = gettype($value);
                        return 1;
                    }
                    $value = (string)$value;
                    $errorCount = 0;

                    $len = mb_strlen($value);
                    if (!isset($params['length']['min'])) {
                        $params['length']['min'] = 6;
                    }
                    if ($len < $params['length']['min']) {
                        $errors[$varName]['pass.length.min'] = $params['length']['min'];
                        $errorCount++;
                    }
                    return $errorCount;
                },
                'int' => function (&$value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['int.type'] = gettype($value);
                        return 1;
                    }
                    $errorCount = 0;
                    if ((string)(int)$value !== (string)$value) {
                        $errors[$varName]['int.format'] = null;
                        return 1;
                    }
                    $value = (int)$value;
                    if (isset($params)) {
                        if (isset($params['min']) && $value < $params['min']) {
                            $errors[$varName]['int.min'] = $params['min'];
                            $errorCount++;
                        }
                        if (isset($params['max']) && $value > $params['max']) {
                            $errors[$varName]['int.max'] = $params['max'];
                            $errorCount++;
                        }
                    }
                    return $errorCount;
                },
                'float' => function (&$value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['float.type'] = gettype($value);
                        return 1;
                    }
                    $errorCount = 0;
                    if ((string)(float)($value) !== (string)$value) {
                        $errors[$varName]['float.format'] = null;
                        return 1;
                    }
                    $value = (float)$value;
                    if (isset($params)) {
                        if (isset($params['min']) && $value < $params['min']) {
                            $errors[$varName]['float.min'] = $params['min'];
                            $errorCount++;
                        }
                        if (isset($params['max']) && $value > $params['max']) {
                            $errors[$varName]['float.max'] = [$params['max']];
                            $errorCount++;
                        }
                    }
                    return $errorCount;
                },
                'string' => function (&$value, &$errors, $params=null, $varName=null) { //tested
                    if (!is_string($value) && !is_numeric($value)) {
                        $errors[$varName]['string.type'] = gettype($value);
                        return 1;
                    }
                    $value = (string)$value;
                    $errorCount = 0;
                    if (isset($params)) {
                        $len = mb_strlen($value);
                        if (isset($params['length']['min']) && $len < $params['length']['min']) {
                            $errors[$varName]['length.min'] = $params['length']['min'];
                            $errorCount++;
                        }
                        if (isset($params['length']['max']) && $len > $params['length']['max']) {
                            $errors[$varName]['length.max'] = $params['length']['max'];
                            $errorCount++;
                        }
                    }
                    return $errorCount;
                },
                'var' => function ($value, &$errors, $params=null,$varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['var.type'] = gettype($value);
                        return 1;
                    }
                    if (!isset($params)) {
                        $params = [];
                    }
                    if (!isset($params['length']['min'])) {
                        $params['length']['min'] = 1;
                    }
                    if (!isset($params['length']['max'])) {
                        $params['length']['max'] = 255;
                    }
                    if (preg_match("~^[A-z_][A-z0-9_]{{$params['length']['min']},{$params['length']['max']}}$~", $value)) {
                        return 0;
                    }
                    $errors[$varName]['var.format'] = [$params['length']['min'], $params['length']['max']];
                    return 1;
                },
                'natural' => function ($value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['natural.type'] = gettype($value);
                        return 1;
                    }
                    $base = '0123456789abcdefghijklmnopqrstuvwxyz';
                    if (!isset($params)) {
                        $params = array();
                    }
                    if (!isset($params['base'])) {
                        $params['base'] = 10;
                    }
                    if (!isset($params['length']['min'])) {
                        $params['length']['min'] = 1;
                    }
                    if (!isset($params['length']['max'])) {
                        $params['length']['max'] = 255;
                    }
                    if (preg_match('~^['.substr($base,0,$params['base']).']{'.$params['length']['min'].','.$params['length']['max'].'}$~', $value, $m)) {
                        return 0;
                    }
                    $errors[$varName]['natural'] = [$params['base'], $params['length']['min'], $params['length']['max']];
                    return 1;
                },
                'time' => function (&$value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['time.type'] = gettype($value);
                        return 1;
                    }
                    // note this works perfect on 64 bit php version
                    if (is_integer($value)) { // unix time?
                        return 0;
                    }
                    if (is_string($value)) { // unix time or gnu time string
                        if ((string)(int)$value === (string)$value) {
                            $value = (int) $value;
                            return 0;
                        } elseif ($value = strtotime($value)) {
                            return 0;
                        } else {
                            $errors[$varName]['time.format'] = null;
                            return 1;
                        }
                    }
                    $errors[$varName]['time.type'] = null;
                    return 1;
                },
                'uri' => function ($value, &$errors, $params=null, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['uri.type'] = gettype($value);
                        return 1;
                    }
                    $pattern='~^([a-z0-9]+://)?([a-z0-9\-_]+(\.[a-z0-9\-_]+)*|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(:[0-9]+)?(/?|/\S+)$~i';
                    if (!preg_match($pattern, $value)) {
                        $errors[$varName]['uri'] = null;
                        return 1;
                    }
                    return 0;
                },
                'flag' => function (&$value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['flag.type'] = gettype($value);
                        return 1;
                    }
                    $value = $value?1:0;
                    return 0;
                },
                'bool' => function (&$value, &$errors, $params=null, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['bool.type'] = gettype($value);
                        return 1;
                    }
                    $value = $value?true:false;
                    return 0;
                },
                'regexp' => function ($value, &$errors, $params, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['regexp.type'] = gettype($value);
                        return 1;
                    }
                    if (preg_match($params,$value)) {
                        return 0;
                    }
                    $errors[$varName]['regexp'] = $params;
                    return 1;
                },
                'filename' => function ($value, &$errors, $params=null, $varName=null) {
                    if (!is_string($value)) {
                        $errors[$varName]['filename.type'] = gettype($value);
                        return 1;
                    }
                    $path = $params?'/':'';
                    if (preg_match('~^[A-z\d_\-'.$path.'][A-z\d_\-'.$path.'\.]{0,251}$~', $value)) {
                        return 0;
                    }
                    Env::$app->logger->error('ACHTUNG! Dangerous filename occurred: '.$value);
                    $errors[$varName]['filename'] = null;
                    return 1;
                },
                'file' => function (&$value, &$errors, array $params, $varName) {
                    if (!is_array($value)) {
                        $errors[$varName]['file.type'] = gettype($value);
                        return 1;
                    }
                    $ec = 0;
                    foreach (['name','type','tmp_name','size','error'] as $p) {
                        if (!isset($value[$p])) {
                            $errors[$varName]['file.'.$p.'.missing'] = null;
                            $ec ++;
                        }
                    }
                    if ($ec) {
                        return $ec;
                    }
                    switch ($value['error']) {
                        case UPLOAD_ERR_INI_SIZE:
                            $errors[$varName]['file.error.size.server'] = ini_get('upload_max_filesize');
                            return 1;
                        case UPLOAD_ERR_FORM_SIZE:
                            $errors[$varName]['file.error.size.client'] = null;
                            return 1;
                        case UPLOAD_ERR_PARTIAL:
                            $errors[$varName]['file.error.partial'] = null;
                            return 1;
                        case UPLOAD_ERR_NO_FILE:
                            if (array_key_exists('emptyAllow', $params)) {
                                $value = null;
                                return 0;
                            }
                            $errors[$varName]['file.error.missing'] = null;
                            return 1;
                        case UPLOAD_ERR_NO_TMP_DIR:
                            $errors[$varName]['file.error.dir'] = null;
                            return 1;
                        case UPLOAD_ERR_CANT_WRITE:
                            $errors[$varName]['file.error.write'] = null;
                            return 1;
                        case UPLOAD_ERR_OK:
                    }

                    if (isset($params['image'])) {
                        $info = getimagesize($value['tmp_name']);
                        if (!$info) {
                            $errors[$varName]['file.image'] = null;
                            return 1;
                        }
                        if (isset($params['mime'])) {
                            if (!in_array($info['mime'], $params['mime'])) {
                                $errors[$varName]['file.mime'] = $params['mime'];
                                return 1;
                            }
                        }

                        $width = $info[0];
                        $height = $info[1];

                        if (!isset($params['image']['width']['min'])) {
                            $params['image']['width']['min'] = 1;
                        }
                        if ($params['image']['width']['min'] > $width) {
                            $errors[$varName]['file.image.width.min'] = $params['image']['width']['min'];
                            $ec ++;
                        }
                        if (isset($params['image']['width']['max']) && $width > $params['image']['width']['max']) {
                            $errors[$varName]['file.image.width.max'] = $params['image']['width']['max'];
                            $ec ++;
                        }

                        if (!isset($params['image']['height']['min'])) {
                            $params['image']['height']['min'] = 1;
                        }
                        if ($params['image']['height']['min'] > $height) {
                            $errors[$varName]['file.image.height.min'] = $params['image']['height']['min'];
                            $ec ++;
                        }
                        if (isset($params['image']['height']['max']) && $height > $params['image']['height']['max']) {
                            $errors[$varName]['file.image.height.max'] = $params['image']['height']['max'];
                            $ec ++;
                        }

                    } else {
                        if (isset($params['mime'])) {
                            $info = finfo_open(FILEINFO_MIME_TYPE);
                            $mime = finfo_file($info, $value['tmp_name']);
                            finfo_close($info);

                            if (!in_array($mime, $params['mime'])) {
                                $errors[$varName]['file.mime'] = $params['mime'];
                                $ec ++;
                            }
                        }
                    }
                    if (isset($params['size'])) {
                        $size = filesize($value['tmp_name']);
                        if (isset($params['size']['max']) && $params['size']['max'] < $size) {
                            $errors[$varName]['file.size.max'] = $params['size']['max'];
                            $ec ++;
                        }
                        if (isset($params['size']['min']) && $params['size']['min'] > $size) {
                            $errors[$varName]['file.size.min'] = $params['size']['min'];
                            $ec ++;
                        }
                    }

                    return $ec;
                },
                'key' => function ($value, &$errors, array $params, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['key.type'] = gettype($value);
                        return 1;
                    }
                    if (array_key_exists($value, $params)) {
                        return 0;
                    }
                    $errors[$varName]['key'] = implode(', ', array_keys($params));
                    return 1;
                },
                'set' => function(&$value, &$errors, array $params, $varName) {
                    if (!is_array($value)) {
                        $errors[$varName]['set.type'] = gettype($value);
                        return 1;
                    }
                    if (array_diff($value, $params)) {
                        $errors[$varName]['set'] = $params;
                        return 1;
                    }
                    $value = array_unique($value);
                    return 0;
                },
                'enum' => function ($value, &$errors, array $params, $varName=null) {
                    if (!is_scalar($value)) {
                        $errors[$varName]['enum.type'] = gettype($value);
                        return 1;
                    }
                    $flip = array_flip($params);
                    if (array_key_exists($value,$flip)) {
                        return 0;
                    }
                    $errors[$varName]['enum'] = $params;
                    return 1;
                },
                'arr' => function (&$value, &$errors, array $params=[], $varName=null) {
                    if (!is_array($value)) {
                        $errors[$varName]['arr.type'] = gettype($value);
                        return 1;
                    }
                    if (isset($params[self::FOREACH_KEY]) && array_key_exists('unique', $params[self::FOREACH_KEY])) {
                        $value = array_unique($value);
                    }
                    $value = array_values($value);
                    return $this->checkForeachElements($value, $errors, $params, $varName);
                },
                'dict'=> function (&$value, &$errors, array $params=[],$varName=null) {
                    if (!is_array($value)) {
                        $errors[$varName]['dict.type'] = gettype($value);
                        return 1;
                    }
                    $toUnset = array();
                    foreach ($value as $key=>$val) {
                        // key validation
                        $keyCheckers = array();
                        if (isset($params[self::FOREACH_KEY])) {
                            $keyCheckers = array_diff_key($params[self::FOREACH_KEY], [
                                'count' => [
                                    'min' => true,
                                    'max' => true,
                                ],
                            ]);
                        }
                        if ($keyCheckers) {
                            // key validation
                            $count = $this->validate($key, $errors, $keyCheckers, $varName.':key');
                            if ($count) return $count;
                        } else {
                            // default key validation if there are no special key validators
                            if (strspn($key, self::KEY_CHARS)!==strlen($key)) {
                                $errors[$varName]['dict.key'] = null;
                                return 1;
                            }
                        }

                        // unset not permitted arguments if there is no universal validators
                        if (!isset($params[self::FOREACH_VALUE])) {
                            if (!isset($params[$key]) || $key === self::FOREACH_KEY || $key===self::FOREACH_VALUE) {
                                $toUnset[]=$key;
                            }
                        }
                    }
                    if ($toUnset) {
                        Env::$app->logger->info('Unset unexpected keys from "'.$varName.'": '.implode(', ', $toUnset));
                        foreach ($toUnset as $key) {
                            unset($value[$key]);
                        }
                    }
                    $errorCount = 0;
                    // validate optional and default members
                    foreach($params as $key=>$param) {
                        if (!isset($param)) {
                            // cancel values by null value checker
                            continue;
                        }
                        if ($key===self::FOREACH_KEY) {
                            continue;
                        }
                        if ($key===self::FOREACH_VALUE) {
                            continue;
                        }
                        if (!isset($value[$key])) {
                            if (is_array($param) && array_key_exists('optional', $param)) {
                                continue;
                            }
                            if (is_array($param) && array_key_exists('default', $param)) {
                                $value[$key] = $param['default'];
                                continue;
                                // without "continue": validator can normalize default data
                            } else {
                                $varNameKey = $varName?($varName.'.'.$key):$key;
                                $errors[$varNameKey]['dict.required'] = null;
                                $errorCount++;
                                continue;
                            }
                        }
                        $errorCount += $this->validate($value[$key], $errors, $params[$key], isset($varName)?"{$varName}.{$key}":$key);
                    }
                    $errorCount += $this->checkForeachElements($value, $errors, $params, $varName);
                    foreach ($errors as $name=>$errorList) {
                        if (isset($params[$name]) && array_key_exists('trash', $params[$name])) {
                            $errorCount -= count($errorList);
                            unset($errors[$name]);
                            if (array_key_exists('default', $params[$name])) {
                                $value[$name] = $params[$name]['default'];
                            } else {
                                unset($value[$name]);
                            }
                        }
                    }
                    return $errorCount;
                },
            );
        }

        public function validate(&$value, &$errors, array $config=[], $varName=null) {
            $errorCount = 0;
            foreach($config as $checker=>$params) {
                if ($checker==='optional') continue;
                if ($checker==='default') continue;
                if ($checker==='trash') continue;
                //if (isset($config['null']) && is_null($value)) {
                //    continue;
                //}
                if (!isset($this->checkers[$checker])) {
                    throw new CheckerUndefinedException($checker);
                }
                $checker = $this->checkers[$checker];
                $errorCount += $checker($value, $errors, $params, $varName);
            }
            return $errorCount;
        }

        public function checkForeachElements(&$value, &$errors, array $params=[], $varName=null) {
            if (isset($params[self::FOREACH_KEY]['count']['max']) && count($value) > $params[self::FOREACH_KEY]['count']['max']) {
                $errors[$varName]['count.max'] = $params[self::FOREACH_KEY]['count']['max'];
                return 1;
            }
            if (isset($params[self::FOREACH_KEY]['count']['min']) && count($value) < $params[self::FOREACH_KEY]['count']['min']) {
                $errors[$varName]['count.min'] = $params[self::FOREACH_KEY]['count']['min'];
                return 1;
            }
            if (isset($params[self::FOREACH_VALUE])) {
                $i = 0;
                foreach ($value as $key => &$val) {
                    $i += $this->validate($val, $errors, $params[self::FOREACH_VALUE], isset($varName)?"{$varName}.{$key}":$key);
                }
                unset($val);
                return $i;
            }
        }

    }
}