<?php
namespace Fw\System;
use Fw\Fw\Env;
use Fw\Fw\EventDispatcherTrait;
use Exception;

class Auth {
    use EventDispatcherTrait;

    public $prefix;
    public $ttl = 2592000; // One month
    public $token;

    public function __construct() {
        $this->prefix = __CLASS__.':'.Env::$app->dir['root'].':'.Env::$app->siteName;
    }

    protected function createToken() {
        return Env::$app->createToken();
    }

    public function start($token) {
        if (!$token) {
            return false;
        }
        $session = $this->fetch($token);
        if ($session) {
            try {
                $this->trigger('start', $session, $token);
                $this->token = $token;
                return true;
            }
            catch (Exception $e) {
                Env::$app->logger->ex($e);
                $this->stop($token);
            }
        }
        return false;
    }

    public function stop($token = null) {
        if (!isset($token)) $token = $this->token;
        if ($this->delete($token)) {
            $this->trigger('stop', $token);
            $this->token = null;
            return true;
        }
        return false;
    }

    public function store($session, $token = null) {
        if (!isset($token)) $token = $this->createToken();
        if (apc_store($this->prefix.$token, $session, $this->ttl)) {
            return $token;
        } else {
            return null;
        }
    }

    protected function fetch($token) {
        $session = apc_fetch($this->prefix.$token, $success);
        if ($success) {
            return $session;
        }
        return null;
    }

    protected function delete($token) {
        return apc_delete($this->prefix.$token);
    }

}