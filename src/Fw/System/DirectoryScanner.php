<?php
namespace Fw\System;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use RecursiveIteratorIterator;

class DirectoryScanner {

    private $directory;

    public function __construct($directory=null) {
        $this->setDirectory($directory);
    }

    public function setDirectory($directory) {
        $this->directory = $directory;
    }

    public function scan($callback) {
        if (!is_callable($callback)) {
            trigger_error(__CLASS__.'::'.__METHOD__.': unexpected '.gettype($callback).', callable expected');
        }
        $searchIterator = new RecursiveDirectoryIterator($this->directory,
            FilesystemIterator::KEY_AS_PATHNAME|
                FilesystemIterator::FOLLOW_SYMLINKS|
                FilesystemIterator::SKIP_DOTS|
                FilesystemIterator::UNIX_PATHS
        );
        $iterator = new RecursiveIteratorIterator($searchIterator);
        foreach ($iterator as $fileName => $info) {
            $callback($fileName, $info);
        }
    }

}