<?php
namespace Fw\System;
use Fw\Fw\Env;
use Fw\Fw\Module as ModuleBasic;
use Exception;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use Imagick;

class Module extends ModuleBasic{

    public function init() {
        Env::$app->cfg['langLegal'] = ['en', 'ru'];
        Env::$app->cfg['langDefault'] = ['en'];
        Env::$app->cfg['saltImage'] = 'I_†®π';
        Env::$app->cfg['saltFile'] = 'F_ß√∆';

        Env::$app->serviceLoaders['router'] = function() {
            return new Router();
        };
        Env::$app->serviceLoaders['mailer'] = function() {
            return new Mailer();
        };
        Env::$app->serviceLoaders['console'] = function() {
            return new Console();
        };
        Env::$app->serviceLoaders['validator'] = function() {
            return new Validator();
        };
        Env::$app->serviceLoaders['cache'] = function() {
            return new Cache();
        };
        Env::$app->serviceLoaders['storage'] = function() {
            return new Storage();
        };
        Env::$app->serviceLoaders['auth'] = function() {
            return new Auth();
        };

        Env::$app->serviceLoaders['ajax'] = function() {
            return new Api('Ajax');
        };

        Env::$app->bind('build', function(){
            Env::$app->ajax->build();
        });

        Env::$app->bind('consoleCreated', function($event, Console $console) {
            Env::$app->ajax->registerConsoleCommands('ajax', $console);
        });

        Env::$app->bind('routerCreated', function($event, Router $router) {

            $router->set('pageError', Router::PRIORITY_NORMAL, function(Request $request){
                if (!(preg_match('~^/(?<code>\d+).html$~', $request->uri, $m))) {
                    return null;
                }
                $response = new Response(200, 'Error '.$m['code'].': '.Response::getStatusText($m['code']), ['Content-Type'=>'text/plain']);
                $response->cache = 3600;
                return $response;
            },function($code){
                return '/error_page_'.(int)$code.'.html';
            });

            $router->set('ajax', Router::PRIORITY_NORMAL, function(Request $request){
                if (!preg_match('~^/ajax/(?<call>[A-z\d]+)\.json$~', $request->uri, $m)) {
                    return null;
                }
                $args = $request->post;
                unset($args['_files']);
                if ($request->files) {
                    $args['_files'] = $request->files;
                }
                Env::$app->auth->start($request->x_authorization);
                return $this->makeAjaxResponse($m['call'], $args);

            }, function($call){
                return '/ajax/'.$call.'.json';
            });

            $router->set('image', Router::PRIORITY_NORMAL, function(Request $request) {
                if (!preg_match('~^/image/(?<secure>[A-z\d]{40})(?<width>[A-z\d]{6})(?<height>[A-z\d]{6})(?<name>[A-z\d\.]+)$~', $request->uri, $m)) {
                    return null;
                }
                $name = $m['name'];
                $width = base_convert($m['width'],16,10);
                $height = base_convert($m['height'],16,10);
                $secure = hash_hmac('sha1',
                    "$name\n$width\n$height\n",
                    Env::$app->cfg['saltImage']
                );
                if ($secure !== $m['secure']) {
                    $response = new Response(404);
                    return $response;
                }

                $path = Env::$app->storage->path($name);
                $image = new Imagick($path);
                $size = $image->getImageGeometry();
                if (!$width)    $width = $size['width']/$size['height'] * $height;
                if (!$width)    $width = $size['width'];
                if (!$height)   $height = $size['height']/$size['width'] * $width;
                $image->cropThumbnailImage($width, $height);
                $info = getimagesize($path);

                $response = new Response(200, $image, ['Content-Type'=>$info['mime']]);
                $response->cache = 3410000;
                return $response;

            }, function($name, $width=0, $height=0) {
                $secure = hash_hmac('sha1',
                    "$name\n$width\n$height\n",
                    Env::$app->cfg['saltImage']
                );
                $width16 = str_pad(base_convert($width,10,16), 6, '0', STR_PAD_LEFT);
                $height16 = str_pad(base_convert($height,10,16), 6, '0', STR_PAD_LEFT);
                return "/image/{$secure}{$width16}{$height16}{$name}";
            });

            $router->set('file', Router::PRIORITY_NORMAL, function(Request $request) {
                if (!preg_match('~^/file/(?<secure>[A-z\d]{40})(?<name>[A-z\d\.]+)$~', $request->uri, $m)) {
                    return null;
                }
                $name = $m['name'];
                $display = $request->getGet('filename');
                $secure = hash_hmac('sha1',
                    "$name\n$display\n",
                    Env::$app->cfg['saltFile']
                );
                if ($secure !== $m['secure']) {
                    $response = new Response(404);
                    return $response;
                }

                $filename = Env::$app->storage->filename($name);
                $path = Env::$app->storage->path($name);

                $info = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($info, $path);
                finfo_close($info);

                $response = new Response(200, '', [
                    'X-Accel-Redirect' => "/data/var/{$filename}",
                    'Content-Type' => $mime,
                    'Content-Disposition' => sprintf('attachment; filename=%s', $display ? $display : basename($path) ),
                ]);
                $response->cache = 3410000;
                return $response;

            }, function($name, $filename='') {
                $secure = hash_hmac('sha1',
                    "$name\n$filename\n",
                    Env::$app->cfg['saltFile']
                );
                $path = "/file/{$secure}{$name}";
                if ($filename) {
                    $path .= '?'.http_build_query(['filename' => $filename]);
                }
                return $path;
            });

        });
    }

    public function makeAjaxResponse($call, $args) {
        $time = microtime(true);
        try {
            $result = Env::$app->ajax->exec($call, $args);
            $result = array(
                'status'=>'ok',
                'time'=>number_format(microtime(true)-$time, 4, '.', ''),
                'result'=>$result,
            );
        } catch (ApiCallInvalidException $e) {
            $result = array(
                'status'=>'invalid',
                'time'=>number_format(microtime(true)-$time, 4, '.', ''),
                'errors'=>$e->getInvalidData(),
            );
        } catch (DeniedException $e) {
            $result = array(
                'status'=>'denied',
                'time'=>number_format(microtime(true)-$time, 4, '.', ''),
            );
        } catch (Exception $e) {
            $result = array(
                'status'=>'error',
                'time'=>number_format(microtime(true)-$time, 4, '.', ''),
                'error'=>str_replace('\\', ':', get_class($e)),
                'code'=>$e->getCode(),
            );
            Env::$app->logger->ex($e);
        }
        $result = json_encode($result, JSON_PRETTY_PRINT).PHP_EOL;
        Env::$app->logger->info($result);
        return new Response(200, $result, ['Content-Type'=>'application/json']);
    }


    /**
     * @return string This method MUST be override in each module
     */
    public function getDirectory() {
        return __DIR__;
    }

}
