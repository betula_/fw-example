<?php
namespace Fw\C;
use Fw\Fw\Module as ModuleBasic;
use Fw\Fw\Env;
use Fw\System\Request;
use Fw\System\Response;
use Fw\System\Router;
use Exception;
use Fw\System\Console;
use Fw\System\Api;

class Module extends ModuleBasic {

    public function getRelatedModules() {
        return [
            'System\\Module'
        ];
    }

    public function init() {
        Env::$app->cfg['c']['const'] = [];

        Env::$app->serviceLoaders['c'] = function(){
            $c =  new ComponentManager();
            if (isset(Env::$app->cfg['const'])) {
                $c->const = Env::$app->cfg['const'];
            }
            return $c;
        };

        Env::$app->bind('build', function(){
            Env::$app->c->build();
        });


        Env::$app->bind('routerCreated', function($event, Router $router) {
            $router->set('pageError', Router::PRIORITY_NORMAL, function(Request $request){
                if (!(preg_match('~^/(?<code>\d+).html$~', $request->uri, $m))) {
                    return null;
                }
                try {
                    $page = Env::$app->c->createByName('pageErrorHttp', array('code'=>$m['code']));
                    $response = new Response(200, Env::$app->c->render($page));
                } catch (Exception $e) {
                    $response = new Response(200, 'Internal error', array('Content-Type'=>'text/plain'));
                }
                $response->cache = 3600;
                return $response;
            },function($code){
                return '/error_page_'.(int)$code.'.html';
            });

            $router->set('ajaxRefreshBatch', Router::PRIORITY_HIGH, function(Request $request){
                if ($request->uri !== '/ajax/refresh_batch.json') {
                    return null;
                };
                try {
                    $batch = $request->post;
                    Env::$app->auth->start($request->x_authorization);
                    $result = Env::$app->c->componentRefreshBatch($batch);
                    return new Response(200, json_encode($result, JSON_PRETTY_PRINT), ['Content-Type'=>'application/json']);
                } catch (Exception $e) {
                    Env::$app->logger->ex($e);
                    return new Response(400, str_replace('\\', ':', get_class($e)));
                }
            }, function(){
                return '/ajax/refresh_batch.json';
            });

            $router->set('ajaxRefresh', Router::PRIORITY_HIGH, function(Request $request){
                if ($request->uri !== '/ajax/refresh.json') {
                    return null;
                };
                try {
                    $properties = $request->get;
                    Env::$app->auth->start($request->x_authorization);
                    $result = Env::$app->c->componentRefresh($properties);
                    return new Response(200, json_encode($result, JSON_PRETTY_PRINT), ['Content-Type'=>'application/json']);
                } catch (Exception $e) {
                    Env::$app->logger->ex($e);
                    return new Response(400, str_replace('\\', ':', get_class($e)));
                }
            }, function(){
                return '/ajax/refresh.json';
            });

        });

        Env::$app->bind('consoleCreated', function($event, Console $console) {
            $console->completions['component'] = function($string) use ($console) {
                if (substr('component', 0, strlen($string)) === $string) {
                    return ['component '];
                }

                if (preg_match('~^component (?<component>[^ ]*)$~', $string, $m)) {
                    $components = Env::$app->c->getComponentList();
                    foreach ($components as $i=>$name) $components[$i] = $name.' ';
                    return $components;
                }

                if (preg_match('~^component (?<component>[^ ]*) ~', $string, $m)) {
                    return Env::$app->c->getComponentPropertyList($m['component']);
                }
                return false;
            };

            $console->commands['~^component (?<component>[^ ]*)( (?<string>.*))?$~'] = function($match) {
                if (!isset($match['component'])) {
                    return 'Component not specified.';
                }
                if (isset($match['string'])) {
                    parse_str($match['string'], $args);
                } else {
                    $args = array();
                }
                $component = Env::$app->c->createByName($match['component'], $args);
                return $component->renderDump();
            };

        });
    }

    public function getDirectory() {
        return __DIR__;
    }
}