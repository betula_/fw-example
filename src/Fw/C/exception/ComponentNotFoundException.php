<?php
namespace Fw\C;
use Exception;

class ComponentNotFoundException extends ComponentException {
    public function __construct($component, $code=500, Exception $prev=null) {
        parent::__construct('Component not found: '.$component,(int) $code, $prev);
    }
}
