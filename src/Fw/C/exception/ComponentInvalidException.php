<?php
namespace Fw\C;
use Exception;
use Fw\C\Component\Component;

class ComponentInvalidException extends ComponentException {

    protected $invalidData;
    protected $component;

    public function __construct(Component $component, array $invalidData, $code=500, Exception $prev=null) {
        $this->invalidData = $invalidData;
        $this->component = $component;
        parent::__construct('Invalid '.get_class($component).': '.json_encode($this->invalidData), $code, $prev);
    }

    public function getInvalidData() {
        return $this->invalidData;
    }
}

