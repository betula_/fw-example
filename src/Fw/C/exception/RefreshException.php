<?php
namespace Fw\C;
use Exception;

class RefreshException extends ComponentException {
    public function __construct($comment, $code=400, Exception $prev=null) {
        parent::__construct('Component refresh error: '.$comment,(int) $code, $prev);
    }
}
