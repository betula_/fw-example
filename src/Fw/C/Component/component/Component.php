<?php
namespace Fw\C\Component;
use Fw\Fw\Env;
use Fw\System\Response;
use Fw\C\ComponentManager;
use Exception;
use Fw\C\ComponentInvalidException;
use Fw\C\ComponentNotFoundException;

class Component {

    /**
     * @var string Global unique ID of Piece
     */
    public $_id;

    /**
     * @var string Component name
     */
    public $_name;

    /**
     * @var string Template name (component state)
     */
    public $_template = 'tpl';

    /**
     * @var array Piece language array (first element is primary language)
     */
    public $_lang;

    /**
     * @var array Published variable list (from data)
     */
    public $_export;

    /**
     * @var Component[][]
     */
    public $_slot = array();

    public function properties() {
        return [
            '_lang'=>[
                'lang'=>null,
                'default'=>Env::$app->cfg['langDefault'],
            ],
        ];
    }

    public function __construct(array $data = [], $validate=true) {
        $errors = array();
        if ($validate && Env::$app->validator->validate($data, $errors, ['dict'=>$this->properties()])) {
            trigger_error('Invalid '.get_class($this).': '.json_encode($errors));
            throw new ComponentInvalidException($this, $errors);
        }
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
        $this->_name = Env::$app->c->getFrontendNameByClassName(get_class($this));
        $this->_id = Env::$app->createUid();
        $this->_export = ['_id', '_name', '_template', '_lang'];
    }

    public function exec() {

    }

    /**
     * @param string $slot
     * @param string $componentName
     * @param array $data
     * @return Component
     */
    public function insert($slot, $componentName, array $data = []) {
        try {
            return $this->_slot[$slot][] = Env::$app->c->createByName($componentName, $data, $this);
        } catch (ComponentNotFoundException $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return $this->_slot[$slot][] = Env::$app->c->createByName('error', ['code'=>$e->getCode()], $this);
        }
    }

    /**
     * @return string Piece descriptor for logging
     */
    public function getFullName() {
        return "{$this->_name}:{$this->_template}";
    }

    public function renderSlotComponents($slotName) {
        if (!isset($this->_slot[$slotName])) {
            return [];
        }
        $result = [];
        foreach ($this->_slot[$slotName] as $child) {
            $result[] = Env::$app->c->render($child);
        }
        return $result;
    }

    public function renderSlot($slotName) {
        return implode('', $this->renderSlotComponents($slotName));
    }

    public function res($name) {
        return Env::$app->c->getResourceUrl(get_class($this), $this->_template, $name);
    }

    public function c($name) {
        return Env::$app->c->const[$name];
    }


    public function path() {
        return htmlspecialchars(call_user_func_array([Env::$app->router,'path'], func_get_args()));
    }

    /**
     * HTML Escape string variable
     * @param string $var
     * @return string
     */
    private function e($var) {
        return htmlspecialchars($var, ENT_QUOTES|ENT_HTML5, 'UTF-8');
    }

    /**
     * @return string Class for container HTML tag
     */
    public function getCssClass() {
        return $this->_name.'-'.$this->_template;
    }

    public function getLogString() {
        $str = array();
        foreach ($this as $key => $value) {
            if ($key[0]==='_') continue;
            if (is_string($value)) {
                if (mb_strlen($value) > 255) {
                    $value = mb_substr($value, 0, 255).'...['.mb_strlen($value).']';
                }
                $str[] = $key.'="'.$value.'"';
            } elseif (is_numeric($value)) {
                $str[] = $key.'='.$value;
            } elseif (is_array($value)) {
                $str[] = $key.'=array['.count($value).']';
            } elseif (is_null($value)) {
                $str[] = $key.'=null';
            } elseif (is_bool($value)) {
                $str[] = $key.'='.$value?'true':'false';
            } elseif (is_object($value)) {
                $str[] = $key.'=('.get_class($value).')';
            } else {
                $str[] = $key.'='.gettype($value);
            }
        }
        return "{$this->_template}:{$this->_name} ".implode(', ', $str);
    }

    private function getExportData() {
        $result = [];
        foreach ($this->_export as $key) {
            if (isset($this->$key)) {
                $result[$key] = $this->$key;
            }
        }
        return $result;
    }

    public function getExportArray() {
        $array = $this->getExportData();
        if (!empty($this->_slot)) {
            foreach ($this->_slot as $name => $components) {
                foreach ($components as $component) {
                    $array['_slot'][$name][] = $component->getExportArray();
                }
            }
        }
        $array['_refresh'] = ['_name' => true] + array_fill_keys(array_keys($this->properties()), true);
        return $array;
    }

    /**
     * @return string HTML <script></script> tag with init script for including to document head
     */
    public function getInitScriptTag() {
        $json = json_encode($this->getExportArray(), JSON_UNESCAPED_UNICODE /*| JSON_PRETTY_PRINT*/);
        return "<script type=\"text/javascript\">//<![CDATA[\n$(function(){document.manager.register({$json})});\n//]]></script>\n";
    }
}
