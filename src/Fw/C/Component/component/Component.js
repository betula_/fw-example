
this.locked = false;
this.ajax = function(p) {
    if (this.locked) {
        log('LOCKED',this);
    }
    this.locked = true;
    p.context = this;
    p.headers = {
        'X-Authorization': manager.token
    };
    if (p.timeout === undefined) p.timeout = 10000;
    p.type = 'POST';
    p.context = this;
    p.success = function(response) {
        if (p[response.status]) {
            p[response.status].call(this, response);
        }
        this.locked = false;
    }

    var error = p.error;
    p.error = function() {
        if (error) {
            error.apply(this, arguments);
        }
        this.locked = false;
    }

    if (p.ajax) {
        p.ajax.call(this, p);
    } else {
        $.ajax(p);
    }
}

this.refresh = function(){
    var data = {};
    for (var i in this.prop ) {
        if (this.prop['_refresh'][i] !== undefined) {
            data[i] = this.prop[i];
            if (data[i] === false) data[i] = '';
        }
    }
    manager.addRefreshQueue(this.prop._id, data);
}

this.cmp = function(id) {
    var i = manager.instance[id];   // TODO rewrite it immediately
    while (typeof i === 'string') {
        i = manager.instance[i];
    }
    return i;
}

this.children = function(name, slot) {
    var list = [];
    for (var i = 0; i < this.prop._children.length; i++) {
        var c = this.prop._children[i];
        if (slot !== undefined && slot !== c.slot) continue;
        if (name === c.name) {
            list.push(this.cmp(c.id));
        }
    }
    return list;
}

this.prop = {};
for (var i in descriptor) {
    if (i !== '_slot') {
        this.prop[i] = descriptor[i];
    } else {
        var children = [];
        for (var j in descriptor._slot) {
            for (var k = 0; k < descriptor._slot[j].length; k++) {
                var d = descriptor._slot[j][k];
                var p = d._name.lastIndexOf('-');
                var n = d._name.charAt(p+1).toLowerCase() + d._name.substr(p+2);
                children.push({
                    id:d._id,
                    name: n,
                    slot: j
                });
            }
        }
        this.prop._children = children;
    }
}

this.dom = $('#'+this.prop._id, document);

manager.template[descriptor._name][descriptor._template].call(this, descriptor);

if (descriptor._slot !== undefined) {
    for(i in descriptor._slot) {
        for (var j in descriptor._slot[i]) {
            manager.register(descriptor._slot[i][j]);
        }
    }
}

