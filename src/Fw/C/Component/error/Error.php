<?php
namespace Fw\C\Component;
use Fw\Fw\Env;

class Error extends Component {

    public $code;

    public function properties() {
        return parent::properties()+[
            'code' => [
                'int' => null
            ],
        ];
    }

}
