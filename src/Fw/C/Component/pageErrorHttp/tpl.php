<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        body,a {background-color: #00A; font-family: monospace; font-size: 20px; color: white;}
        p.caption {text-align: center; padding-top: 130px;}
        p.caption span {background-color: #AAA;}
        p.line {margin-left: 130px;}
        p.any {text-align: center;}
    </style>
    <title>Error <?=$E($c->code)?></title>
</head>
<body>
    <p class="caption"><span>&nbsp;Error <?=$E($c->code)?>&nbsp;</span></p>
    <p class="line">A non-fatal exception <?=$E($c->code)?> has occured at <?=date('r')?>.
    <br/>The current page can not be displayed.</p>
    <p class="line">
<?if ($c->code==404):?>
        * You opened the link to the page that does not exists.<br/>
        * Please notify source page author that the link is wrong.<br/>
<?elseif ($c->code==503):?>
        * Server at the maintenance, please try again later.<br/>
<?else:?>
        * Please try again later.<br/>
        * Please notify system administrator.<br/>
<?endif;?>
        * Press CTRL+ALT+DEL if you want to restart your computer.<br>
        &nbsp;&nbsp;You will lose any unsaved information in all applications.</p>
    <p class="any">Press any key to continue _</p>
</body>
</html>
