<?php
namespace Fw\C\Component;
{
    class PageErrorHttp extends Component {

        public function properties() {
            return parent::properties()+array(
                'code' => array(
                    'int' => null,
                ),
            );
        }

    }
}
