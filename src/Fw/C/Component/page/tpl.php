<!DOCTYPE html>
<html>
<head>
    <title><?=$E($c->title)?></title>
    <script type="text/javascript" src="/res/jquery.js?<?=$this->version?>"></script>
<?$BLOCK('head')?>
<?$END()?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <link rel="stylesheet" href="/res/var/manager.css?<?=$this->version?>" type="text/css" media="all" />
    <script type="text/javascript" src="/res/var/manager.js?<?=$this->version?>"></script>

    <?=$c->getInitScriptTag()?>
</head>
<body class="<?=$c->getCssClass()?>" id="<?=$c->_id?>">
<?$BLOCK('body')?>
<?$END()?>
</body>
</html>
