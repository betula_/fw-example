<?php
namespace Fw\C\Component;
use Fw\Fw\Env;

class Page extends Component {

    public $title = '';

    public function exec() {
        Env::$app->c->bind('titleChange', function($event, $title){
            $this->title = $title;
        });
    }

}