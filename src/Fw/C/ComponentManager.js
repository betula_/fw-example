document.manager = new (function Manager() {
    'use strict';
    var manager = this;
    this.template = {};
    this.component = {};
    this.instance = {};
    this.refreshQueue = {};
    this.refreshReady = true;

    var log = function(){
        if (manager.getCookie('debug', '0')=='1') {
            console.log.apply(console, arguments);
        }
    };
    var warn = function(){
        if (manager.getCookie('debug', '0')=='1') {
            console.warn.apply(console, arguments);
        }
    };
    var error = function(){
        if (manager.getCookie('debug', '0')=='1') {
            console.error.apply(console, arguments);
        }
    };

    this.getCookie = function(name, def)
    {
        var cookies = (document.cookie || '').split('; ');
        for (var i=0; i < cookies.length; i++) {
            var c = cookies[i].split('=');
            if (c[0] === name) return decodeURIComponent(c[1]);
        }
        return def;
    };

    this.setCookie = function(name, value, expires, path)
    {
        var cookie = name + '=' + encodeURIComponent(value) + '; Expires=' + expires.toGMTString();
        if (path === undefined) path='/';
        cookie += '; Path='+path;
        log('Set cookie', cookie);
        document.cookie = cookie;
    };

    (function(){
        if (manager.getCookie('debug_events', '0')=='1') {
            var original_trigger = jQuery.event.trigger;
            jQuery.event.trigger = function(event, data, elem, onlyHandlers) {
                log('Trigger', typeof event === 'object'?event.type:'', event, data, elem);
                original_trigger.apply(this, arguments);
            }

            var original_add = jQuery.event.add;
            jQuery.event.add = function(elem, types, handler, data ) {
                log('Bind', types, elem);
                original_add.apply(this, arguments);
            }
        }
    })();

    this.token = this.getCookie('token','');
    $(document).on('tokenChange', function(e){
        if (e.expires===undefined || e.token===undefined) {
        } else {
            manager.token = e.token;
            manager.setCookie('token', e.token, e.expires);
        }
    });

    this.register = function(descriptor){
        if (this.component[descriptor._name]===undefined) {
            warn('Undefined component name', descriptor);
        }
        else {
            try {
                var component = new this.component[descriptor._name](descriptor);
                this.instance[descriptor._id] = component;
                return component;
            } catch (e) {
                error(descriptor._name, descriptor._template,  e.message);
            }
        }
    };

    this.addRefreshQueue = function(id, data) {
        manager.refreshReady = true;
        manager.refreshQueue[id] = data;
        setTimeout(function(){
            if (manager.refreshReady) {
                manager.refreshReady = false;
                $.ajax({
                    type: 'POST',
                    headers: {
                        'X-Authorization':manager.token
                    },
                    url: '<?=$this->path("ajaxRefreshBatch")?>',
                    data: manager.refreshQueue,
                    success: function(data) {
                        for (var id in data) {
                            manager.instance[id] = undefined;
                            $('#'+id).replaceWith(data[id].html);
                            manager.register(data[id].descriptor);
                            manager.instance[id] = data[id].descriptor._id;
                        }
                    }
                });
                manager.refreshQueue = {};
            } else {
                warn('Refresh queue not ready');
            }
            manager.refreshReady = false;
        }, 100);
    }

    /*COMPONENTS*/

})();
