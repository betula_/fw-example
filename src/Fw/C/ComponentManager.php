<?php
namespace Fw\C;
use Fw\Fw\Env;
use Exception;
use Fw\C\Component\Component;
use Fw\C\Component\Invalid;
use Fw\C\Component\Error;
use Fw\System\RedirectException;
use Fw\System\NotFoundException;
use Fw\System\DirectoryScanner;
use Fw\System\Request;
use Fw\System\Response;
use Fw\Fw\EventDispatcherTrait;

class ComponentManager {
    use EventDispatcherTrait;

    private $mapFileName;

    public $version = 0;

    private $maps = array();

    public $const = array();

    public function __construct() {
        $this->version = include Env::$app->dir['var'].'/componentManagerCompileVersion.php';
        if (!$this->version) {
            $this->increaseVersion();
        }
        $this->mapFileName = Env::$app->dir['var'].'/componentMap.php';
        $this->maps = include $this->mapFileName;
    }

    public function componentRefresh(array $properties) {
        Env::$app->logger->info(json_encode($properties, JSON_PRETTY_PRINT));
        if (!isset($properties['_name'])) {
            throw new RefreshException('name missing');
        }
        $className = $this->getClassNameByFrontendName($properties['_name']);
        $component = $this->makeComponent($className, $properties);
        return array(
            'html'=>$this->render($component),
            'descriptor'=>$component->getExportArray(),
        );
    }

    public function componentRefreshBatch(array $batch) {
        foreach ($batch as $id=>$properties) {
            $batch[$id] = $this->componentRefresh($properties);
        }
        return $batch;
    }

    public function createByName($componentName, array $properties = [], $validate = true) {
        if (!isset($this->maps['u'][$componentName])) {
            throw new ComponentNotFoundException($componentName);
        }
        return $this->makeComponent($this->maps['u'][$componentName], $properties, $validate);
    }

    public function makeComponent($className, array $properties = [], $validate = true) {
        $time = microtime(true);
        try {
            if (!isset($this->maps['c'][$className])) {
                throw new ComponentNotFoundException($className);
            }
            $result = new $className($properties, $validate);
            $result->exec();
        } catch (NotFoundException $e) {
            throw $e;
        } catch (RedirectException $e) {
            throw $e;
        } catch (ComponentInvalidException $e) {
            Env::$app->logger->warn(get_class($e).' ['.$e->getCode().']: '.$e->getMessage());
            $result = new Invalid(array(
                'errors' => $e->getInvalidData()
            ));
        } catch (Exception $e) {
            Env::$app->logger->ex($e);
            $result = $this->createByName('error', [
                'code' => $e->getCode()
            ]);
        }
        $time = (microtime(true) - $time)*1000;
        Env::$app->logger->info(sprintf("[%6.1f] %s", $time, $result->getLogString()));
        return $result;
    }

    public function increaseVersion() {
        $this->version++;
        Env::$app->logger->warn('Version increased to '.$this->version);
        file_put_contents(Env::$app->dir['var'].'/componentManagerCompileVersion.php', '<?php return '.$this->version.';');
    }

    public function getFrontendNameByClassName($className) {
        if (preg_match('~^Fw-([^\-]+)-Component-(.+)$~', str_replace('\\', '-', $className), $name)) {
            return $name[1].'-'.$name[2];
        } else {
            trigger_error('Wrong component class name: '.$className, E_USER_WARNING);
        }
    }

    public function getClassNameByFrontendName($frontendName) {
        return 'Fw\\'.str_replace('-', '\\', substr_replace($frontendName, '-Component', strpos($frontendName, '-'), 0));
    }

    public function getComponentName(Component $component) {
        $className = get_class($component);
        return $this->getComponentNameByClassName($className);
    }

    public function getComponentNameByClassName($className) {
        return lcfirst(substr($className, strrpos($className, '\\')+1));
    }

    public function render(Component $c) {
        return $this->renderComponent($c);
    }

    public function renderComponent(Component $c) {
        $render = function ($block) use (&$render) {
            if (!$block) {
                return '';
            }
            $result = [];
            foreach ($block['content'] as $i) {
                if (isset($i['text'])) {
                    $result[] = $i['text'];
                }
                elseif (isset($i['block'])) {
                    $result[] = $render($i);
                }
            }
            return implode($result);
        };
        $find = function ($block, $path) {
            $b = $block;
            foreach ($path as $piece) {
                $b = $b['content'][$piece];
            }
            return $b;
        };

        $className = get_class($c);

        $list = [];
        if (isset($this->maps['c'][$className]['parents'])) {
            $list = $this->maps['c'][$className]['parents'];
        }
        array_push($list, $className);

        $buffer = [];

        foreach ($list as $class) {
            $block = $this->renderTemplate($class, $c, false);
            if (!$block) {
                continue;
            }
            if (!$buffer) {
                $buffer = $block;
                continue;
            }
            foreach ($block['map'] as $name => $path) {
                if (isset($buffer['map'][$name])) {

                    $b = $find($block, $path);
                    $p = $find($buffer, $buffer['map'][$name]);

                    // Check and save parent
                    $parent = null;
                    foreach ($b['content'] as $k=>$i) {
                        if (isset($i['parent'])) {
                            if (!isset($parent)) {
                                $parent = $render($p);
                            }
                            $b['content'][$k] = ['text' => $parent];
                        }
                    }
                    $parent = null; // clear parent


                    $link = &$buffer;
                    foreach ($buffer['map'][$name] as $piece) {
                        $link['map'] = array_diff_key($link['map'], $p['map']); // Remove parent child from map
                        foreach($b['map'] as $k=>$m) {
                            if (isset($link['map'][$k])) {
                                trigger_error('Cannot redeclare block '.$k);
                            }
                            $link['map'][$k] = array_merge($p['path'],$m); // Add path to child to new block
                        }
                        $link = &$link['content'][$piece]; // Get block link
                    }

                    // Replace to new block
                    $link['block'] = $b['block'];
                    $link['content'] = $b['content'];
                    $link['map'] = $b['map'];

                    unset ($link);
                }
            }
        }

        return $render($buffer);

    }


    public function renderTemplate($className, Component $c) {

        if (!isset($this->maps['c'][$className])) {
            trigger_error('Map entry missing: '.$className);
            return null;
        }
        if (!isset($this->maps['c'][$className]['tpl'][$c->_template]['php'])) {
            return null;
        }

        $step = 0;
        $steps[$step] = [
            'block' => '',
            'content' => [],
            'path' => [],
            'map' => [],
        ];

        $BLOCK = function($block) use (&$steps, &$step) {
            $steps[$step]['content'][] = ['text' => ob_get_clean()];
            $index = count($steps[$step]['content']);
            $path = $steps[$step]['path'];
            $path[] = $index;
            $b = [
                'block' => $block,
                'content' => [],
                'path' => $path,
                'map' => [],
            ];
            $steps[$step]['content'][] = &$b;
            for ($i=$step; $i>=0; $i--) {
                if (isset($steps[$i]['map'][$block])) {
                    trigger_error('Cannot redeclare block '.$block);
                }
                $steps[$i]['map'][$block] = array_slice($path, $i);
            }
            $steps[++$step] = &$b;
            ob_start();
        };
        $END = function() use (&$steps, &$step) {
            $steps[$step]['content'][] = ['text' => ob_get_clean()];
            $step --;
            ob_start();
        };
        $PARENT = function() use (&$steps, &$step) {
            $steps[$step]['content'][] = ['text' => ob_get_clean()];
            $steps[$step]['content'][] = ['parent' => true];
            ob_start();
        };

        $E = function(&$var, $default='') {
            if (!isset($var)) return $default;
            return htmlspecialchars($var, ENT_QUOTES|ENT_HTML5, 'UTF-8');
        };

        ob_start();

        $box = function($className, $c) use ($BLOCK, $END, $PARENT, $E) {
            include $this->maps['c'][$className]['tpl'][$c->_template]['php'];
        };
        $box($className, $c);

        $steps[$step]['content'][] = ['text' => ob_get_clean()];
        $result = $steps[0];
        $steps = null;
        return $result;
    }

    private function generateCss() {
        $render = function(Component $c, $_css) {
            ob_start();
            include $_css;
            $content = ob_get_clean();
            $selector = '.'.$c->getCssClass();
            $content = preg_replace(
                array('~^\*~ms',        '~\s{2,}~ms',   '~\n~ms',   '~}~ms' ),
                array($selector,        '',             '',         "}\n"   ),
                $content
            );
            return $content;
        };
        $content = [];
        foreach ($this->maps['c'] as $map) {
            $component = $map['instance'];
            if (isset($map['tpl'])) {
                foreach ($map['tpl'] as $tplName => $tpl) {
                    $component->_template = $tplName;
                    $css = [];
                    foreach ($map['parents'] as $pName) {
                        if (isset($this->maps['c'][$pName]['tpl'][$tplName]['css'])) {
                            $css[] = $render($component, $this->maps['c'][$pName]['tpl'][$tplName]['css']);
                        }
                    }
                    if (isset($tpl['css'])) {
                        $css[] = $render($component, $tpl['css']);
                    }
                    $content[] = "/* {$component->_name} {$tplName} */\n".implode("\n", $css);
                }
            }
        }
        $fileName = Env::$app->dir['res'].'/var/manager.css';
        Env::$app->logger->warn("\033[33mSaving ".$fileName."\033[0m");
        $content = implode("\n", $content);
        file_put_contents($fileName, $content);
    }

    private function generateJs() {
        $render = function(Component $c, $_js) {
            ob_start();
            include $_js;
            $content = ob_get_clean();
            return $content;
        };
        $content = [];
        foreach ($this->maps['c'] as $map) {
            $component = $map['instance'];
            $name = $component->_name;
            $block = '';
            // javascript inheritance
            foreach ($map['inheritance'] as $pName) {
                if (isset($this->maps['c'][$pName]['js'])) {
                    $parent = $this->maps['c'][$pName]['instance']->_name;
                    $block .= "manager.component['$parent'].call(this, descriptor);\n    ";
                    break;
                }
            }
            // generate component script
            if (isset($map['js'])) {
                $block .= $render($component, $map['js']);
            }
            $js = "this.component['$name'] = function(descriptor){\n    ";
            $js .= str_replace("\n", "\n    ", $block);
            $js .= "\n};\n";
            // generate template script
            $js .= "this.template['$name'] = {};\n";
            if (isset($map['tpl'])) {
                foreach ($map['tpl'] as $tplName => $tpl) {
                    $component->_template = $tplName;

                    $block = '';
                    foreach ($map['inheritance'] as $pName) {
                        if (isset($this->maps['c'][$pName]['tpl'][$tplName]['js'])) {
                            $parent = $this->maps['c'][$pName]['instance']->_name;
                            $block .= "manager.template['$parent']['$tplName'].call(this, descriptor);\n    ";
                            break;
                        }
                    }
                    if (isset($tpl['js'])) {
                        $block .= $render($component, $tpl['js']);
                    }
                    $js .= "this.template['$name']['$tplName'] = function(descriptor) {\n    ";
                    $js .= str_replace("\n", "\n    ", $block);
                    $js .= "\n};\n";
                }
            }
            $content[] = $js;
        }
        $fileName = Env::$app->dir['res'].'/var/manager.js';
        $content = str_replace("\n", "\n    ", implode($content));
        ob_start();
        require __DIR__.'/ComponentManager.js';
        $manager = ob_get_clean();
        $manager = str_replace('/*COMPONENTS*/', $content, $manager);
        Env::$app->logger->warn("\033[33mSaving ".$fileName."\033[0m");
        file_put_contents($fileName, $manager);
    }

    private function generateRawMaps() {
        $this->maps['c'] = [];
        $resDir = Env::$app->dir['res'].'/var';
        if (!is_dir($resDir)) {
            mkdir($resDir);
        }
        $scanner = new DirectoryScanner();
        foreach (Env::$app->modules as $module) {
            $directory = $module->getDirectory();
            $scanner->setDirectory($directory);
            $scanner->scan(function($fileName) {
                $fwPath = substr($fileName, strlen(Env::$app->dir['src']));
                if (preg_match('~/Component/(?:[_a-z][_A-z\d]*/|)+[A-Z][A-z\d]*\.(?<format>php|js)$~', $fwPath, $m)) {
                    // found component className or JS
                    preg_match_all('~/(?<piece>[A-Z][A-z\d]*)~', $fwPath, $ns);
                    $className = implode('\\', $ns['piece']);
                    if ($m['format']==='php') {
                        $this->maps['c'][$className]['php'] = $className;
                    } elseif ($m['format']==='js') {
                        $this->maps['c'][$className]['js'] = $fileName;
                    }
                } elseif (preg_match('~^(?<ns>.*/Component)/(?:[_a-z][_A-z\d]*/|)+(?<name>[a-z][A-z\d]*)/(?<tpl>[a-z][A-z\d_]*)\.(?<format>php|js|css|res)(?:/(?<res>.*))?$~', $fwPath, $m)) {
                    // found template piece
                    preg_match_all('~/(?<piece>[A-Z][A-z\d]*)~', $m['ns'], $ns);
                    $className = implode('\\', $ns['piece']).'\\'.ucfirst($m['name']);
                    if ($m['format']==='res') {
                        $frontendName = $this->getFrontendNameByClassName($className);
                        $dir = Env::$app->dir['res'].'/var';
                        if (!is_dir($dir)) mkdir($dir);
                        $location = "/$frontendName/{$m['tpl']}/{$m['res']}";
                        $target = $dir.$location;
                        if (!is_dir(dirname($target))) mkdir(dirname($target), 0755, true);
                        if (!is_link($target)) {
                            if (!symlink($fileName, $target)) {
                                trigger_error("Symlink failed: $fileName => $target");
                            }
                            Env::$app->logger->info("link: $fileName => $target");
                        }
                        $this->maps['c'][$className]['tpl'][$m['tpl']][$m['format']][$m['res']] = $location;
                    } else {
                        $this->maps['c'][$className]['tpl'][$m['tpl']][$m['format']] = $fileName;
                    }
                }
            });
        }
    }

    private function processRawMaps() {
        $this->maps['u'] = []; // usage maps

        foreach ($this->maps['c'] as $className => $map) {
            $name = $this->getComponentNameByClassName($className);

            if (class_exists($className)) {
                $this->maps['u'][$name] = $className;
            } else {
                trigger_error('Class not found: '.$className);
            }

            // make parent list
            $c = $className;
            $parents = [];
            while ($c = get_parent_class($c)) {
                if (!isset($this->maps['c'][$c])) break;
                $parents[] = $c;
            }
            $this->maps['c'][$className]['inheritance'] = $parents;
            $this->maps['c'][$className]['parents'] = array_reverse($parents);

            // generate instance for each component
            $this->maps['c'][$className]['instance'] = new $className([], false);
        }
        // processing
        foreach ($this->maps['c'] as &$map) {
            foreach ($map['inheritance'] as $pName) {
                if (isset($this->maps['c'][$pName]['tpl'])) {
                    // set missing template pieces from parents
                    foreach ($this->maps['c'][$pName]['tpl'] as $tplName => $tpl) {
                        // set blank templates
                        if (!isset($map['tpl'][$tplName])) $map['tpl'][$tplName] = [];
                        // link to parent resources
                        if (isset($tpl['res'])) {
                            foreach ($tpl['res'] as $res => $location) {
                                if (!isset($map['tpl'][$tplName]['res'][$res])) {
                                    $map['tpl'][$tplName]['res'][$res] = $location;
                                }
                            }
                        }
                    }
                }
            }
        }
        unset($map);
    }

    private function pruneMaps() {
        foreach ($this->maps['c'] as &$map) {
            unset($map['instance']);
            unset($map['inheritance']);
        }
        unset($map);
    }

    public function build() {
        $this->generateRawMaps();
        $this->processRawMaps();
        $this->generateCss();
        $this->generateJs();
        $this->pruneMaps();

        Env::$app->logger->warn("Saving: ".$this->mapFileName);
        $this->increaseVersion();
        file_put_contents($this->mapFileName, '<?php return '.var_export($this->maps, true).';');
    }

    public function getResourceUrl($name, $template, $file) {
        if (isset($this->maps['c'][$name]['tpl'][$template]['res'][$file])) {
            return '/res/var'.($this->maps['c'][$name]['tpl'][$template]['res'][$file]).'?'.$this->version;
        } else {
            trigger_error("Res missed: {$name}[{$template}] -> {$file}", E_USER_NOTICE);
            return '/';
        }
    }

    /**
     * Use router to make URI
     * @param string $name Routing rule name
     * @param array $args Routing args
     * @return string URI
     */
    public function path($name, array $args = []) {
        return htmlspecialchars(call_user_func_array(Env::$app->router->path[(string)$name], $args));
    }

    public function getComponentPropertyList($componentName) {
        if (!isset($this->maps['u'][$componentName])) return [];
        $className = $this->maps['u'][$componentName];
        return array_keys((new $className())->properties());
    }

    public function getComponentList() {
        return array_keys($this->maps['u']);
    }

    public function makeComponentResponse($componentName, array $prop=array()) {
        $component = $this->createByName($componentName, $prop);
        $status = 200;
        if ($component->_name==='C-Invalid') {
            $status = 404;
        }
        if ($component->_name==='C-Error') {
            $status = 500;
        }
        $response = new Response($status, $this->render($component));
        return $response;
    }

}
