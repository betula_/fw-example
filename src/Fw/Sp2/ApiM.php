<?php
namespace Fw\Sp2;
use Fw\M\Api as ApiBasic;
use Fw\Fw\Env;
use PDO;


/**
 * @property \PDO $db
 */

class ApiM extends ApiBasic {


    public function registerValidators($validator) {
        parent::registerValidators($validator);

        $validator->checkers['purchaseOptionType'] = function(&$value, &$errors, $params=null, $varName=null) {
            if (!is_string($value)) {
                $errors[$varName]['purchaseOptionType.type'] = gettype($value);
                return 1;
            }
            $types = Env::$app->cfg['purchaseOptionTypes'];
            if (!in_array($value, $types)) {
                $errors[$varName]['purchaseOptionType'] = $types;
                return 1;
            }
            return 0;
        };

    }

}