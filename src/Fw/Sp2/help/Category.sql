
CREATE  TABLE IF NOT EXISTS `Category` (
  `category_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `path` CHAR(200) NOT NULL ,
  `order` INT NOT NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `important` INT NOT NULL ,
  `depth` INT NOT NULL ,
  `nesting` INT NOT NULL ,
  PRIMARY KEY (`category_id`) ,
  UNIQUE INDEX `category_id_UNIQUE` (`category_id` ASC) )
ENGINE = InnoDB;

