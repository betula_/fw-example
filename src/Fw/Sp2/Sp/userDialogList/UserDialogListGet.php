<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogListGet extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'int' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ]
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['user_id'].':'.$args['count'].':'.$args['page'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('
          select u.user_id, MAX(u.pm_id) as pm_id from (
            (
              select r.ID_MEMBER as user_id, m.ID_PM as pm_id from smf_personal_messages as m
              join smf_pm_recipients as r on m.ID_PM=r.ID_PM
              where m.ID_MEMBER_FROM = :user_id
            ) union (
              select m.ID_MEMBER_FROM as user_id, m.ID_PM as pm_id from smf_personal_messages as m
              join smf_pm_recipients as r on m.ID_PM=r.ID_PM
              where r.ID_MEMBER = :user_id
            )
          ) as u
          group by u.user_id
          order by u.pm_id desc
          limit :from, :count;
        ');
        $stmt->bindValue(':user_id', $args['user_id']);
        $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
        $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $list = [];
        foreach ($rows as $i) {
            $list[] = [
                'message' => $this->api->pmGetById(['pm_id' => $i['pm_id']]),
                'opponent' => $this->api->userGetById(['user_id' => $i['user_id']]),
            ];
        }

        $result = [
            'list' => $list,
            'count' => count($list),
        ];

        $this->cacheSet($cacheKey, $result, 1);
        return $result;

    }

}