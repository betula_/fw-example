<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogListCountGet extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'int' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['user_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('
          select count(distinct u.user_id) as total from (
            (
              select r.ID_MEMBER as user_id, m.ID_PM as pm_id from smf_personal_messages as m
              join smf_pm_recipients as r on m.ID_PM=r.ID_PM
              where m.ID_MEMBER_FROM = :user_id
            ) union (
              select m.ID_MEMBER_FROM as user_id, m.ID_PM as pm_id from smf_personal_messages as m
              join smf_pm_recipients as r on m.ID_PM=r.ID_PM
              where r.ID_MEMBER = :user_id
            )
          ) as u;
        ');
        $stmt->bindValue(':user_id', $args['user_id']);
        $stmt->execute();
        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $total = $count['total'];

        $result = [
            'total' => $total,
        ];

        if ($total < 100) {
            $ttl = 30;
        } elseif ($total < 1000) {
            $ttl = 300;
        } else {
            $ttl = 30000;
        }

        $this->cacheSet($cacheKey, $result, $ttl);
        return $result;

    }

}