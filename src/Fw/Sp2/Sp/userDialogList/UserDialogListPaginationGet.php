<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Sp\Sp\PaginationGet;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogListPaginationGet extends PaginationGet {

    public function args() {
        return parent::args() + [
            'user_id' => [
                'int' => null,
            ],
        ];
    }

    public function total($args) {
        $count = $this->api->userDialogListCountGet(['user_id' => $args['user_id']]);
        return $count['total'];
    }

}