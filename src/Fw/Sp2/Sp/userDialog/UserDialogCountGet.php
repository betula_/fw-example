<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogCountGet extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'int' => null,
            ],
            'opponent_id' => [
                'int' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['user_id'].':'.$args['opponent_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('
          select
          (
            select count(*) as c from smf_personal_messages as m
            join smf_pm_recipients as r on m.ID_PM=r.ID_PM
            where m.ID_MEMBER_FROM = :user_id and r.ID_MEMBER = :opponent_id
          ) + (
            select count(*) as c from smf_personal_messages as m
            join smf_pm_recipients as r on m.ID_PM=r.ID_PM
            where m.ID_MEMBER_FROM = :opponent_id and r.ID_MEMBER = :user_id
          ) as total;
        ');
        $stmt->bindValue(':user_id', $args['user_id']);
        $stmt->bindValue(':opponent_id', $args['opponent_id']);
        $stmt->execute();

        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $total = $count['total'];

        $result = [
            'total' => $total,
        ];

        if ($total < 100) {
            $ttl = 30;
        } elseif ($total < 1000) {
            $ttl = 300;
        } else {
            $ttl = 3000;
        }

        $this->cacheSet($cacheKey, $result, $ttl);
        return $result;

    }

}