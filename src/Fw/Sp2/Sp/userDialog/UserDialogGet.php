<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogGet extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'int' => null,
            ],
            'opponent_id' => [
                'int' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ]
        ];
    }

    public function perform ($args) {

        $stmt = $this->api->db->prepare('
          (
            select m.ID_PM as pm_id, m.msgtime from smf_personal_messages as m
            join smf_pm_recipients as r on m.ID_PM=r.ID_PM
            where m.ID_MEMBER_FROM = :user_id and r.ID_MEMBER = :opponent_id
          ) union (
            select m.ID_PM as pm_id, m.msgtime from smf_personal_messages as m
            join smf_pm_recipients as r on m.ID_PM=r.ID_PM
            where m.ID_MEMBER_FROM = :opponent_id and r.ID_MEMBER = :user_id
          )
          order by msgtime desc
          limit :from, :count;
        ');
        $stmt->bindValue(':user_id', $args['user_id']);
        $stmt->bindValue(':opponent_id', $args['opponent_id']);
        $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
        $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
        $stmt->execute();

        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $list = [];
        foreach($rows as $i) {
            $list[] = $this->api->pmGetById(['pm_id' => $i['pm_id']]);
        }

        return [
            'list' => $list,
        ];
    }

}