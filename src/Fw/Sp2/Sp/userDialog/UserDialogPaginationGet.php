<?php
namespace Fw\Sp2\Sp;
use Fw\Sp\ApiCall;
use Fw\Sp\Sp\PaginationGet;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserDialogPaginationGet extends PaginationGet {

    public function args() {
        return parent::args() + [
            'user_id' => [
                'int' => null,
            ],
            'opponent_id' => [
                'int' => null,
            ]
        ];
    }

    public function total($args) {
        $count = $this->api->userDialogCountGet([
            'user_id' => $args['user_id'],
            'opponent_id' => $args['opponent_id'],
        ]);
        return $count['total'];
    }

}