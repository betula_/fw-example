var self = this;

this.authEndOk = function(data) {
    $(document).trigger({
        type: 'tokenChange',
        token: data.token,
        expires: new Date(data.expires*1000)
    });
    document.location.href = '<?=$this->path("pageHome");?>';
}

