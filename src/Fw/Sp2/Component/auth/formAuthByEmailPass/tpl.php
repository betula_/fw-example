<?$BLOCK('form')?>
    <form action="#login" method="post">

        <div class="alert alert-error hide" data-error="">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Такого пользователя не существует
        </div>

        <div class="control-group" data-error="email">
            <label>Имя пользователя или e-mail:</label>
            <input type="text" name="email" class="input-block-level" />
        </div>
        <div class="control-group">
            <label>Пароль:</label>
            <input type="password" name="pass" class="input-block-level" />
        </div>
    </form>

<?$END()?>
