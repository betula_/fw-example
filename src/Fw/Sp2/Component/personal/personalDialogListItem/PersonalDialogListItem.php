<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PersonalDialogListItem extends Component {

    public function properties() {
        return parent::properties() + [
            'pm_id' => [
                'int' => null,
            ],
            'opponent_id' => [
                'int' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'pm_id';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }

        $this->userId = $user['user_id'];

        $message = $sp->pmGetById(['pm_id' => $this->pm_id]);

        $this->messageTitle = $message['subject'];
        $this->messageHtml = $message['body_html'];
        $this->messageDate = date('d.m.Y, H:i', $message['msgtime']);
        $this->messageSenderId = $message['sender_id'];

        $opponent = $sp->userGetById(['user_id' => $this->opponent_id]);

        $this->opponentId = $opponent['user_id'];
        $this->opponentRealName = $opponent['realName'];

    }


}
