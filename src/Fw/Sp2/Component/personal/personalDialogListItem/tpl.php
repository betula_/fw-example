<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <div class="row">
        <div class="span1">
            <div class="image">
                <img src="<?=$c->res('avatar.gif')?>" />
            </div>
        </div>
        <div class="span2">
            <div class="user">
                <a href="#" class="name"><?=$E($c->opponentRealName)?></a>
                <div class="date"><?=$E($c->messageDate)?></div>
            </div>
        </div>
        <div class="span6">
            <a href="<?=$c->path('pagePersonalDialog', $c->opponentId)?>" class="message <?if($c->userId === $c->messageSenderId) echo 'own';?>">
                <div class="title"><?=$E($c->messageTitle)?></div>
                <div class="body">
                    <?=$c->messageHtml?>
                </div>
            </a>
        </div>
    </div>

</div>
