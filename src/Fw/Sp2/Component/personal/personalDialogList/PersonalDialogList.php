<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PersonalDialogList extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }

        $list = $sp->userDialogListGet(['user_id' => $user['user_id'], 'page' => $this->page]);

        foreach($list['list'] as $i) {
            $this->insert('items', 'personalDialogListItem', [
                'pm_id' => $i['message']['pm_id'],
                'opponent_id' => $i['opponent']['user_id'],
            ]);
        }

    }


}
