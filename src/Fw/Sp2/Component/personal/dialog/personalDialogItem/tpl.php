<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">


    <a href="#" class="name"><?=$E($c->senderRealName)?></a>
    <div class="date"><?=$E($c->messageDate)?></div>

    <div class="message <?if($c->userId === $c->senderId) echo 'own';?>">
        <div class="title"><?=$E($c->messageTitle)?></div>
        <div class="body">
            <?=$c->messageHtml?>
        </div>
    </div>


</div>
