<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PersonalDialogBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => null,
            ],
            'opponent_id' => [
                'int' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'opponent_id';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }


        $this->insert('list', 'personalDialog', ['page' => $this->page, 'opponent_id' => $this->opponent_id]);
        $this->insert('pagination', 'personalDialogPagination', ['page' => $this->page, 'opponent_id' => $this->opponent_id]);

    }


}
