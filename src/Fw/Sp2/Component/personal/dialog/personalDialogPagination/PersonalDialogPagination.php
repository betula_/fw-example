<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PersonalDialogPagination extends Pagination {

    public function properties() {
        return parent::properties() + [
            'opponent_id' => [
                'int' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'opponent_id';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }

        $pagination = $sp->userDialogPaginationGet([
            'user_id' => $user['user_id'],
            'opponent_id' => $this->opponent_id,
            'page' => $this->page
        ]);

        $this->start = $pagination['start'];
        $this->end = $pagination['end'];
        $this->prev = $pagination['prev'];
        $this->next = $pagination['next'];
        $this->list = $pagination['list'];

    }

    public function pathPage($page) {
        return $this->path('pagePersonalDialog', $this->opponent_id, ['page'=>$page]);
    }

}
