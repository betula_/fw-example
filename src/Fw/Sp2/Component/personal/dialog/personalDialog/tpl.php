<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <ul>
        <?foreach($c->renderSlotComponents('items') as $i):?>
        <li><?=$i?></li>
        <?endforeach;?>
    </ul>
</div>
