<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PersonalDialog extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'opponent_id' => [
                'int' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'opponent_id';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }

        $list = $sp->userDialogGet([
            'user_id' => $user['user_id'],
            'opponent_id' => $this->opponent_id,
            'page' => $this->page
        ]);

        foreach($list['list'] as $i) {
            $this->insert('items', 'personalDialogItem', [
                'pm_id' => $i['pm_id'],
            ]);
        }

    }


}
