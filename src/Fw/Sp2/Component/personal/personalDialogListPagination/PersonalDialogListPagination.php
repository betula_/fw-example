<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PersonalDialogListPagination extends Pagination {

    public function exec() {
        parent::exec();

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }

        $pagination = $sp->userDialogListPaginationGet(['user_id' => $user['user_id'], 'page' => $this->page]);

        $this->start = $pagination['start'];
        $this->end = $pagination['end'];
        $this->prev = $pagination['prev'];
        $this->next = $pagination['next'];
        $this->list = $pagination['list'];

    }

    public function pathPage($page) {
        return $this->path('pagePersonalDialogList', ['page'=>$page]);
    }

}
