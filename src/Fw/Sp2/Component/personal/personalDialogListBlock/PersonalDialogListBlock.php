<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PersonalDialogListBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';

        $sp = Env::$app->sp;
        $user = $sp->user;

        if (!$user) {
            $this->_template = 'empty';
            return;
        }


        $this->insert('list', 'personalDialogList', ['page' => $this->page]);
        $this->insert('pagination', 'personalDialogListPagination', ['page' => $this->page]);

    }


}
