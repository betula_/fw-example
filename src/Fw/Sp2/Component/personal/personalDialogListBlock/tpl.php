<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <h3>Сообщения</h3>

    <ul class="nav nav-tabs">
        <li class="active"><a href="<?=$c->path('pagePersonalDialogList')?>">Диалоги</a></li>
        <li><a href="<?=$c->path('pagePersonalDialogList')?>">Просмотр диалогов</a></li>
    </ul>

    <div class="slot-list">
        <?=$c->renderSlot('list')?>
    </div>

    <div class="slot-pagination">
        <?=$c->renderSlot('pagination')?>
    </div>

</div>
