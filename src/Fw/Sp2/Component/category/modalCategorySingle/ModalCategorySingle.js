var self = this;

this.dom.bind('categorySingleNeed', function(e, data) {
    self.okCallback = data.ok;
    self.show();
});

this.finish = function() {
    self.hide();
    if (self.okCallback) {
        self.okCallback({
            category_id: self.category_id
        });
    }
}

