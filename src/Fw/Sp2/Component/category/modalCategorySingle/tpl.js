var self = this;

var button = this.dom.find('[data-action="category-select"]');

this.dom.bind('selectCategorySingleOk', function(e, data) {
    e.stopPropagation();

    if (data.nesting > 0 || !data.nesting) {
        button.addClass('disabled');
    } else {
        button.removeClass('disabled');
    }
    self.category_id = data.category_id;
});

this.show = function() {
    button.addClass('disabled');
    self.dom.modal('show');
};
this.hide = function() {
    self.dom.modal('hide');
};

button.click(function(e) {
    var t = $(this);
    if (!t.is('.disabled')) {
        self.finish();
    }
});