<div class="modal hide fade <?=$c->getCssClass()?>" id="<?=$E($c->_id)?>" tabindex="-1" role="dialog">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Выбор категории</h3>
    </div>
    <div class="modal-body">
        <?=$c->renderSlot('body')?>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">Закрыть</button>
        <button class="btn btn-primary disabled" data-action="category-select">Выбрать</button>
    </div>
</div>
