<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <ul class="breadcrumb">
        <?if(isset($c->category)):?>
        <li><a href="#" data-id="" data-nesting="1">Всё</a> <span class="divider">/</span></li>
        <?foreach(array_reverse($c->parents) as $category):?>
        <li><a href="#" data-id="<?=$E($category['category_id'])?>" data-nesting="<?=$E($category['nesting'])?>"><?=$E($category['title'])?></a> <span class="divider">/</span></li>
        <?endforeach;?>
        <li class="active"><?=$E($c->category['title'])?></li>
        <?else:?>
        <li class="active">Всё</li>
        <?endif;?>
    </ul>

    <div class="row">
        <?foreach ($c->table as $column):?>
        <div class="span2">
            <?foreach($column as $block):?>
            <?if ($block['type']=='important'):?>
            <ul class="nav nav-pills nav-stacked important">
                <?foreach($block['list'] as $category):?>
                <li <?if(isset($c->category['category_id']) && $category['category_id'] == $c->category['category_id']):?>class="active"<?endif?> >
                    <a href="#" data-id="<?=$E($category['category_id'])?>" data-nesting="<?=$E($category['nesting'])?>"><?=$E($category['title'])?></a>
                </li>
                <?endforeach;?>
            </ul>
            <?else:?>
            <div class="letter"><?=$E($block['letter'])?></div>
            <ul class="nav nav-pills nav-stacked">
                <?foreach($block['list'] as $category):?>
                <li <?if(isset($c->category['category_id']) && $category['category_id'] == $c->category['category_id']):?>class="active"<?endif?> >
                    <a href="#" data-id="<?=$E($category['category_id'])?>" data-nesting="<?=$E($category['nesting'])?>"><?=$E($category['title'])?></a>
                </li>
                <?endforeach;?>
            </ul>
            <?endif;?>
            <?endforeach;?>
        </div>
        <?endforeach;?>
    </div>
</div>
