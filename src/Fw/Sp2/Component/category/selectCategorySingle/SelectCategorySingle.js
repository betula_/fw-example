var self = this;

this.select = function(data) {
    self.dom.trigger('selectCategorySingleOk', data);
    self.prop.category_id = data.category_id;
    self.refresh();
}

this.dom.bind('categorySingleNeed', function(e, data) {
    self.select(data);
});
