var self = this;
var region = this.dom.find('a[data-id]');

region.click(function(e) {
    var t = $(this);
    var data = {};
    if (t.attr('data-id')) {
        data.category_id = t.attr('data-id');
    }
    if (t.attr('data-nesting')) {
        data.nesting = t.attr('data-nesting');
    }

    self.select(data);
    e.preventDefault();
});
