<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class SelectCategorySingle extends Component {

    public function properties() {
        return parent::properties() + [
            'category_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'columns' => [
                'int' => null,
                'default' => 3,
            ]
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'category_id';
        $this->_export[] = 'columns';

        $parent_id = null;
        if (isset($this->category_id)) {

            $category = Env::$app->m->categoryGetById(['category_id' => $this->category_id]);
            $this->category = $category;
            $p = Env::$app->m->categoryParentsGetById(['category_id' => $this->category_id]);
            $this->parents = $p['list'];

            if ($category['nesting']) {
                $parent_id = $category['category_id'];
            } elseif (isset($p['list'][0])) {
                $parent_id = $p['list'][0]['category_id'];
            }

        }

        $p = Env::$app->m->categoryChildrenGetById(['category_id' => $parent_id, 'depth' => 1]);

        $important = [];
        $dict = [];
        $count = 0;

        foreach ($p['list'] as $category) {
            if ($category['important']) {
                $important[] = $category;
            } else {
                $count ++;
                $dict[mb_strtoupper(mb_substr($category['title'],0,1))][] = $category;
            }
        }
        ksort($dict);

        $index = 0;
        $table = [];
        if ($important) {
            $table[$index][] = [
                'type' => 'important',
                'list' => $important,
            ];
            $index ++;
        }
        $line = $count / ($this->columns - $index);
        $col = 0;
        foreach ($dict as $letter => $group) {
            $table[$index][] = [
                'type' => 'letter',
                'letter' => $letter,
                'list' => $group,
            ];
            $col += count($group);
            if ($col >= $line) {
                $index ++;
                $col = 0;
            }
        }

        $this->table = $table;
    }

}
