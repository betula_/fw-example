<?php
namespace Fw\Sp2\Component;
use Fw\M\Component\RichText as RichTextBasic;
use Fw\Fw\Env;

class RichText extends RichTextBasic {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();
    }

}
