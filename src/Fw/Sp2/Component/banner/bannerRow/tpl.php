<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <div class="box">
        <div class="inner">
            <ul>
                <?foreach($c->renderSlotComponents('items') as $i):?>
                <li><?=$i?></li>
                <?endforeach;?>
            </ul>
        </div>
        <a href="#" class="arrow-left"></a>
        <a href="#" class="arrow-right"></a>
    </div>

    <div class="control-slide" data-action="collapse">
        <a href="#">свернуть</a>
        <span class="caret up"></span>
    </div>
    <div class="control-slide hide" data-action="expand">
        <a href="#">развернуть</a>
        <span class="caret down"></span>
    </div>


    <div class="control-buy">
        <a href="#">Купить здесь место</a>
    </div>
</div>
