var self = this;

var controlSlide = this.dom.find('> .control-slide');
var collapse = controlSlide.filter('[data-action="collapse"]');
var expand = controlSlide.filter('[data-action="expand"]');
var box = this.dom.find('> .box');

collapse.click(function(e) {
    e.preventDefault();

    collapse.addClass('hide');
    expand.removeClass('hide');
    box.slideUp('fast');
});
expand.click(function(e) {
    e.preventDefault();

    collapse.removeClass('hide');
    expand.addClass('hide');
    box.slideDown('fast');
});
