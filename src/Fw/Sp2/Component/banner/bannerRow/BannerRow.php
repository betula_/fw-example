<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class BannerRow extends Component {

    public function exec() {
        parent::exec();


        for ($i=0; $i<10; $i++) {
            $this->insert('items', 'bannerRowItem');
        }

    }

}
