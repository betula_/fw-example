<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class BannerRowItem extends Component {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        $this->image = $this->res('image.png');

    }

}
