<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PurchaseListPagination extends Pagination {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'owner_id';
        $this->_export[] = 'category_id';

        $m = Env::$app->m;

        if (isset($this->owner_id)) {
            $pagination = $m->purchaseListPaginationGetByOwnerId([
                'page' => $this->page,
                'owner_id' => $this->owner_id,
            ]);
        } else {
            $pagination = $m->purchaseListPaginationGetByCategoryId([
                'page' => $this->page,
                'category_id' => $this->category_id,
            ]);
        }

        $this->start = $pagination['start'];
        $this->end = $pagination['end'];
        $this->prev = $pagination['prev'];
        $this->next = $pagination['next'];
        $this->list = $pagination['list'];

    }

    public function pathPage($page) {
        return $this->path('pagePurchaseList',
            array_filter([
                'page' => $page,
                'owner_id' => $this->owner_id,
                'category_id' => $this->category_id,
            ])
        );
    }

}
