<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class ModalPurchaseAdd extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formPurchaseAdd');
    }

}
