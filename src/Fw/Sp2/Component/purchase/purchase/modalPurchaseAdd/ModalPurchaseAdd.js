var self = this;

this.dom.bind('purchaseAddNeed', function(e, data) {
    self.children('formPurchaseAdd')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseAddOk', function(e) {
    self.hide();
});