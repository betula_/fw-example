<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Purchase extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'purchase_id';
        $this->_export[] = 'managed';

        $m = Env::$app->m;

        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }

        $item = $m->purchaseGetById(['purchase_id' => $this->purchase_id]);
        $this->image = $item['image'];
        $this->title = $item['title'];
        $this->description = $item['description'];

        $this->managed = false;
        if (isset($m->user['role']['admin'])) {
            $this->managed = true;
        }
        if (isset($m->user['user_id'])) {
            if ($m->user['user_id'] == $item['owner_id']) {
                $this->managed = true;
            }
            if ($m->user['user_id'] == $item['org_id']) {
                $this->managed = true;
            }
        }

        $this->allowRemove = true;

        $this->insertManage();
        $this->insertOption();
        $this->insertProduct();
    }

    public function insertManage() {
        if (!$this->managed) return;
        Env::$app->c->trigger('modalAdd', 'modalPurchaseUpdate');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseRemove');
    }

    public function insertOption() {
        $this->insert('option', 'purchaseOptionList', [
            'purchase_id' => $this->purchase_id,
            'managed' => $this->managed,
        ]);
    }

    public function insertProduct() {
        $this->insert('product', 'purchaseProductListBlock', [
            'purchase_id' => $this->purchase_id,
            'managed' => $this->managed,
        ]);
    }


}
