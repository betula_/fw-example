var self = this;

this.dom.bind('purchaseUpdateOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="purchase-update"]').click(function() {
        $.event.trigger('purchaseUpdateNeed', {
            purchase_id: self.prop.purchase_id
        });
    });
    this.dom.find('[data-action="purchase-remove"]').click(function() {
        $.event.trigger('purchaseRemoveNeed', {
            purchase_id: self.prop.purchase_id
        });
    });
}

this.dom.bind('purchaseRemoveOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        document.location.href = '<?=$c->path("pagePurchaseList");?>';
    }
});