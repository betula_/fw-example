<div id="<?=$E($c->_id)?>" class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>">

    <?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-mini btn-info" data-action="purchase-update">
            <i class="icon-pencil icon-white"></i>
        </button>
        <?if(isset($c->allowRemove)):?>
        <button class="btn btn-mini btn-warning" data-action="purchase-remove">
            <i class="icon-remove icon-white"></i>
        </button>
        <?endif;?>
    </div>
    <?endif;?>
    <?$END()?>

    <?$BLOCK('body')?>
    <div class="row">
        <div class="span6">
            <h1><?=$E($c->title)?></h1>
            <p>
                <?=$c->description?>
            </p>

            <b>Параметры:</b>
            <div class="slot-option">
                <?=$c->renderSlot('option')?>
            </div>

            <div class="slot-product">
                <?=$c->renderSlot('product')?>
            </div>

        </div>

        <?if(isset($c->image)):?>
        <div class="span2">
            <div class="img-polaroid">
                <img src="<?=$c->path('image', $c->image, 140)?>" />
            </div>
        </div>
        <?endif;?>
    </div>

    <?$END()?>

</div>