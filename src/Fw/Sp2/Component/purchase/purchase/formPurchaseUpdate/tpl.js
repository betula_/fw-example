var self = this;

var form = this.dom.find('> form');

this.formInit(form, {
    send: function(data) {
        self.purchaseUpdate(data);
    }
});