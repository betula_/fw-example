<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'purchase_id';

        $item = null;
        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }

        $item = Env::$app->m->purchaseGetById(['purchase_id' => $this->purchase_id]);

        $this->image = $item['image'];
        $this->title = $item['title'];

        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
            'value' => $item['description'],
        ]);

        $this->_export[] = 'category_id';

    }

}
