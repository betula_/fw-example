var self = this;

this.purchaseUpdate = function(options) {
    options.data.purchase_id = self.prop.purchase_id;

    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseUpdate"]);?>';

    self.ajax(options);
}

this.purchaseUpdateOk = function(data) {
    $.event.trigger('purchaseUpdateOk', data);
}