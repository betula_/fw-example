var self = this;


this.dom.bind('purchaseRemoveNeed', function(e, data) {
    self.prop.purchase_id = data.purchase_id;
    self.show();
});

this.purchaseRemove = function(data) {
    var ok = data.ok;
    data.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseRemoveOk(response.result);
    }
    data.url = '<?=$this->path("ajax", ["purchaseRemove"]);?>';

    self.ajax(data);
}

this.purchaseRemoveOk = function(data) {
    $.event.trigger('purchaseRemoveOk', data);
}