var self = this;

this.dom.find('[data-action="purchase-remove"]').click(function() {
    self.purchaseRemove({
        ok: function() {
            self.hide();
        },
        error: function() {},
        data: {
            purchase_id: self.prop.purchase_id
        }
    });
});
