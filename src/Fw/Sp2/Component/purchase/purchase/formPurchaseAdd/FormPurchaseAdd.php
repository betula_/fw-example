<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseAdd extends Form {

    public function exec() {
        parent::exec();

        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
        ]);

    }

}
