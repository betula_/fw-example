var self = this;

this.purchaseAdd = function(options) {
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseAddOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseAdd"]);?>';

    self.ajax(options);
}

this.purchaseAddOk = function(data) {
    $.event.trigger('purchaseAddOk', data);
}