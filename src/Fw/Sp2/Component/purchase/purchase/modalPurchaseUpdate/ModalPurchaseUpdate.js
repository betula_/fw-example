var self = this;

this.dom.bind('purchaseUpdateNeed', function(e, data) {
    self.children('formPurchaseUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseUpdateOk', function(e) {
    self.hide();
});