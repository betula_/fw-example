<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseList extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'owner_id';
        $this->_export[] = 'category_id';

        $m = Env::$app->m;


        if (isset($this->owner_id)) {
            $list = $m->purchaseListGetByOwnerId([
                'page' => $this->page,
                'owner_id' => $this->owner_id,
            ]);
        } else {
            $list = $m->purchaseListGetByCategoryId([
                'page' => $this->page,
                'category_id' => $this->category_id,
            ]);
        }

        foreach($list['list'] as $i) {
            $this->insert('items', 'purchaseListItem', [
                'purchase_id' => $i['purchase_id'],
            ]);
        }

    }


}
