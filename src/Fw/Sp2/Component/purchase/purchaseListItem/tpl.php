<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <div class="row">
        <div class="span1">
            <div class="image">
                <?if(isset($c->purchaseImage)):?>
                <img src="<?=$c->path('image', $c->purchaseImage, 51, 51)?>" />
                <?else:?>
                <img src="<?=$c->res('noimage.jpg')?>" />
                <?endif;?>
            </div>
        </div>
        <div class="span2">
            <div class="user">
                <a href="#" class="name"><?=$E($c->purchaseOrgName)?></a>
                <div class="date"><?=$E($c->purchaseDate)?></div>
            </div>
        </div>
        <div class="span6">
            <a href="<?=$c->path('pagePurchase', $c->purchaseId)?>" class="box">
                <div class="title"><?=$E($c->purchaseTitle)?></div>
                <div class="body">
                    <?=$c->purchaseHtml?>
                </div>
            </a>
        </div>
    </div>

</div>
