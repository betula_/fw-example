<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseListItem extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'purchase_id';

        $m = Env::$app->m;
        $sp = Env::$app->sp;


        $item = $m->purchaseGetById([
            'purchase_id' => $this->purchase_id
        ]);


        $this->purchaseImage = $item['image'];
        $this->purchaseId = $item['purchase_id'];
        $this->purchaseTitle = $item['title'];
        $this->purchaseHtml = $item['description'];
        $this->purchaseDate = date('d.m.Y, H:i', $item['dtc']);

        if (isset($item['org_id'])) {
            $userSp = $m->userSpGetByUserId(['user_id' => $item['org_id']]);
            $user = $sp->userGetById(['user_id' => $userSp['id']]);

            $this->purchaseOrgName = $user['realName'];
        }

    }


}
