<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'purchase_id';

        $m = Env::$app->m;

        if (isset($m->user['role']['org'])) {
            $this->org = true;
            Env::$app->c->trigger('modalAdd', 'modalPurchaseUpdate');
            //Env::$app->c->trigger('modalAdd', 'modalPurchaseRemove');
        }


        $this->insert('purchase', 'purchase', [
            'purchase_id' => $this->purchase_id,
        ]);

        //$this->insert('breadcrumb', 'purchaseListBreadcrumb', [
        //    'category_id' => $this->category_id,
        //]);

    }


}
