<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <h3>Закупка</h3>


    <div class="slot-list">
        <?=$c->renderSlot('purchase')?>
    </div>

    <div class="slot-product">
        <?=$c->renderSlot('product')?>
    </div>

</div>
