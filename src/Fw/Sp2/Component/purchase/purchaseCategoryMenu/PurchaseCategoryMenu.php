<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseCategoryMenu extends Component {

    public function exec() {
        $this->items = Env::$app->m->categoryPurchaseTreeGetById(['category_id'=>null, 'depth'=>2])['tree'];
    }

    public function pathCategory($category_id) {
        return $this->path('pagePurchaseList', [
            'category_id' => $category_id
        ]);
    }

}
