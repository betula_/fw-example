var self = this;

var list = this.dom.find('li[data-group]');

list.filter('.line').each(function() {
    var t = $(this);
    t.children('a').click(function(e) {
        e.preventDefault();
        var group = t.attr('data-group');
        self.select(group);
    });
});
list.filter('.panel').each(function() {
    var t = $(this);
    t.children('a').click(function(e) {
        e.preventDefault();
        var group = t.attr('data-group');
        self.unselect(group);
    });
});



this.selected = null;
this.select = function(group) {
    if (this.selected) {
        this.unselect();
    }
    this.selected = group;
    var i = list.filter('[data-group="'+group+'"]');
    i.filter('.line').addClass('hide');
    i.filter('.panel').removeClass('hide');
}

this.unselect = function(group) {
    if (!group) {
        group = this.selected;
    }
    if (group == this.selected) {
        this.selected = null;
    }
    var i = list.filter('[data-group="'+group+'"]');
    i.filter('.line').removeClass('hide');
    i.filter('.panel').addClass('hide');
}

this.dom.bind('click', function(e) {
    e.stopPropagation();
});

this.dom.bind('pageClick', function() {
    self.unselect();
});