<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <h3>Каталог закупок</h3>
    <ul class="vertical">
        <?foreach($c->items as $i):?>
        <li class="line" data-group="<?=$E($i['category']['category_id'])?>">
            <a href="<?=$c->pathCategory($i['category']['category_id'])?>"><?=$E($i['category']['title'])?></a>
        </li>
        <li class="panel hide" data-group="<?=$E($i['category']['category_id'])?>">
            <a href="<?=$c->pathCategory($i['category']['category_id'])?>"><?=$E($i['category']['title'])?></a>
            <div class="sub-panel">
                <ul class="cell">
                    <?$middle = ceil(count($i['tree'])/2);?>
                    <?foreach($i['tree'] as $l):?>
                    <li>
                        <span class="counter"><?=$E($l['category']['count'])?></span>
                        <a href="<?=$c->pathCategory($l['category']['category_id'])?>"><?=$E($l['category']['title'])?></a>
                    </li>
                    <?if (--$middle == 0):?>
                </ul><ul class="cell">
                    <?endif;?>
                    <?endforeach;?>
                </ul>
            </div>
        </li>
        <?endforeach;?>
    </ul>
</div>
