<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseListBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'owner_id';
        $this->_export[] = 'category_id';

        $m = Env::$app->m;

        if (isset($m->user['role']['org']) || isset($m->user['role']['admin'])) {
            $this->org = true;
            Env::$app->c->trigger('modalAdd', 'modalPurchaseAdd');
        }


        $this->insert('list', 'purchaseList', [
            'page' => $this->page,
            'owner_id' => $this->owner_id,
            'category_id' => $this->category_id,
        ]);
        $this->insert('pagination', 'purchaseListPagination', [
            'page' => $this->page,
            'owner_id' => $this->owner_id,
            'category_id' => $this->category_id,
        ]);
        //$this->insert('breadcrumb', 'purchaseListBreadcrumb', [
        //    'category_id' => $this->category_id,
        //]);

    }


}
