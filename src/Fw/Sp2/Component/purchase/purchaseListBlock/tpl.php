<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <h3>Закупки</h3>

    <?if(isset($c->org) && isset($c->owner_id)):?>
    <div class="well well-small clearfix" data-action="purchase-add">
        <a href="<?=$c->path('pagePurchaseAdd')?>" class="btn pull-right">Создать закупку</a>
    </div>
    <?endif;?>

    <div class="slot-list">
        <?=$c->renderSlot('list')?>
    </div>

    <div class="slot-pagination">
        <?=$c->renderSlot('pagination')?>
    </div>

</div>
