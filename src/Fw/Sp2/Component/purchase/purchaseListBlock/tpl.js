var self = this;


this.dom.find('[data-action="purchase-add"] > a').click(function(e) {
    e.preventDefault();
    $.event.trigger('purchaseAddNeed', {
        category_id: self.prop.category_id
    });
});

this.dom.bind('purchaseAddOk', function(e, data) {
    self.refresh();
});
