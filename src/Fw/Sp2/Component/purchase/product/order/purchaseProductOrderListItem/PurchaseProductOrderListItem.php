<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductOrderListItem extends Component {

    public function properties() {
        return parent::properties() + [
            'order_id' => [
                'uid' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'order_id';

        $m = Env::$app->m;
        $sp = Env::$app->sp;

        $item = $m->purchaseProductOrderGetById(['order_id' => $this->order_id]);
        $this->count = $item['count'];

        $userSpInfo = $m->userSpGetByUserId(['user_id' => $item['user_id']]);
        $userSp = $sp->userGetById(['user_id' => $userSpInfo['id']]);

        $this->userName = $userSp['realName'];

    }

}
