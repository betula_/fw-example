<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductOrderBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'product_id' => [
                'uid' => null,
                'optional' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'product_id';

        if (!isset($this->product_id)) {
            $this->_template = 'empty';
            return;
        }
        $m = Env::$app->m;

        $this->insert('list', 'purchaseProductOrderList', [
            'product_id' => $this->product_id,
        ]);

        if (isset($m->user)) {
            $this->insert('order', 'purchaseProductOrder', [
                'product_id' => $this->product_id,
            ]);
        }
    }

}
