var self = this;

var input = this.dom.find('input[name="count"]');

this.dom.find('[data-action="order-add"]').click(function(e) {
    e.preventDefault();
    self.orderAdd(input.val());
});

this.dom.find('[data-action="order-remove"]').click(function(e) {
    e.preventDefault();
    self.orderRemove();
});


this.dom.bind('purchaseProductOrderUpdateOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductOrderAddOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductOrderRemoveOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});