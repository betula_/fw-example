<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <?if($E($c->count)):?>
    <div>
        <span class="label label-important">Вы заказали <?=$E($c->count)?> шт.</span>
    </div>
    <?endif;?>
    <div class="row">
        <div class="span1">
            <b>Кол-во:</b>
        </div>
        <div class="span1">
            <input type="text" name="count" value="<?=$E($c->count,0)?>" class="span1" />
        </div>
        <div class="span1">
            <a href="#" class="btn btn-mini" data-action="order-add">Да</a>
            <a href="#" class="btn btn-mini btn-danger" data-action="order-remove">Удалиться</a>
        </div>
    </div>


</div>