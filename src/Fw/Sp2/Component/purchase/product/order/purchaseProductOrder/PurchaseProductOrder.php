<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductOrder extends Component {

    public function properties() {
        return parent::properties() + [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'product_id';

        $m = Env::$app->m;
        if (!isset($m->user)) {
            return;
        }

        $items = $m->purchaseProductOrderListGet([
            'product_id' => $this->product_id,
            'user_id' => $m->user['user_id'],
            'count' => 1,
        ])['list'];
        if (isset($items[0])) {
            $this->count = $items[0]['count'];
        }

    }

}
