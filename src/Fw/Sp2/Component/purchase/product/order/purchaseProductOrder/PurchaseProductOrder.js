var self = this;

this.orderAdd = function(count) {
    var options = {
        data: {
            product_id: self.prop.product_id,
            count: count
        }
    };

    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.orderAddOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseProductOrderAdd"]);?>';

    self.ajax(options);
}

this.orderAddOk = function(data) {
    $.event.trigger('purchaseProductOrderAddOk', data);
}

this.orderRemove = function() {
    var options = {
        data: {
            product_id: self.prop.product_id
        }
    };

    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.orderRemoveOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseProductOrderRemove"]);?>';

    self.ajax(options);
}

this.orderRemoveOk = function(data) {
    $.event.trigger('purchaseProductOrderRemoveOk', data);
}