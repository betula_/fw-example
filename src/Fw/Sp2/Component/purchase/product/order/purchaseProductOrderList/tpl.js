var self = this;


this.dom.bind('purchaseProductOrderUpdateOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductOrderAddOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductOrderRemoveOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});
