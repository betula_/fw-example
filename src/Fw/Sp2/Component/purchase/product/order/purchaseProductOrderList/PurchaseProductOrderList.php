<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductOrderList extends Component {

    public function properties() {
        return parent::properties() + [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'product_id';

        $m = Env::$app->m;

        $list = $m->purchaseProductOrderListGet([
            'product_id' => $this->product_id,
            'count' => 1000,
        ]);

        foreach($list['list'] as $i) {
            $this->insert('list', 'purchaseProductOrderListItem', [
                'order_id' => $i['order_id'],
            ]);
        }
    }

}
