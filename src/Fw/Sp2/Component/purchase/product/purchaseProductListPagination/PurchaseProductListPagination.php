<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PurchaseProductListPagination extends Pagination {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'purchase_id' => [
                'uid' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'purchase_id';

        $m = Env::$app->m;

        $pagination = $m->purchaseProductListPaginationGet([
            'page' => $this->page,
            'purchase_id' => $this->purchase_id,
        ]);

        $this->start = $pagination['start'];
        $this->end = $pagination['end'];
        $this->prev = $pagination['prev'];
        $this->next = $pagination['next'];
        $this->list = $pagination['list'];

    }

    public function pathPage($page) {
        return '#'.$page;
    }

}
