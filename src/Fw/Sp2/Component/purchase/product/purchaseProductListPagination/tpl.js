var self = this;

this.dom.bind('purchaseProductPaginationChange', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.prop.page = data.page;
        self.refresh();
    }
});

this.dom.find('a[href^="#"]').click(function(e) {
    e.stopPropagation();
    e.preventDefault();
    var page = $(this).attr('href').substring(1);

    console.log(page);

    $.event.trigger('purchaseProductPaginationChange', {
        purchase_id: self.prop.purchase_id,
        page: page
    });
});