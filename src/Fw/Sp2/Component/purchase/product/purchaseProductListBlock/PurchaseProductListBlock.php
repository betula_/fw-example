<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductListBlock extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
            ],
            'page' => [
                'int' => null,
                'default' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';
        $this->_export[] = 'purchase_id';
        $this->_export[] = 'managed';

        $this->insert('list', 'purchaseProductList', [
            'managed' => $this->managed,
            'page' => $this->page,
            'purchase_id' => $this->purchase_id,
        ]);
        $this->insert('pagination', 'purchaseProductListPagination', [
            'managed' => $this->managed,
            'page' => $this->page,
            'purchase_id' => $this->purchase_id,
        ]);

    }


}
