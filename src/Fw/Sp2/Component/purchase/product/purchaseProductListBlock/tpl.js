var self = this;


this.dom.bind('purchaseProductPaginationChange', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.prop.page = data.page;
    }
});
