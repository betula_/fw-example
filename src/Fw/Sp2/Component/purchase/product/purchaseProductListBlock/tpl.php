<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <h3>Товары</h3>

    <div class="slot-list">
        <?=$c->renderSlot('list')?>
    </div>

    <div class="slot-pagination">
        <?=$c->renderSlot('pagination')?>
    </div>

</div>
