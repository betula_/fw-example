<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProduct extends Component {

    public function properties() {
        return parent::properties() + [
            'product_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'product_id';
        $this->_export[] = 'managed';

        if (!isset($this->product_id)) {
            $this->_template = 'empty';
            return;
        }

        $product = Env::$app->m->purchaseProductGetById(['product_id' => $this->product_id]);

        $this->image = $product['image'];
        $this->title = $product['title'];
        $this->description = $product['description'];

        $this->insertManage();

        $this->insert('orders', 'purchaseProductOrderBlock', [
            'product_id' => $this->product_id,
        ]);
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalPurchaseProductUpdate');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseProductRemove');
    }

}
