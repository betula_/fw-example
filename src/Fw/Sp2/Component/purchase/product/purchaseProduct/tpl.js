var self = this;

this.dom.bind('purchaseProductUpdateOk', function(e, data) {
    if (data.product_id == self.prop.product_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="purchase-product-update"]').click(function() {
        $.event.trigger('purchaseProductUpdateNeed', {
            product_id: self.prop.product_id
        });
    });
    this.dom.find('[data-action="purchase-product-remove"]').click(function() {
        $.event.trigger('purchaseProductRemoveNeed', {
            product_id: self.prop.product_id
        });
    });
}
