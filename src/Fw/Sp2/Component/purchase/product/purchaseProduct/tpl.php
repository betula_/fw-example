<div id="<?=$E($c->_id)?>" class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>">

    <?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-mini btn-info" data-action="purchase-product-update">
            <i class="icon-pencil icon-white"></i>
        </button>
        <button class="btn btn-mini btn-warning" data-action="purchase-product-remove">
            <i class="icon-remove icon-white"></i>
        </button>
    </div>
    <?endif;?>
    <?$END()?>

    <?$BLOCK('body')?>

    <div class="row">
        <div class="span6">
            <h1><?=$E($c->title)?></h1>
            <p>
                <?=$c->description?>
            </p>


            <div class="row">
                <div class="offset 2 span4">
                    <?=$c->renderSlot('orders')?>
                </div>
            </div>
        </div>

        <?if(isset($c->image)):?>
        <div class="span2">
            <div class="img-polaroid">
                <img src="<?=$c->path('image', $c->image, 140)?>" />
            </div>
        </div>
        <?endif;?>
    </div>

    <?$END()?>

</div>