<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseProductList extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'purchase_id';
        $this->_export[] = 'managed';
        $this->_export[] = 'page';

        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }

        $m = Env::$app->m;

        $list = $m->purchaseProductListGet([
            'purchase_id' => $this->purchase_id,
            'page' => $this->page,
        ]);

        foreach($list['list'] as $i) {
            $this->insert('list', 'purchaseProduct', [
                'product_id' => $i['product_id'],
                'managed' => $this->managed,
            ]);
        }

        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalPurchaseProductRemove');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseProductUpdate');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseProductAdd');
    }

}
