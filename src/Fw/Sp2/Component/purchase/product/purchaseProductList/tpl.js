var self = this;


if (self.prop.managed) {
    this.dom.find('[data-action="purchase-product-add"]').click(function() {
        $.event.trigger('purchaseProductAddNeed', {
            purchase_id: self.prop.purchase_id
        });
    });
}

this.dom.bind('purchaseProductUpdateOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductAddOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseProductRemoveOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});

this.dom.bind('purchaseProductPaginationChange', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.prop.page = data.page;
        self.refresh();
    }
});