var self = this;


this.dom.bind('purchaseProductRemoveNeed', function(e, data) {
    self.prop.product_id = data.product_id;
    self.show();
});

this.purchaseProductRemove = function(data) {
    var ok = data.ok;
    data.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseProductRemoveOk(response.result);
    }
    data.url = '<?=$this->path("ajax", ["purchaseProductRemove"]);?>';

    self.ajax(data);
}

this.purchaseProductRemoveOk = function(data) {
    $.event.trigger('purchaseProductRemoveOk', data);
}