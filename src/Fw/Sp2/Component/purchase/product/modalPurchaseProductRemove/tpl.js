var self = this;

this.dom.find('[data-action="purchase-product-remove"]').click(function() {
    self.purchaseProductRemove({
        ok: function() {
            self.hide();
        },
        error: function() {},
        data: {
            product_id: self.prop.product_id
        }
    });
});
