var self = this;

this.dom.bind('purchaseProductAddNeed', function(e, data) {
    self.children('formPurchaseProductAdd')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseProductAddOk', function(e) {
    self.hide();
});