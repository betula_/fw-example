<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class ModalPurchaseProductAdd extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formPurchaseProductAdd');
    }

}
