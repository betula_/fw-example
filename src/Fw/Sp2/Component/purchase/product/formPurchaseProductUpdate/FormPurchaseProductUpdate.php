<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseProductUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'product_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'product_id';
        $this->_export[] = 'category_id';

        $item = null;
        if (!isset($this->product_id)) {
            $this->_template = 'empty';
            return;
        }
        $m = Env::$app->m;

        $item = $m->purchaseProductGetById(['product_id' => $this->product_id]);

        $this->image = $item['image'];
        $this->title = $item['title'];
        $this->order = $item['order'];
        $this->price = $item['price'];
        $this->currency = $item['currency'];
        $this->code = $item['code'];

        $this->category_id = $item['category_id'];

        $this->currencies = $m->currencyListGet()['list'];


        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
            'value' => $item['description'],
        ]);

        $this->insert('category', 'selectCategorySingle', [
            'columns' => 4,
            'category_id' => $item['category_id'],
        ]);

    }

}
