var self = this;

this.purchaseProductUpdate = function(options) {
    options.data.product_id = self.prop.product_id;
    options.data.category_id = self.prop.category_id;

    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseProductUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseProductUpdate"]);?>';

    self.ajax(options);
}

this.purchaseProductUpdateOk = function(data) {
    $.event.trigger('purchaseProductUpdateOk', data);
}