var self = this;

var form = this.dom.find('> form');

this.formInit(form, {
    send: function(data) {
        self.purchaseProductUpdate(data);
    }
});

this.dom.bind('selectCategorySingleOk', function(e, data) {
    e.stopPropagation();
    self.prop.category_id = data.category_id;
});
