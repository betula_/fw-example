var self = this;

this.purchaseProductAdd = function(options) {
    options.data.purchase_id = self.prop.purchase_id;
    options.data.category_id = self.prop.category_id;

    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseProductAddOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseProductAdd"]);?>';

    self.ajax(options);
}

this.purchaseProductAddOk = function(data) {
    $.event.trigger('purchaseProductAddOk', data);
}