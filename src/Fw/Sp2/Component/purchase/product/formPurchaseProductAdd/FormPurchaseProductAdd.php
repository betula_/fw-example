<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseProductAdd extends Form {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'purchase_id';
        $this->_export[] = 'category_id';

        $m = Env::$app->m;

        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }

        $this->currencies = $m->currencyListGet()['list'];

        $this->category_id = null;
        $this->currency = null;
        $item = $m->purchaseProductLastGet(['purchase_id' => $this->purchase_id]);
        if ($item) {
            $this->category_id = $item['category_id'];
            $this->currency = $item['currency'];
        }

        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
        ]);

        $this->insert('category', 'selectCategorySingle', [
            'columns' => 4,
            'category_id' => $this->category_id,
        ]);




    }

}
