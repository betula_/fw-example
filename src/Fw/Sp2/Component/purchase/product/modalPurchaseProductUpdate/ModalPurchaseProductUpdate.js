var self = this;

this.dom.bind('purchaseProductUpdateNeed', function(e, data) {
    self.children('formPurchaseProductUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseProductUpdateOk', function(e) {
    self.hide();
});