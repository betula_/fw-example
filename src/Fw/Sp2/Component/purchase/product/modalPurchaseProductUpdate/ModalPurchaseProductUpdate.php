<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class ModalPurchaseProductUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formPurchaseProductUpdate');
    }

}
