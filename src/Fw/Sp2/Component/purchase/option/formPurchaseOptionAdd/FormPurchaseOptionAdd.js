var self = this;

this.purchaseOptionAdd = function(options) {
    options.data.purchase_id = self.prop.purchase_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseOptionAddOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseOptionAdd"]);?>';

    self.ajax(options);
}

this.purchaseOptionAddOk = function(data) {
    $.event.trigger('purchaseOptionAddOk', data);
}