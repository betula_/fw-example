<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <form method="post" action="">

        <div class="alert alert-error hide" data-error="">
            <a class="close" data-dismiss="alert" href="#">×</a>
            Ошибка
        </div>

        <div class="row">
            <div class="span4">

                <div class="control-group" data-error="description">
                    <label>Тип значения</label>
                    <select name="kind_id">
                        <?foreach($c->kindList as $kind):?>
                        <option value="<?=$E($kind['kind_id'])?>"><?=$E($kind['title'])?></option>
                        <?endforeach;?>
                    </select>
                </div>

                <div class="control-group" data-error="description">
                    <label>Значение</label>
                    <?=$c->renderSlot('option')?>
                </div>

                <div class="control-group" data-error="order">
                    <label>Порядковый номер</label>
                    <input type="text" class="span2" name="order"/>
                </div>
            </div>
        </div>

    </form>

</div>