var self = this;

var form = this.dom.find('> form');

this.formInit(form, {
    send: function(data) {
        self.purchaseOptionAdd(data);
    }
});

form.find('select[name="kind_id"]').change(function(e) {
    var id = $(e.target).val();
    self.children('purchaseOptionEditor')[0].prop.kind_id = id;
    self.children('purchaseOptionEditor')[0].refresh();
});