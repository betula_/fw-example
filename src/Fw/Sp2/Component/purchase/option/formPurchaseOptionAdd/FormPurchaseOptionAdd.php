<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseOptionAdd extends Form {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'purchase_id';

        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }
        $m = Env::$app->m;

        $kinds = $m->purchaseOptionKindListGet()['list'];
        $this->kindList = $kinds;

        $this->insert('option', 'purchaseOptionEditor', [
            'kind_id' => $kinds[0]['kind_id'],
            'name' => 'value',
        ]);
    }

}
