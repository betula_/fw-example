var self = this;

this.purchaseOptionUpdate = function(options) {
    options.data.option_id = self.prop.option_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseOptionUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["purchaseOptionUpdate"]);?>';

    self.ajax(options);
}

this.purchaseOptionUpdateOk = function(data) {
    $.event.trigger('purchaseOptionUpdateOk', data);
}