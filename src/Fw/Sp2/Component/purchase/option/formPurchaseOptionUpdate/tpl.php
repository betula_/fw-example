<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <form method="post" action="">

        <div class="alert alert-error hide" data-error="">
            <a class="close" data-dismiss="alert" href="#">×</a>
            Ошибка
        </div>

        <div class="row">
            <div class="span4">

                <div class="control-group" data-error="description">
                    <label><?=$E($c->label)?></label>
                    <?=$c->renderSlot('option')?>
                </div>

                <div class="control-group" data-error="order">
                    <label>Порядковый номер</label>
                    <input type="text" class="span2" name="order" value="<?=$E($c->order)?>"/>
                </div>
            </div>
        </div>

    </form>

</div>