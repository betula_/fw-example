<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class FormPurchaseOptionUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'option_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'option_id';

        $item = null;
        if (!isset($this->option_id)) {
            $this->_template = 'empty';
            return;
        }

        $m = Env::$app->m;

        $option = $m->purchaseOptionGetById(['option_id' => $this->option_id]);
        $kind = $m->purchaseOptionKindGetById(['kind_id' => $option['kind_id']]);

        $this->insert('option', 'purchaseOptionEditor', [
            'kind_id' => $option['kind_id'],
            'name' => 'value',
            'value' => $option['value'],
        ]);

        $this->label = $kind['title'];
        $this->order = $option['order'];

    }

}
