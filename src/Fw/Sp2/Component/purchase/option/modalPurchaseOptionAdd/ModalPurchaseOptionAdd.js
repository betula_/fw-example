var self = this;

this.dom.bind('purchaseOptionAddNeed', function(e, data) {
    self.children('formPurchaseOptionAdd')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseOptionAddOk', function(e) {
    self.hide();
});