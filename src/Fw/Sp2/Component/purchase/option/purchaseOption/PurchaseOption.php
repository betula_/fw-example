<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseOption extends Component {

    public function properties() {
        return parent::properties() + [
            'option_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'option_id';
        $this->_export[] = 'managed';

        $image = null;
        if (!isset($this->option_id)) {
            $this->_template = 'empty';
            return;
        }

        $m = Env::$app->m;

        $option = $m->purchaseOptionGetById(['option_id' => $this->option_id]);

        $kind = $m->purchaseOptionKindGetById(['kind_id' => $option['kind_id']]);

        $this->type = $kind['type'];
        $this->title = $kind['title'];
        $this->value = $option['value'];

    }

}
