var self = this;

this.dom.bind('purchaseOptionUpdateOk', function(e, data) {
    if (data.option_id == self.prop.option_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="purchase-option-update"]').click(function() {
        $.event.trigger('purchaseOptionUpdateNeed', {
            option_id: self.prop.option_id
        });
    });
    this.dom.find('[data-action="purchase-option-remove"]').click(function() {
        $.event.trigger('purchaseOptionRemoveNeed', {
            option_id: self.prop.option_id
        });
    });
}
