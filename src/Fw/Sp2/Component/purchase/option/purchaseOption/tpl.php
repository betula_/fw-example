<div id="<?=$E($c->_id)?>" class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>">

    <?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-mini btn-info" data-action="purchase-option-update">
            <i class="icon-pencil icon-white"></i>
        </button>
        <button class="btn btn-mini btn-warning" data-action="purchase-option-remove">
            <i class="icon-remove icon-white"></i>
        </button>
    </div>
    <?endif;?>
    <?$END()?>

    <?$BLOCK('body')?>

    <?if($c->type == 'switch'):?>
    <?=$E($c->title)?>:
    <?if($c->value):?>Да<?else:?>Нет<?endif;?>

    <?elseif ($c->type == 'text'):?>
    <?=$E($c->title)?>: <?=$E($c->value)?>

    <?elseif ($c->type == 'uri'):?>
    <?=$E($c->title)?>: <a href="<?=$E($c->value)?>" target="_blank"><?=$E($c->value)?></a>

    <?endif;?>

    <?$END()?>

</div>