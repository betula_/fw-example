var self = this;


this.dom.bind('purchaseOptionRemoveNeed', function(e, data) {
    self.prop.option_id = data.option_id;
    self.show();
});

this.purchaseOptionRemove = function(data) {
    var ok = data.ok;
    data.ok = function(response) {
        if (ok) ok.call(this, response);
        self.purchaseOptionRemoveOk(response.result);
    }
    data.url = '<?=$this->path("ajax", ["purchaseOptionRemove"]);?>';

    self.ajax(data);
}

this.purchaseOptionRemoveOk = function(data) {
    $.event.trigger('purchaseOptionRemoveOk', data);
}