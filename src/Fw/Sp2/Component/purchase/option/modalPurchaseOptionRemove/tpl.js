var self = this;

this.dom.find('[data-action="purchase-option-remove"]').click(function() {
    self.purchaseOptionRemove({
        ok: function() {
            self.hide();
        },
        error: function() {},
        data: {
            option_id: self.prop.option_id
        }
    });
});
