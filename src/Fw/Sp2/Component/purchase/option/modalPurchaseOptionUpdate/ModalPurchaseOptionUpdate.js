var self = this;

this.dom.bind('purchaseOptionUpdateNeed', function(e, data) {
    self.children('formPurchaseOptionUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('purchaseOptionUpdateOk', function(e) {
    self.hide();
});