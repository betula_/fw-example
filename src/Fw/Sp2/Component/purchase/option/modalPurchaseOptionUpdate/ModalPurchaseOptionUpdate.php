<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class ModalPurchaseOptionUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formPurchaseOptionUpdate');
    }

}
