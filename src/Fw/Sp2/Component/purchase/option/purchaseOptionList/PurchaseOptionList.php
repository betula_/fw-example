<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseOptionList extends Component {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'purchase_id';
        $this->_export[] = 'managed';

        if (!isset($this->purchase_id)) {
            $this->_template = 'empty';
            return;
        }

        $m = Env::$app->m;

        $list = $m->purchaseOptionListGetByPurchaseId([
            'purchase_id' => $this->purchase_id,
        ]);

        foreach($list['list'] as $i) {
            $this->insert('items', 'purchaseOption', [
                'option_id' => $i['option_id'],
                'managed' => $this->managed,
            ]);
        }

        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalPurchaseOptionRemove');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseOptionUpdate');
        Env::$app->c->trigger('modalAdd', 'modalPurchaseOptionAdd');
    }

}
