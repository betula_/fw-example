var self = this;


if (self.prop.managed) {
    this.dom.find('[data-action="purchase-option-add"]').click(function() {
        $.event.trigger('purchaseOptionAddNeed', {
            purchase_id: self.prop.purchase_id
        });
    });
}

this.dom.bind('purchaseOptionUpdateOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseOptionAddOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});
this.dom.bind('purchaseOptionRemoveOk', function(e, data) {
    if (data.purchase_id == self.prop.purchase_id) {
        self.refresh();
    }
});