<div
    class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>"
    id="<?=$E($c->_id)?>">

<?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-mini btn-success" data-action="purchase-option-add">
            <i class="icon-plus icon-white"></i>
        </button>
    </div>
    <?endif;?>
<?$END()?>

<?$BLOCK('body')?>
    <ul>
        <?foreach($c->renderSlotComponents('items') as $i):?>
        <li><?=$i?></li>
        <?endforeach?>
    </ul>
<?$END()?>

</div>