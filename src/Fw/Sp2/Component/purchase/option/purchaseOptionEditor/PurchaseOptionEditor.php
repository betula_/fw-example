<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class PurchaseOptionEditor extends Component {

    public function properties() {
        return parent::properties() + [
            'kind_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'value' => [
                'string' => null,
                'default' => '',
            ],
            'name' => [
                'string' => null,
                'default' => 'value',
            ]
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'kind_id';
        $this->_export[] = 'name';

        if (!isset($this->kind_id)) {
            $this->_template = 'empty';
            return;
        }

        $m = Env::$app->m;

        $kind = $m->purchaseOptionKindGetById(['kind_id' => $this->kind_id]);

        $this->title = $kind['title'];
        $this->type = $kind['type'];
    }

}
