<div id="<?=$E($c->_id)?>" class="<?=$c->getCssClass()?>">

    <?if($c->type == 'switch'):?>

    <select name="<?=$E($c->name)?>">
        <option value="1" <?if($c->value):?>selected="selected"<?endif;?>>Да</option>
        <option value="" <?if(!$c->value):?>selected="selected"<?endif;?>>Нет</option>
    </select>

    <?elseif($c->type == 'text' || $c->type == 'uri'):?>

    <input type="text" name="<?=$E($c->name)?>" value="<?=$E($c->value)?>" placeholder="<?=$E($c->title)?>"  />

    <?endif;?>

</div>