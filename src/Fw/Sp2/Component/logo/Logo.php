<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Logo extends Component {

    public function properties() {
        return parent::properties() + [
            'inverse' => [
                'bool' => true,
                'default' => false,
            ],
        ];
    }

}
