
var config = {
    theme : 'advanced',
    theme_advanced_buttons1: 'undo,redo,|,bold,italic,underline,|,link,unlink,|,justifyleft,justifycenter,justifyright,|,pastetext,pasteword',
    theme_advanced_buttons2: 'tablecontrols',
    plugins: 'inlinepopups,paste,legacyoutput,contextmenu,autolink,table',
    theme_advanced_toolbar_location: 'top',
    theme_advanced_statusbar_location: 'none',
    skin_variant: 'silver',
    skin: 'o2k7',
    language : 'ru'
};
this.dom.children('textarea').tinymce(config);

