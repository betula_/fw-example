<?php
namespace Fw\Sp2\Component;
use Fw\M\Component\Content as ContentBasic;
use Fw\Fw\Env;

class Content extends ContentBasic {

    public function properties() {
        $props = parent::properties();
        $props['managed'] = null;
        return $props;
    }

    public function exec() {
        $this->managed = isset(Env::$app->m->user['role']['admin']);
        parent::exec();
    }

}
