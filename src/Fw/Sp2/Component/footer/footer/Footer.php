<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Footer extends Component {

    public function exec() {
        parent::exec();

        $this->insert('column1', 'footerProject');
        $this->insert('column2', 'footerForumMessageLastList');
        $this->insert('column3', 'footerAbout');
        $this->insert('column4', 'footerNotice');

        $this->insert('logo', 'logo', ['inverse'=>true]);
        $this->insert('copyright', 'footerCopyright');
    }

}
