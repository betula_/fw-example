<footer class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <div class="container">
        <div class="row">
            <div class="span3">
                <?=$c->renderSlot('column1');?>
            </div>
            <div class="span4">
                <?=$c->renderSlot('column2');?>
            </div>
            <div class="span2">
                <?=$c->renderSlot('column3');?>
            </div>
            <div class="span3">
                <?=$c->renderSlot('column4');?>
            </div>
        </div>

        <div class="bottom">
            <?=$c->renderSlot('logo')?>
            <?=$c->renderSlot('copyright')?>
        </div>
    </div>
</footer>
