<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class FooterAbout extends Component {

    public function exec() {
        parent::exec();

        $this->insert('content', 'content', [
            'name' => 'footerAbout',
        ]);

    }

}
