<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class NavUser extends Component {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        if (!Env::$app->sp->user) {
            $this->_template = 'empty';
            return;
        }

        $sp = Env::$app->sp;
        $m = Env::$app->m;

        $this->userNameReal = $sp->user['realName'];
        $this->unreadMessageCount = $sp->user['unreadMessages'];


        $this->userRoleOrg = isset($m->user['role']['org']);
        $this->userId = $m->user['user_id'];

    }

}
