<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <ul class="nav">
                    <li><a href="#" class="user-name"><b><?=$E($c->userNameReal)?></b></a></li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a href="<?=$c->path('pagePersonalDialogList')?>">
                            <i class="icon-envelope"></i>
                            <span class="badge badge-important over"><?=$E($c->unreadMessageCount)?></span>
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a href="#">
                            <i class="icon-user"></i>
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a href="#">
                            <i class="icon-cog"></i>
                        </a>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <a href="<?=$c->path('pagePurchaseList')?>">Закупки</a>
                    </li>
                    <li>
                        <a href="<?=$c->path('pagePurchaseList', ['owner_id' => $c->userId])?>">Мои Закупки</a>
                    </li>
                </ul>

                <ul class="nav pull-right">
                    <li><a href="#" class="sign-out" data-action="sign-out"><i class="icon-exit"></i><b>Выход</b></a></li>
                </ul>


            </div>
        </div>
    </div>


</div>
