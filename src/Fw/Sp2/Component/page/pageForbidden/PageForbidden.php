<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PageForbidden extends Page {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        $this->title = 'Forbidden';

        $this->insert('right', 'content', ['name'=>'pageForbidden']);
    }

}
