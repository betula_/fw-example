<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PagePurchaseList extends Page {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function insertRight() {
        $this->insert('right', 'purchaseListBlock', [
            'page' => $this->page,
            'owner_id' => $this->owner_id,
            'category_id' => $this->category_id,
        ]);
    }

}
