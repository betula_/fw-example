<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PagePersonalDialog extends PagePersonal {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'default' => null,
            ],
            'opponent_id' => [
                'default' => null,
            ]
        ];
    }

    public function insertRight() {
        $this->insert('right', 'personalDialogBlock', [
            'page' => $this->page,
            'opponent_id' => $this->opponent_id,
        ]);
    }

}
