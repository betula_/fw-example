<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;
use Fw\System\RedirectException;

class PagePersonal extends Page {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        Env::$app->c->trigger('titleChange', 'Личный кабинет');

        $user = Env::$app->sp->user;
        if (!$user) {
            throw new RedirectException(
                Env::$app->router->path('pageForbidden'),
                403
            );
        }
    }

    public function insertMenuCategory() {
    }

}
