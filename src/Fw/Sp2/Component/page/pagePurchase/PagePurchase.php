<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PagePurchase extends Page {

    public function properties() {
        return parent::properties() + [
            'purchase_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function insertRight() {
        $this->insert('right', 'purchaseBlock', [
            'purchase_id' => $this->purchase_id,
        ]);
    }

}
