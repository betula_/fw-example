<?php
namespace Fw\Sp2\Component;
use Fw\Fw\Env;

class PagePersonalDialogList extends PagePersonal {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'default' => null,
            ],
        ];
    }

    public function insertRight() {
        $this->insert('right', 'personalDialogListBlock', [
            'page' => $this->page,
        ]);
    }

}
