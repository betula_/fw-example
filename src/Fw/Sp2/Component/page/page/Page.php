<?php
namespace Fw\Sp2\Component;
use Fw\M\Component\Page as PageBasic;
use Fw\Fw\Env;

class Page extends PageBasic {

    public $tinymce = false;

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        Env::$app->c->trigger('titleChange', 'Совместные покупки Мам Ростова');

        $this->insertHeader();
        $this->insertMiddle();

        $this->insertLeft();
        $this->insertRight();

        $this->insertFooter();
        $this->insertModal();

        $this->tinymce = isset(Env::$app->m->user['role']['admin']) || isset(Env::$app->m->user['role']['org']);



    }

    public function insertHeader() {
        $this->insert('header', 'navUser');
        $this->insert('header', 'header');
    }

    public function insertMiddle() {
        $this->insert('middle', 'bannerRow');
    }

    public function insertLeft() {
        $this->insertMenuCategory();
    }

    public function insertMenuCategory() {
        $this->insert('left','purchaseCategoryMenu');
    }

    public function insertRight() {

    }

    public function insertFooter() {
        $this->insert('footer', 'footer');
    }

    public function insertModal() {
        if (!Env::$app->sp->user) {
            Env::$app->c->trigger('modalAdd', 'modalAuthByEmailPass');
        }
        Env::$app->c->trigger('modalAdd', 'authEnd');
    }


}
