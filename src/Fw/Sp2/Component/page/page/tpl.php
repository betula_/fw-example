<?$BLOCK('head')?>
<link rel="stylesheet" href="/res/bootstrap/css/bootstrap.css?<?=$this->version?>" />
<script type="text/javascript" language="javascript" src="/res/bootstrap/js/bootstrap.min.js?<?=$this->version?>"></script>


<link href="http://fonts.googleapis.com/css?family=Ubuntu+Condensed&subset=cyrillic,latin" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/res/theme/theme.css?<?=$this->version?>" />

<?if($c->tinymce):?>
<script type="text/javascript" src="/res/tinymce/jscripts/tiny_mce/jquery.tinymce.js?<?=$this->version?>"></script>
<script type="text/javascript" src="/res/tinymce/jscripts/tiny_mce/tiny_mce.js?<?=$this->version?>"></script>
<?endif?>
<?$END()?>

<?$BLOCK('middle')?>
<div class="container">
    <?$PARENT()?>

    <div class="row">
        <div class="span3">
            <?=$c->renderSlot('left')?>
        </div>
        <div class="span9">
            <?=$c->renderSlot('right')?>
        </div>
    </div>
</div>
<?$END()?>
