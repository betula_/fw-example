<?php
namespace Fw\Sp2\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Header extends Component {

    public function exec() {
        parent::exec();

        $this->insert('logo', 'logo');
        $this->insert('link', 'helpLink');

        $this->authorized = false;
        if (Env::$app->sp->user) {
            $this->authorized = true;
        }
    }

}
