<header class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <div class="container">
        <div class="logo">
            <?=$c->renderSlot('logo')?>
        </div>

        <div class="link">
            <?=$c->renderSlot('link')?>
        </div>

        <div class="forum">
            <a href="#" class="btn">Форум</a>
        </div>

        <?if(!$c->authorized):?>
        <div class="sign-in">
            <button class="btn">Вход</button>
        </div>
        <?endif;?>

        <div class="search">

        </div>

    </div>
</header>
