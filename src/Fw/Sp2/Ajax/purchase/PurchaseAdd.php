<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseAdd extends ApiCall {

    public function args() {
        return [
            'title' => [
                'string' => [
                    'length' => ['min'=>1, 'max'=>255],
                ],
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'default' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = [
            'title' => $args['title'],
        ];

        if (isset($args['_files']['image'])) {
            $imageUid = $this->api->createUid().'.jpg';
            $path = Env::$app->storage->path($imageUid, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('jpeg');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $imageUid;
        }

        $purchase = Env::$app->m->purchaseAdd($params);
        return [
            'purchase_id' => $purchase['purchase_id'],
        ];
    }

}
