<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseUpdate extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ]
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'purchase_id', 'title', 'description'
        ], true));

        if (isset($args['_files']['image'])) {
            $imageUid = $this->api->createUid().'.jpg';
            $path = Env::$app->storage->path($imageUid, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('jpeg');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $imageUid;
        }

        $purchase = Env::$app->m->purchaseUpdateById($params);
        return [
            'purchase_id' => $purchase['purchase_id'],
        ];
    }

}
