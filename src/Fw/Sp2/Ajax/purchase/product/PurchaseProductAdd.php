<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseProductAdd extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'order' => [
                'string' => null,
                'default' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'default' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'description' => [
                'string' => null,
                'default' => null,
            ],
            'code' => [
                'string' => ['length'=>['max'=>50]],
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'currency' => [
                'currency' => null,
                'default' => Env::$app->cfg['currencyDefault'],
            ],
            'price' => [
                'price' => null,
                'default' => 0,
            ],
        ];
    }

    public function perform($args) {

        $params = [
            'purchase_id' => $args['purchase_id'],
            'order' => $args['order'] ? $args['order'] : null,
            'title' => $args['title'],
            'description' => $args['description'],
            'category_id' => $args['category_id'] ? $args['category_id'] : null,
            'code' => $args['code'],
            'currency' => $args['currency'],
            'price' => $args['price'],
        ];

        if (isset($args['_files']['image'])) {
            $imageUid = $this->api->createUid().'.jpg';
            $path = Env::$app->storage->path($imageUid, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('jpeg');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $imageUid;
        }

        $product = Env::$app->m->purchaseProductAdd($params);
        return [
            'purchase_id' => $params['purchase_id'],
            'product_id' => $product['product_id'],
        ];
    }

}
