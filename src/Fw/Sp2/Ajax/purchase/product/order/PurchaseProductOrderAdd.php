<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseProductOrderAdd extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
            'count' => [
                'int' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = [
            'product_id' => $args['product_id'],
            'count' => $args['count'],
        ];

        $order = Env::$app->m->purchaseProductOrderAdd($params);
        return [
            'product_id' => $params['product_id'],
            'order_id' => $order['order_id'],
        ];
    }

}
