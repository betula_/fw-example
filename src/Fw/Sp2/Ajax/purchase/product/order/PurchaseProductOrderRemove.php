<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseProductOrderRemove extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform($args) {
        $m = Env::$app->m;

        if (!isset($m->user)) {
            throw new DeniedException();
        }

        $m->purchaseProductOrderRemove([
            'product_id' => $args['product_id'],
            'user_id' => $m->user['user_id'],
        ]);
        return [
            'product_id' => $args['product_id'],
        ];
    }

}
