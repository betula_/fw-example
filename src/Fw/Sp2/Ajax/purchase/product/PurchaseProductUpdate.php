<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseProductUpdate extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
            'order' => [
                'string' => null,
                'optional' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ],
            'code' => [
                'string' => ['length'=>['max'=>50]],
                'optional' => null,
            ],
            'category_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'currency' => [
                'currency' => null,
                'optional' => null,
            ],
            'price' => [
                'price' => null,
                'optional' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'product_id', 'title', 'description', 'code', 'currency', 'price', 'category_id',
        ], true));

        if (isset($args['order']) && ($args['order'] || $args['order'] === '0')) {
            $params['order'] = $args['order'];
        }

        if (isset($args['_files']['image'])) {
            $imageUid = $this->api->createUid().'.jpg';
            $path = Env::$app->storage->path($imageUid, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('jpeg');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $imageUid;
        }

        $m = Env::$app->m;
        $result = $m->purchaseProductUpdateById($params);
        $product = $m->purchaseProductGetById(['product_id' => $result['product_id']]);

        return [
            'product_id' => $product['product_id'],
            'purchase_id' => $product['purchase_id'],
        ];
    }

}
