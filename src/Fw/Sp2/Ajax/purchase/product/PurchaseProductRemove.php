<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseProductRemove extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform($args) {
        $m = Env::$app->m;
        $product = $m->purchaseProductGetById(['product_id' => $args['product_id']]);
        $m->purchaseProductRemoveById([
            'product_id' => $product['product_id'],
        ]);
        return [
            'product_id' => $product['product_id'],
            'purchase_id' => $product['purchase_id'],
        ];
    }

}
