<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseRemove extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform($args) {
        $purchase = Env::$app->m->purchaseGetById(['purchase_id' => $args['purchase_id']]);
        Env::$app->m->purchaseRemoveById([
            'purchase_id' => $purchase['purchase_id'],
        ]);
        return [
            'purchase_id' => $purchase['purchase_id'],
        ];
    }

}
