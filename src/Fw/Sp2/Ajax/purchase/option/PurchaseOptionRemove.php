<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PurchaseOptionRemove extends ApiCall {

    public function args() {
        return [
            'option_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform($args) {
        $m = Env::$app->m;
        $option = $m->purchaseOptionGetById(['option_id' => $args['option_id']]);
        Env::$app->m->purchaseOptionRemoveById([
            'option_id' => $option['option_id'],
        ]);
        return [
            'option_id' => $option['option_id'],
            'purchase_id' => $option['purchase_id'],
        ];
    }

}
