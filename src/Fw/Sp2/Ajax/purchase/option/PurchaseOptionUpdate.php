<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;

class PurchaseOptionUpdate extends ApiCall {

    public function args() {
        return [
            'option_id' => [
                'uid' => null,
            ],
            'kind_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'order' => [
                'string' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'value' => [
                'string' => null,
                'optional' => null,
            ]
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'option_id', 'title', 'value'
        ], true));

        if (isset($args['order']) && ($args['order'] || $args['order'] === '0')) {
            $params['order'] = $args['order'];
        }
        if (isset($args['kind_id']) && $args['kind_id']) {
            $params['kind_id'] = $args['kind_id'];
        }

        $m = Env::$app->m;
        $result = $m->purchaseOptionUpdateById($params);
        $option = $m->purchaseOptionGetById(['option_id' => $result['option_id']]);

        return [
            'option_id' => $option['option_id'],
            'purchase_id' => $option['purchase_id'],
        ];
    }

}
