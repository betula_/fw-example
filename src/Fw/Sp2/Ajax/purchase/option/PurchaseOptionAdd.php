<?php
namespace Fw\Sp2\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;

class PurchaseOptionAdd extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'kind_id' => [
                'uid' => null,
            ],
            'order' => [
                'string' => null,
                'default' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'value' => [
                'string' => null,
            ],
        ];
    }

    public function perform($args) {

        $m = Env::$app->m;
        $option = $m->purchaseOptionAdd([
            'purchase_id' => $args['purchase_id'],
            'kind_id' => $args['kind_id'],
            'order' => $args['order'] ? $args['order'] : null,
            'title' => $args['title'],
            'value' => $args['value'],
        ]);
        return [
            'option_id' => $option['option_id'],
            'purchase_id' => $args['purchase_id'],
        ];
    }

}
