<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseUpdateById extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'org_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'image' => [
                'string' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $purchase = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);
        $this->permit($args + [
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $original = $purchase;

        $values[':purchase_id'] = $args['purchase_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';

        foreach(['org_id', 'title', 'description', 'image'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update Purchase set %s where purchase_id=:purchase_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return [
            'purchase_id' => $args['purchase_id'],
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

}