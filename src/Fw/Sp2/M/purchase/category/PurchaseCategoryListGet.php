<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseCategoryListGet extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'purchase_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function perform ($args) {

        $cacheKey = $args['category_id'].':'.$args['purchase_id'];
        $rows = $this->cacheGet($cacheKey);
        if (!$rows) {

            $where = [];
            $values = [];
            if (isset($args['category_id'])) {
                $where[] = 'category_id=:category_id';
                $values[':category_id'] = $args['category_id'];
            }
            if (isset($args['purchase_id'])) {
                $where[] = 'purchase_id=:purchase_id';
                $values[':purchase_id'] = $args['purchase_id'];
            }

            $query = 'select * from PurchaseCategory';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by dtm desc';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $this->cacheSet($cacheKey, $rows, 1);
        }

        return [
            'list' => $rows,
        ];
    }

}