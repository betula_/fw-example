<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseCategoryRemove extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (isset($this->api->user['role']['admin'])) {
            return;
        }
        throw new DeniedException();
    }

    public function perform ($args) {
        $purchase = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);

        $args['owner_id'] = null;
        if (isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $this->permit($args + [
            'purchase' => $purchase,
        ]);

        if (!isset($args['category_id'])) {
            return [];
        }

        $stmt = $this->api->db->prepare('
            select `count` from PurchaseCategory
            where category_id=:category_id and purchase_id=:purchase_id
            limit 1
        ');
        $stmt->execute([
            ':category_id' => $args['category_id'],
            ':purchase_id' => $args['purchase_id'],
        ]);
        $link = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$link) {
            return [];
        }

        if ($link['count'] > 1) {
            $stmt = $this->api->db->prepare('update PurchaseCategory set dtm=:time, `count`:=`count`-1');
            $stmt->execute([
                ':time' => $this->api->time,
            ]);
        }
        else {
            $stmt = $this->api->db->prepare('
                delete from PurchaseCategory
                where category_id=:category_id and purchase_id=:purchase_id
            ');
            $stmt->execute([
                ':purchase_id' => $args['purchase_id'],
                ':category_id' => $args['category_id'],
            ]);

            $this->categoryPurchaseCountChange([
                'category_id' => $args['category_id'],
                'delta' => -1,
            ]);
        }

        return [];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function categoryPurchaseCountChange(array $args = []) {
        return $this->api->categoryPurchaseCountChange($args);
    }

}