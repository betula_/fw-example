<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionRemoveByPurchaseId extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $list = $this->optionListGetByPurchase(['purchase_id' => $args['purchase_id']])['list'];
        foreach($list as $i) {
            $this->optionRemoveById(['option_id' => $i['option_id']]);
        }
    }

    public function optionListGetByPurchase(array $args = []) {
        return $this->api->purchaseOptionListGetByPurchaseId($args);
    }

    public function optionRemoveById(array $args = []) {
        return $this->api->purchaseOptionRemoveById($args);
    }


}