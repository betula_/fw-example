<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseOptionAdd extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'kind_id' => [
                'uid' => null,
            ],
            'value' => [
                'string' => [
                    'length' => ['max' => 1024],
                ],
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'order' => [
                'int' => null,
                'default' => $this->api->time,
            ]
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if (isset($this->api->user['role']['admin'])) {
            return;
        }
        throw new DeniedException();
    }

    public function perform ($args) {
        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }
        $purchase = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);

        $this->permit($args + [
            'purchase' => $purchase,
        ]);
        $kind = $this->optionKindGetById(['kind_id' => $args['kind_id']]);

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into PurchaseOption
            ( option_id,  dtc,  dtm,  purchase_id,  kind_id,  value,  owner_id, `order`) values
            (:uid,       :time,:time,:purchase_id, :kind_id, :value, :owner_id, :order )');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':purchase_id' => $args['purchase_id'],
            ':kind_id' => $args['kind_id'],
            ':value' => $args['value'],
            ':owner_id' => $args['owner_id'],
            ':order' => $args['order'],
        ]);

        return [
            'option_id' => $uid,
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function optionKindGetById(array $args = []) {
        return $this->api->purchaseOptionKindGetById($args);
    }

}