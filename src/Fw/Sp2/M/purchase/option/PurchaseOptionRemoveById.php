<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseOptionRemoveById extends ApiCall {

    public function args() {
        return [
            'option_id' => [
                'uid' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['option']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $option = $this->optionGetById(['option_id' => $args['option_id']]);
        $purchase = $this->purchaseGetById(['purchase_id' => $option['purchase_id']]);

        $this->permit($args + [
            'option' => $option,
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $stmt = $this->api->db->prepare('delete from PurchaseOption where option_id = ?');
        $stmt->execute([$args['option_id']]);
        return [];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function optionGetById(array $args = []) {
        return $this->api->purchaseOptionGetById($args);
    }


}