<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionKindListGet extends ApiCall {

    public function args() {
        return [
        ];
    }

    public function perform ($args) {
        $cacheKey = '';
        $rows = $this->cacheGet($cacheKey);

        if (!$rows) {
            $stmt = $this->api->db->prepare('select
              kind_id
              from PurchaseOptionKind
              order by `order`');
            $stmt->execute();
            $rows = $stmt->fetchAll();
            $this->cacheSet($cacheKey, $rows);
        }

        $list = [];
        foreach ($rows as $row) {
            $list[] = $this->optionKindGetById(['kind_id' => $row['kind_id']]);
        }

        return [
            'list' => $list,
        ];
    }

    public function optionKindGetById(array $args = []) {
        return $this->api->purchaseOptionKindGetById($args);
    }

}