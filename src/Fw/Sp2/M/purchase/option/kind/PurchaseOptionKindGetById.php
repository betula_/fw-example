<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionKindGetById extends ApiCall {

    public function args() {
        return [
            'kind_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['kind_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from PurchaseOptionKind where kind_id = ? limit 1');
        $stmt->execute([$args['kind_id']]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new ApiCallInvalidException($this, [
                'kind_id' => ['not_found' => null],
            ], 404);
        }

        $this->cacheSet($cacheKey, $result);
        return $result;
    }

}