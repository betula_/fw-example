<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseOptionKindAdd extends ApiCall {

    public function args() {
        return [
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'name' => [
                'name' => null,
                'default' => null,
            ],
            'type' => [
                'purchaseOptionType' => null,
                'default' => null,
            ],
            'order' => [
                'int' => null,
                'default' => $this->api->time,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $uid = $this->api->createUid();
        $name = $args['name'];
        if (!isset($name)) {
             $name = $uid;
        } else {
            $stmt = $this->api->db->prepare('select kind_id from PurchaseOptionKind where `name`=:name limit 1');
            $stmt->execute([':name' => $args['name']]);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                throw new ApiCallInvalidException($this, [
                    'name' => ['exists' => true],
                ]);
            }
        }

        $stmt = $this->api->db->prepare('insert into PurchaseOptionKind
            ( kind_id,    dtc,  dtm,  `name`, `title`,  owner_id, `order`,  type) values
            (:uid,       :time,:time, :name,  :title,  :owner_id, :order,  :type)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':name' => $name,
            ':title' => $args['title'],
            ':owner_id' => $args['owner_id'],
            ':order' => $args['order'],
            ':type' => $args['type'],
        ]);

        return [
            'kind_id' => $uid,
        ];
    }

}