<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionKindGetByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['name'];
        $kindId = $this->cacheGet($cacheKey);

        if (!$kindId) {
            $stmt = $this->api->db->prepare('select * from PurchaseOptionKind where name = ? limit 1');
            $stmt->execute([$args['name']]);
            $kind = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$kind) {
                throw new ApiCallInvalidException($this, array(
                    'name' => array(
                        'not_found' => null,
                    ),
                ));
            }
            $kindId = $kind['kind_id'];
            $this->cacheSet($cacheKey, $kindId);
        }

        $kind = $this->optionKindGetById(['kind_id' => $kindId]);
        return $kind;
    }

    public function optionKindGetById(array $args = []) {
        return $this->api->purchaseOptionKindGetById($args);
    }

}