<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionGetById extends ApiCall {

    public function args() {
        return [
            'option_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        //$cacheKey = $args['option_id'];
        //$cache = $this->cacheGet($cacheKey);
        //if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from PurchaseOption where option_id = ? limit 1');
        $stmt->execute([$args['option_id']]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new ApiCallInvalidException($this, [
                'option_id' => ['not_found' => null],
            ], 404);
        }

        //$this->cacheSet($cacheKey, $result);
        return $result;
    }

}