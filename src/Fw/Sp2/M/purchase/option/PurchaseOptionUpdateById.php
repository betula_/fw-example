<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseOptionUpdateById extends ApiCall {

    public function args() {
        return [
            'option_id' => [
                'uid' => null,
            ],
            'value' => [
                'string' => [
                    'length' => ['max' => 1024],
                ],
                'optional' => null,
            ],
            'order' => [
                'int' => null,
                'optional' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['option']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $option = $this->optionGetById(['option_id' => $args['option_id']]);
        $purchase = $this->purchaseGetById(['purchase_id' => $option['purchase_id']]);

        $this->permit($args + [
            'option' => $option,
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $original = $option;

        $values[':option_id'] = $args['option_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';

        foreach(['value', 'order'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update PurchaseOption set %s where option_id=:option_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return [
            'option_id' => $args['option_id'],
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function optionGetById(array $args = []) {
        return $this->api->purchaseOptionGetById($args);
    }

}