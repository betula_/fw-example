<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseOptionListGetByPurchaseId extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'];
        $rows = $this->cacheGet($cacheKey);

        if (!$rows) {
            $stmt = $this->api->db->prepare('select
              option_id
              from PurchaseOption
              where purchase_id=:purchase_id order by `order`');
            $stmt->execute([
                ':purchase_id' => $args['purchase_id'],
            ]);
            $rows = $stmt->fetchAll();

            //$this->cacheSet($cacheKey, $rows);
        }

        $list = [];
        foreach ($rows as $row) {
            $list[] = $this->optionGetById(['option_id' => $row['option_id']]);
        }

        return [
            'list' => $list,
        ];
    }

    public function optionGetById(array $args = []) {
        return $this->api->purchaseOptionGetById($args);
    }


}