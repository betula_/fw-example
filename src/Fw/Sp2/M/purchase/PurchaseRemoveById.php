<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseRemoveById extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $purchase = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);

        $this->permit($args + [
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $this->purchaseProductRemoveByPurchaseId(['purchase_id' => $args['purchase_id']]);
        $this->purchaseOptionRemoveByPurchaseId(['purchase_id' => $args['purchase_id']]);

        $stmt = $this->api->db->prepare('delete from Purchase where purchase_id = ?');
        $stmt->execute([$args['purchase_id']]);

        return [];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function purchaseProductRemoveByPurchaseId(array $args = []) {
        return $this->api->purchaseProductRemoveByPurchaseId($args);
    }

    public function purchaseOptionRemoveByPurchaseId(array $args = []) {
        return $this->api->purchaseOptionRemoveByPurchaseId($args);
    }

}