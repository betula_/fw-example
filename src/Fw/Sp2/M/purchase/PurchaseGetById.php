<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseGetById extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from Purchase where purchase_id = ? limit 1');
        $stmt->execute([$args['purchase_id']]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new ApiCallInvalidException($this, [
                'purchase_id' => ['not_found' => null],
            ], 404);
        }

        //$this->cacheSet($cacheKey, $result);
        return $result;
    }

}