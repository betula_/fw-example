<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseAdd extends ApiCall {

    public function args() {
        return [
            'org_id' => [
                'uid' => null,
                'default' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'description' => [
                'string' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'image' => [
                'string' => null,
                'default' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (isset($this->api->user['role']['org'])) {
            return;
        }
        if (isset($this->api->user['role']['admin'])) {
            return;
        }
        throw new DeniedException();
    }

    public function perform ($args) {
        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }
        if (!isset($args['org_id'])) {
            $args['org_id'] = $args['owner_id'];
        }
        $this->permit($args);

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Purchase
            ( purchase_id,  dtc,  dtm,  title,  description,  org_id, owner_id, image) values
            (:uid,         :time,:time,:title, :description, :org_id, :owner_id,:image )');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':title' => $args['title'],
            ':description' => $args['description'],
            ':org_id' => $args['org_id'],
            ':owner_id' => $args['owner_id'],
            ':image' => $args['image'],
        ]);


        return [
            'purchase_id' => $uid,
        ];
    }

}