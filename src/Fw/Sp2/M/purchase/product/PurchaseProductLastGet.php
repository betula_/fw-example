<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductLastGet extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'];
        $row = $this->cacheGet($cacheKey);

        if (!$row) {

            $where = [];
            $values = [];

            $where[] = 'purchase_id=:purchase_id';
            $values[':purchase_id'] = $args['purchase_id'];

            $query = 'select product_id from PurchaseProduct';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by dtc desc limit 1';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            //$this->cacheSet($cacheKey, $rows, 3);
        }

        if (!$row) {
            return null;
        }

        $item = $this->productGetById(['product_id' => $row['product_id']]);
        return $item;

    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }


}