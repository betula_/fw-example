<?php
namespace Fw\Sp2\M;
use Fw\M\M\PaginationGet;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductListPaginationGet extends PaginationGet {

    public function args() {
        return parent::args() + [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function total($args) {
        $count = $this->api->purchaseProductListCountGet(['purchase_id' => $args['purchase_id']]);
        return $count['total'];
    }

}