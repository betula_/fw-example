<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductAdd extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'description' => [
                'string' => null,
                'default' => null,
            ],
            'image' => [
                'string' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'order' => [
                'int' => null,
                'default' => $this->api->time,
            ],
            'code' => [
                'string' => ['length'=>['max'=>50]],
                'default' => null,
            ],
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'currency' => [
                'currency' => null,
                'default' => Env::$app->cfg['currencyDefault'],
            ],
            'price' => [
                'price' => null,
                'default' => 0,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (isset($this->api->user['role']['admin'])) {
            return;
        }
        throw new DeniedException();
    }

    public function perform ($args) {
        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }
        $purchase = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);

        if ($args['category_id']) {
            $category = $this->categoryGetById(['category_id' => $args['category_id']]);
        }

        $this->permit($args + [
            'purchase' => $purchase,
        ]);


        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into PurchaseProduct
            ( product_id,   dtc,  dtm,  purchase_id,  title,  description,  image,  owner_id, `order`,  code, category_id,  currency, price) values
            (:uid,         :time,:time,:purchase_id, :title, :description, :image, :owner_id, :order,  :code,:category_id, :currency,:price)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':purchase_id' => $args['purchase_id'],
            ':title' => $args['title'],
            ':description' => $args['description'],
            ':image' => $args['image'],
            ':owner_id' => $args['owner_id'],
            ':order' => $args['order'],
            ':code' => $args['code'],
            ':category_id' => $args['category_id'],
            ':currency' => $args['currency'],
            ':price' => $args['price'],
        ]);

        $this->purchaseCategoryAdd([
            'purchase_id' => $args['purchase_id'],
            'category_id' => $args['category_id'],
        ]);

        return [
            'product_id' => $uid,
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

    public function purchaseCategoryAdd(array $args = []) {
        return $this->api->purchaseCategoryAdd($args);
    }

}