<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductListGet extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'].':'.$args['count'].':'.$args['page'];
        $rows = $this->cacheGet($cacheKey);

        if (!$rows) {

            $where = [];
            $values = [];

            $where[] = 'purchase_id=:purchase_id';
            $values[':purchase_id'] = $args['purchase_id'];

            $query = 'select product_id from PurchaseProduct';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by `order` limit :from, :count';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
            $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //$this->cacheSet($cacheKey, $rows, 3);
        }

        $list = [];
        foreach($rows as $i) {
            $list[] = $this->productGetById(['product_id' => $i['product_id']]);
        }

        return [
            'list' => $list,
        ];

    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }


}