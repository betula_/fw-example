<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductGetById extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['product_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from PurchaseProduct where product_id = ? limit 1');
        $stmt->execute([$args['product_id']]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new ApiCallInvalidException($this, [
                'product_id' => ['not_found' => null],
            ], 404);
        }

        //$this->cacheSet($cacheKey, $result);
        return $result;
    }

}