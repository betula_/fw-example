<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductRemoveById extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['product']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $product = $this->productGetById(['product_id' => $args['product_id']]);
        $purchase = $this->purchaseGetById(['purchase_id' => $product['purchase_id']]);

        $this->permit($args + [
            'product' => $product,
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $this->orderRemove(['product_id' => $product['product_id']]);

        $this->purchaseCategoryRemove([
            'purchase_id' => $product['purchase_id'],
            'category_id' => $product['category_id'],
        ]);

        $stmt = $this->api->db->prepare('delete from PurchaseProduct where product_id = ?');
        $stmt->execute([$args['product_id']]);

        return [];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }

    public function orderRemove(array $args = []) {
        return $this->api->purchaseProductOrderRemove($args);
    }

    public function purchaseCategoryRemove(array $args = []) {
        return $this->api->purchaseCategoryRemove($args);
    }


}