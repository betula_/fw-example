<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductUpdateById extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ],
            'image' => [
                'string' => null,
                'optional' => null,
            ],
            'order' => [
                'int' => null,
                'optional' => null,
            ],
            'code' => [
                'string' => ['length'=>['max'=>50]],
                'optional' => null,
            ],
            'category_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'currency' => [
                'currency' => null,
                'optional' => null,
            ],
            'price' => [
                'price' => null,
                'optional' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['product']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $product = $this->productGetById(['product_id' => $args['product_id']]);
        $purchase = $this->purchaseGetById(['purchase_id' => $product['purchase_id']]);

        if (isset($args['category_id'])) {
            $category = $this->categoryGetById(['category_id' => $args['category_id']]);
        }

        $this->permit($args + [
            'product' => $product,
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $original = $product;

        $values[':product_id'] = $args['product_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';

        foreach(['title', 'description', 'image', 'order', 'code', 'category_id', 'currency', 'price'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update PurchaseProduct set %s where product_id=:product_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        if (isset($args['category_id']) && $original['category_id'] != $args['category_id']) {
            $this->purchaseCategoryAdd([
                'purchase_id' => $product['purchase_id'],
                'category_id' => $args['category_id'],
            ]);
            $this->purchaseCategoryRemove([
                'purchase_id' => $product['purchase_id'],
                'category_id' => $original['category_id'],
            ]);
        }

        return [
            'product_id' => $args['product_id'],
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }

    public function purchaseCategoryAdd(array $args = []) {
        return $this->api->purchaseCategoryAdd($args);
    }

    public function purchaseCategoryRemove(array $args = []) {
        return $this->api->purchaseCategoryRemove($args);
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

}