<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductOrderUpdateById extends ApiCall {

    public function args() {
        return [
            'order_id' => [
                'uid' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 1,
            ],
            'date' => [
                'int' => null,
                'default' => $this->api->time,
            ],
        ];
    }

    public function permit(array $args = []) {
        if ($args['owner_id'] === $args['order']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['order']['user_id']) {
            return;
        }
        if ($args['owner_id'] === $args['product']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['owner_id']) {
            return;
        }
        if ($args['owner_id'] === $args['purchase']['org_id']) {
            return;
        }
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        if (isset($args['count'])) {
            if ($args['count'] < 1) {
                throw new ApiCallInvalidException($this, [
                    'count' => [
                        'invalid' => true,
                    ],
                ]);
            }
        }

        $order = $this->orderGetById(['order_id' => $args['order_id']]);
        $purchase = $this->purchaseGetById(['purchase_id' => $order['purchase_id']]);
        $product = $this->productGetById(['product_id' => $order['product_id']]);

        $this->permit($args + [
            'order' => $order,
            'product' => $product,
            'purchase' => $purchase,
            'owner_id' => $this->api->user['user_id'],
        ]);

        $original = $order;

        $values[':order_id'] = $args['order_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';

        foreach(['count', 'date'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update PurchaseProductOrder set %s where order_id=:order_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return [
            'order_id' => $args['order_id'],
            'product_id' => $product['product_id'],
            'purchase_id' => $purchase['purchase_id'],
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }

    public function orderGetById(array $args = []) {
        return $this->api->purchaseProductOrderGetById($args);
    }

}