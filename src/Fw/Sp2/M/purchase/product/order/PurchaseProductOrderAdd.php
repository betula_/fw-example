<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductOrderAdd extends ApiCall {

    public function args() {
        return [
            'product_id' => [
                'uid' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 1,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'date' => [
                'int' => null,
                'default' => $this->api->time,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (isset($this->api->user)) {
            return;
        }
        throw new DeniedException();
    }

    public function perform ($args) {
        if ($args['count'] < 1) {
            throw new ApiCallInvalidException($this, [
                'count' => [
                    'invalid' => true,
                ],
            ]);
        }

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }
        $product = $this->productGetById(['product_id' => $args['product_id']]);
        $this->permit($args+[
            'product' => $product,
        ]);

        $stmt = $this->api->db->prepare('select * from PurchaseProductOrder where product_id=:product_id and user_id=:user_id limit 1');
        $stmt->execute([
            ':product_id' => $args['product_id'],
            ':user_id' => $args['owner_id'],
        ]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($row) {
            return $this->orderUpdateById([
                'order_id' => $row['order_id'],
                'count' => $args['count'],
            ]);
        }

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into PurchaseProductOrder
            ( order_id, dtc,  dtm,  purchase_id,  product_id, `count`,  user_id,  `date`, owner_id) values
            (:uid,     :time,:time,:purchase_id, :product_id, :count,  :user_id,  :date, :owner_id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':purchase_id' => $product['purchase_id'],
            ':product_id' => $args['product_id'],
            ':count' => $args['count'],
            ':user_id' => $args['owner_id'],
            ':owner_id' => $args['owner_id'],
            ':date' => $args['date'],
        ]);

        return [
            'order_id' => $uid,
            'product_id' => $args['product_id'],
        ];
    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }

    public function orderUpdateById(array $args = []) {
        return $this->api->purchaseProductOrderUpdateById($args);
    }

}