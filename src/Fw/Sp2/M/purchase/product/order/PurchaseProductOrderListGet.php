<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductOrderListGet extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
                'default' => null,
            ],
            'product_id' => [
                'uid' => null,
                'default' => null,
            ],
            'user_id' => [
                'uid' => null,
                'default' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'].':'.$args['product_id'].':'.$args['user_id'].':'.$args['count'].':'.$args['page'];
        $rows = $this->cacheGet($cacheKey);

        if (!$rows) {

            $where = [];
            $values = [];

            if ($args['purchase_id']) {
                $where[] = 'purchase_id=:purchase_id';
                $values[':purchase_id'] = $args['purchase_id'];
            }
            if ($args['product_id']) {
                $where[] = 'product_id=:product_id';
                $values[':product_id'] = $args['product_id'];
            }
            if ($args['user_id']) {
                $where[] = 'user_id=:user_id';
                $values[':user_id'] = $args['user_id'];
            }

            $query = 'select order_id from PurchaseProductOrder';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by `date` desc limit :from, :count';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
            $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            //$this->cacheSet($cacheKey, $rows, 3);
        }

        $list = [];
        foreach($rows as $i) {
            $list[] = $this->orderGetById(['order_id' => $i['order_id']]);
        }

        return [
            'list' => $list,
        ];

    }

    public function orderGetById(array $args = []) {
        return $this->api->purchaseProductOrderGetById($args);
    }


}