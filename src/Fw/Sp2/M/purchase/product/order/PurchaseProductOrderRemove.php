<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductOrderRemove extends ApiCall {

    public function args() {
        return [
            'order_id' => [
                'uid' => null,
                'default' => null,
            ],
            'product_id' => [
                'uid' => null,
                'default' => null,
            ],
            'purchase_id' => [
                'uid' => null,
                'default' => null,
            ],
            'user_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (isset($args['purchase'])) {
            if ($args['owner_id'] === $args['purchase']['owner_id']) return;
            if ($args['owner_id'] === $args['purchase']['org_id']) return;
        }
        if (isset($args['product'])) {
            if ($args['owner_id'] === $args['product']['owner_id']) return;
        }
        if (isset($args['order'])) {
            if ($args['owner_id'] === $args['order']['owner_id']) return;
            if ($args['owner_id'] === $args['order']['user_id']) return;
        }
        if (isset($args['owner_id'])) {
            if (isset($args['user_id'])) {
                if ($args['owner_id'] === $args['user_id']) return;
            }
        }

        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {

        $where = [];
        $values = [];

        if ($args['purchase_id']) {
            $args['purchase'] = $this->purchaseGetById(['purchase_id' => $args['purchase_id']]);
            $where[] = 'purchase_id=:purchase_id';
            $values[':purchase_id'] = $args['purchase_id'];
        }
        if ($args['product_id']) {
            $args['product'] = $this->productGetById(['product_id' => $args['product_id']]);;
            $where[] = 'product_id=:product_id';
            $values[':product_id'] = $args['product_id'];
        }
        if ($args['order_id']) {
            $args['order'] = $this->orderGetById(['order_id' => $args['order_id']]);;
            $where[] = 'order_id=:order_id';
            $values[':order_id'] = $args['order_id'];
        }
        if ($args['user_id']) {
            $where[] = 'user_id=:user_id';
            $values[':user_id'] = $args['user_id'];
        }

        if (isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $this->permit($args);
        if (!count($where)) {
            return [];
        }

        $query = 'delete from PurchaseProductOrder';
        if ($where) {
            $query .= ' where '.implode(' and ', $where);
        }
        $stmt = $this->api->db->prepare($query);
        $stmt->execute($values);

        return [];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function productGetById(array $args = []) {
        return $this->api->purchaseProductGetById($args);
    }

    public function orderGetById(array $args = []) {
        return $this->api->purchaseProductOrderGetById($args);
    }

}