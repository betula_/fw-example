<?php
namespace Fw\Sp2\M;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseProductListCountGet extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['purchase_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $where = [];
        $values = [];

        $where[] = 'purchase_id=:purchase_id';
        $values[':purchase_id'] = $args['purchase_id'];

        $query = 'select count(product_id) as total from PurchaseProduct';
        if ($where) {
            $query .= ' where '.implode(' and ', $where);
        }
        $stmt = $this->api->db->prepare($query);
        $stmt->execute($values);
        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $total = $count['total'];

        $result = [
            'total' => $total,
        ];

        if ($total < 100) {
            $ttl = 30;
        } elseif ($total < 1000) {
            $ttl = 300;
        } else {
            $ttl = 30000;
        }

        $this->cacheSet($cacheKey, $result, $ttl);
        return $result;

    }


}