<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PurchaseProductRemoveByPurchaseId extends ApiCall {

    public function args() {
        return [
            'purchase_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $list = $this->productListGetByPurchase(['purchase_id' => $args['purchase_id']])['list'];
        foreach($list as $i) {
            $this->productRemoveById(['product_id' => $i['product_id']]);
        }
    }

    public function productListGetByPurchase(array $args = []) {
        return $this->api->purchaseProductListGet($args);
    }

    public function productRemoveById(array $args = []) {
        return $this->api->purchaseProductRemoveById($args);
    }


}