<?php
namespace Fw\Sp2\M;
use Fw\M\M\PaginationGet;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListPaginationGetByCategoryId extends PaginationGet {

    public function args() {
        return parent::args() + [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function total($args) {
        $count = $this->api->purchaseListCountGetByCategoryId(['category_id' => $args['category_id']]);
        return $count['total'];
    }

}