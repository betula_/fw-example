<?php
namespace Fw\Sp2\M;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListCountGetByOwnerId extends ApiCall {

    public function args() {
        return [
            'owner_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['owner_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $where = [];
        $values = [];

        $where[] = 'owner_id=:owner_id';
        $values[':owner_id'] = $args['owner_id'];

        $query = 'select count(purchase_id) as total from Purchase';
        if ($where) {
            $query .= ' where '.implode(' and ', $where);
        }
        $stmt = $this->api->db->prepare($query);
        $stmt->execute($values);
        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $total = $count['total'];


        $result = [
            'total' => $total,
        ];

        if ($total < 100) {
            $ttl = 30;
        } elseif ($total < 1000) {
            $ttl = 300;
        } else {
            $ttl = 30000;
        }

        $this->cacheSet($cacheKey, $result, $ttl);
        return $result;

    }

}