<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListGetByCategoryId extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function perform ($args) {

        $cacheKey = $args['category_id'].':'.$args['count'].':'.$args['page'];
        $rows = $this->cacheGet($cacheKey);
        if (!$rows) {

            $where = [];
            $values = [];

            if (isset($args['category_id'])) {
                $idList = [];
                if (isset($args['category_id'])) {
                    $idList[] = $args['category_id'];
                }
                $list = $this->categoryChildrenIdGetById([
                    'category_id' => $args['category_id']
                ]);
                foreach ($list['list'] as $row) {
                    $idList[] = $row['category_id'];
                }

                $p = [];
                $v = [];
                foreach ($idList as $i => $id) {
                    $p[] = ':'.$i;
                    $v[':'.$i] = $id;
                }
                $where[] = sprintf('category_id in (%s)', implode(',', $p));
                $values += $v;
            }

            $query = 'select distinct purchase_id from PurchaseCategory';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by dtc desc limit :from, :count';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
            $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $this->cacheSet($cacheKey, $rows, 3);
        }

        $list = [];
        foreach($rows as $i) {
            $list[] = $this->purchaseGetById(['purchase_id' => $i['purchase_id']]);
        }

        return [
            'list' => $list,
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }

    public function categoryChildrenIdGetById(array $args = []) {
        return $this->api->categoryPurchaseChildrenIdGetById($args);
    }

}