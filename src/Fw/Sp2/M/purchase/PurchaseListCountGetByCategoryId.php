<?php
namespace Fw\Sp2\M;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListCountGetByCategoryId extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['category_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $where = [];
        $values = [];

        if (isset($args['category_id'])) {
            $idList = [];
            if (isset($args['category_id'])) {
                $idList[] = $args['category_id'];
            }
            $list = $this->categoryChildrenIdGetById([
                'category_id' => $args['category_id']
            ]);
            foreach ($list['list'] as $row) {
                $idList[] = $row['category_id'];
            }

            $p = [];
            $v = [];
            foreach ($idList as $i => $id) {
                $p[] = ':'.$i;
                $v[':'.$i] = $id;
            }
            $where[] = sprintf('category_id in (%s)', implode(',', $p));
            $values += $v;
        }

        $query = 'select count(distinct purchase_id) as total from PurchaseCategory';
        if ($where) {
            $query .= ' where '.implode(' and ', $where);
        }
        $stmt = $this->api->db->prepare($query);
        $stmt->execute($values);
        $count = $stmt->fetch(PDO::FETCH_ASSOC);
        $total = $count['total'];


        $result = [
            'total' => $total,
        ];

        if ($total < 100) {
            $ttl = 30;
        } elseif ($total < 1000) {
            $ttl = 300;
        } else {
            $ttl = 30000;
        }

        $this->cacheSet($cacheKey, $result, $ttl);
        return $result;

    }

    public function categoryChildrenIdGetById(array $args = []) {
        return $this->api->categoryPurchaseChildrenIdGetById($args);
    }

}