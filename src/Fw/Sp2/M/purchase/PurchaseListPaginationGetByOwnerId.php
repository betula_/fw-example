<?php
namespace Fw\Sp2\M;
use Fw\M\M\PaginationGet;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListPaginationGetByOwnerId extends PaginationGet {

    public function args() {
        return parent::args() + [
            'owner_id' => [
                'uid' => null,
            ],
        ];
    }

    public function total($args) {
        $count = $this->api->purchaseListCountGetByOwnerId(['owner_id' => $args['owner_id']]);
        return $count['total'];
    }

}