<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PurchaseListGetByOwnerId extends ApiCall {

    public function args() {
        return [
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'owner_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {

        $cacheKey = $args['count'].':'.$args['page'].':'.$args['owner_id'];
        $rows = $this->cacheGet($cacheKey);
        if (!$rows) {

            $where = [];
            $values = [];

            $where[] = 'owner_id=:owner_id';
            $values[':owner_id'] = $args['owner_id'];

            $query = 'select purchase_id from Purchase';
            if ($where) {
                $query .= ' where '.implode(' and ', $where);
            }
            $query .= ' order by dtm desc limit :from, :count';
            $stmt = $this->api->db->prepare($query);

            foreach ($values as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            $stmt->bindValue(':count', $args['count'], PDO::PARAM_INT);
            $stmt->bindValue(':from', $args['page']*$args['count'], PDO::PARAM_INT);
            $stmt->execute();

            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $this->cacheSet($cacheKey, $rows, 3);
        }

        $list = [];
        foreach($rows as $i) {
            $list[] = $this->purchaseGetById(['purchase_id' => $i['purchase_id']]);
        }

        return [
            'list' => $list,
        ];
    }

    public function purchaseGetById(array $args = []) {
        return $this->api->purchaseGetById($args);
    }


}