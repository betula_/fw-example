<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;

class UserGetBySpId extends ApiCall {

    public function args() {
        return [
            'id' => [
                'int' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from UserSp where id = ? limit 1');
        $stmt->execute([$args['id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$user) {
            throw new ApiCallInvalidException($this, [
                'id' => [
                    'not_found' => null,
                ],
            ]);
        }
        $user = $this->api->userGetById(['user_id' => $user['user_id']]);
        return $user;
    }

}