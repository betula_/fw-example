<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserSpGetByUserId extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['user_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from UserSp where user_id = ? limit 1');
        $stmt->execute([$args['user_id']]);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new ApiCallInvalidException($this, [
                'user_id' => [
                    'not_found' => null,
                ],
            ]);
        }

        $this->cacheSet($cacheKey, $result);
        return $result;
    }

}