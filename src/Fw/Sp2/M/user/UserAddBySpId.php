<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;

class UserAddBySpId extends ApiCall {

    public function args() {
        return [
            'id' => [
                'int' => null,
            ],
        ];
    }

    public function perform ($args) {

        $stmt = $this->api->db->prepare('select * from UserSp where id = ? limit 1');
        $stmt->execute([$args['id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user) {
            throw new ApiCallInvalidException($this, [
                'id' => [
                    'exists' => true,
                ],
            ]);
        }
        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into User
            ( user_id,  dtc,  dtm) values
            (:uid,     :time,:time)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
        ]);

        $stmt = $this->api->db->prepare('insert into UserSp
            ( user_id,  dtc,  dtm,  id) values
            (:uid,     :time,:time,:id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':id' => $args['id'],
        ]);

        $user = $this->api->userGetById(['user_id' => $uid]);
        return $user;
    }

}