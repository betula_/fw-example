<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\M\M\Reset as ResetBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class Reset extends ResetBasic {

    public function perform ($args) {
        parent::perform($args);

        $this->api->resetPurchase();
    }

}