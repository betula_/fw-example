<?php
namespace Fw\Sp2\M;
use Fw\M\M\ResetCategory as ResetCategoryBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetCategoryPurchase extends ResetCategoryBasic {

    public function truncate() {
        $this->api->db->exec('truncate CategoryPurchase');
    }

    public function categoryAdd(array $args = []) {
        return $this->api->categoryPurchaseAdd($args);
    }

    public function getCategoryString() {
        $string =
            <<<TEXT
Детская одежда
    Верхняя одежда
    Головные уборы
    Нарядная одежда
    Повседневная одежда, трикотаж
    Одежда для подростков, школьная одежда
    Белье для детей, колготки, носки
Раздел товаров для детей
    Игрушки для детей
    Книги и развивающее пособия
    Компьютерные игры, программы и техника для детей школьного возраста
    Мебель для детей
    Детская гигиена
    Коляски, велосипеды, самокаты, ролики
    Товары для школы
Взрослая одежда
    Женское
        Верхняя одежда
        Одежда повседневная и для офиса
        Вечерняя и свадебная одежда
        Одежда для дома
        Спортивная одежда
        Большие размеры (54-68р.)
        Одежда для беременных и кормящих
    Мужское
        Верхняя одежда
        Одежда для офиса
        Повседневная одежда
        Спортивная одежда
        Большие размеры
Обувь
    Детская обувь
    Женская обувь
    Мужская обувь
Украшения и аксессуары
    Бижутерия и ювелирные изделия
    Аксессуары для волос
    Кожгалантерея
Белье и колготки
    Нижнее белье
    термобелье
    Купальники
    Колготки, носки
Товары для дома и сада
    Посуда и товары для кухни
    Мебель и предметы интерьера
    Товары для уборки, бытовая химия, товары для дома
    Всё для ремонта
    Товары для садоводов
    Осветительная техника
    Товары для домашних животных
    Книги, канц. товары
    Товары для организации праздника
Текстиль для дома и Товары для рукоделия
    Постельные принадлежности
    Текстиль для дома
    Товары для творчества
    Рукоделие
Автотовары
    Шины, диски
    Авто аксессуары (чехлы, коврики, шторки)
    Авто химия
    Техника для автомобиля
    Детская безопасность и комфорт в автомобиле
Техника
    Техника для кухни
    Бытовая техника
    Электронные гаджеты
Продукты
    чай, кофе, аксессуары
    орешки, сладости, снеки
    разное
Спорт. Здоровье. Красота.
    Косметика и парфюмерия
    Средства по уходу за волосами
    Лечебная косметика и приборы
    Похудение и коррекция фигуры
    Спортивные товары и тренажеры
    Ортопедические товары
Услуги, билеты, абонементы и спец. предложения
    Билеты
    Абонементы в оздоровительно-физкультурные центры
    Предложения салонов красоты и спа центров
    Тренинги
    Путешествия
Сезонное предложение
    Товары для Нового года
    Карнавальная одежда и костюмы
	23 февраля-8 марта
    Пасха
    Другие праздники
TEXT;
        return $string;
    }

}