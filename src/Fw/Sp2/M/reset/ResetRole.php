<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\M\M\ResetRole as ResetRoleBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetRole extends ResetRoleBasic {


    public function perform ($args) {
        parent::perform($args);

        $this->api->roleAddByName([
            'name' => 'org',
        ]);
    }

}