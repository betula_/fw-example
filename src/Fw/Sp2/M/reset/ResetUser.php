<?php
namespace Fw\Sp2\M;
use Fw\M\M\ResetUser as ResetUserBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetUser extends ResetUserBasic {

    public function perform ($args) {
        parent::perform($args);
        $this->api->resetUserSp();
    }


}