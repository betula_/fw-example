<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetUserSp extends ApiCall {


    public function perform ($args) {
        $this->api->db->exec('truncate UserSp');
    }

}