<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\M\M\ResetContent as ResetContentBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetContent extends ResetContentBasic {


    public function perform ($args) {
        parent::perform($args);

        $this->api->contentAddByName([
            'name' => 'pageForbidden',
            'content' => 'Вы не имеете доступа к запрашиваемой информации',
        ]);

        $this->api->contentAddByName([
            'name' => 'footerCopyright',
            'content' => '© Копирайт 2012',
        ]);

        $this->api->contentAddByName([
            'name' => 'helpLink',
            'content' => '<p><a href="#">помощь</a>&nbsp;<a href="#">правила</a></p>',
        ]);

        $this->api->contentAddByName([
            'name' => 'footerProject',
            'content' => '<p><a href="#">правила</a></p><p><a href="#">черный список</a></p>',
        ]);
        $this->api->contentAddByName([
            'name' => 'footerAbout',
            'content' => '<p><a href="#">общие вопросы</a></p><p><a href="#">организаторам</a></p>',
        ]);

    }

}