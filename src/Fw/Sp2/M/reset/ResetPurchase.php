<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetPurchase extends ApiCall {


    public function perform ($args) {

        $this->api->db->exec('truncate Purchase');
        $this->api->db->exec('truncate PurchaseCategory');
        $this->api->db->exec('truncate PurchaseOption');
        $this->api->db->exec('truncate PurchaseOptionKind');
        $this->api->db->exec('truncate PurchaseProduct');

        $kind[] = $this->api->purchaseOptionKindAdd([
            'title' => 'Гарантия цвета',
            'type' => 'switch',
            'name' => 'garantyColor',
        ]);
        $kind[] = $this->api->purchaseOptionKindAdd([
            'title' => 'Гарантия размера',
            'type' => 'switch',
            'name' => 'garantySize',
        ]);
        $kind[] = $this->api->purchaseOptionKindAdd([
            'title' => 'Гарантия брака',
            'type' => 'switch',
            'name' => 'garantyDefect',
        ]);
        $kind[] = $this->api->purchaseOptionKindAdd([
            'title' => 'Сайт закупки',
            'type' => 'uri',
            'name' => 'uriHome',
        ]);

        $purchase[] = $this->api->purchaseAdd([
            'title' => 'Закупка',
            'description' => 'Описание закупки',
        ]);

        $this->api->purchaseOptionAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'kind_id' => $kind[3]['kind_id'],
            'value' => 'http://www.yandex.ru',
        ]);
        $this->api->purchaseOptionAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'kind_id' => $kind[0]['kind_id'],
            'value' => '0',
        ]);
        $this->api->purchaseOptionAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'kind_id' => $kind[1]['kind_id'],
            'value' => '1',
        ]);
        $this->api->purchaseOptionAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'kind_id' => $kind[2]['kind_id'],
            'value' => '1',
        ]);

        $product[] = $this->api->purchaseProductAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'title' => 'Товар 1',
            'description' => 'Описание товара 1',
            'order' => 100,
        ]);
        $product[] = $this->api->purchaseProductAdd([
            'purchase_id' => $purchase[0]['purchase_id'],
            'title' => 'Товар 2',
            'description' => 'Описание товара 2',
            'order' => 200,
        ]);


    }




}