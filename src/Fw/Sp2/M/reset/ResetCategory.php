<?php
namespace Fw\Sp2\M;
use Fw\M\M\ResetCategory as ResetCategoryBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetCategory extends ResetCategoryBasic {

    public function perform ($args) {
        parent::perform($args);
        $this->api->resetCategoryPurchase();
    }


}