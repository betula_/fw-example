<?php
namespace Fw\Sp2\M;
use Fw\M\M\CategoryTreeGetById;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryPurchaseTreeGetById extends CategoryTreeGetById {

    public function categoryChildrenGetById(array $args = []) {
        return $this->api->categoryPurchaseChildrenGetById($args);
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

}