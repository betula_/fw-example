<?php
namespace Fw\Sp2\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class CategoryPurchaseCountChange extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'delta' => [
                'int' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user)) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit($args);

        if (!isset($args['category_id'])) {
            return [];
        }

        $list = [];
        $category = $this->categoryGetById(['category_id' => $args['category_id']]);

        $list[] = $category['category_id'];
        if ($category['path']) {
            $list = array_merge($list, explode('/', $category['path']));
        }
        $placeHolder = implode(',', array_fill(0, count($list), '?'));

        $stmt = $this->api->db->prepare(sprintf(
            'update CategoryPurchase set `count` := `count` + ? where category_id in (%s)',
            $placeHolder
        ));

        $stmt->execute(array_merge(
            [$args['delta']],
            $list
        ));

        return [];
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

}