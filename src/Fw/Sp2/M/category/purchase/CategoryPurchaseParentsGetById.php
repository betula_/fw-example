<?php
namespace Fw\Sp2\M;
use Fw\M\M\CategoryParentsGetById;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryPurchaseParentsGetById extends CategoryParentsGetById {

    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

}