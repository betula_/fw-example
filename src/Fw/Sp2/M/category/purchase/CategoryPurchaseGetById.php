<?php
namespace Fw\Sp2\M;
use Fw\M\M\CategoryGetById;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryPurchaseGetById extends CategoryGetById {

    public function perform($args) {
        $category = parent::perform($args);

        $cacheKey = __CLASS__.$args['category_id'];
        $row = $this->cacheGet($cacheKey);
        if (!$row) {
            $stmt = $this->api->db->prepare('select * from CategoryPurchase where category_id = ? limit 1');
            $stmt->execute([$args['category_id']]);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$row) {
                throw new ApiCallInvalidException($this, [
                    'category_id' => [
                        'not_found' => null,
                    ],
                ]);
            }

            $this->cacheSet($cacheKey, $row);
        }

        $category += $row;
        return $category;
    }

}