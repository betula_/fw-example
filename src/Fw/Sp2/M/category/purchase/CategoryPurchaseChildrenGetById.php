<?php
namespace Fw\Sp2\M;
use Fw\M\M\CategoryChildrenGetById;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryPurchaseChildrenGetById extends CategoryChildrenGetById {


    public function categoryGetById(array $args = []) {
        return $this->api->categoryPurchaseGetById($args);
    }

}