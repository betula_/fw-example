<?php
namespace Fw\Sp2\M;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;
use Fw\M\M\CategoryChildrenGetById;

class CategoryPurchaseChildrenIdGetById extends CategoryChildrenGetById {

    public function resultPerform($rows) {
        return $rows;
    }

}