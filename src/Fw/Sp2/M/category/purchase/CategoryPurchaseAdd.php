<?php
namespace Fw\Sp2\M;
use Fw\M\M\CategoryAdd;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class CategoryPurchaseAdd extends CategoryAdd {

    public function perform ($args) {
        $result = parent::perform($args);

        $stmt = $this->api->db->prepare('insert into CategoryPurchase
            ( category_id,  dtc,  dtm,  `count`) values
            (:uid,         :time,:time, :count)');
        $stmt->execute([
            ':uid' => $result['category_id'],
            ':time' => $this->api->time,
            ':count' => 0,
        ]);

        return $result;
    }

}