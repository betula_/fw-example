<?php
namespace Fw\Sp2;
use Fw\Fw\Module as ModuleBasic;
use Fw\Fw\Env;
use Fw\System\Request;
use Fw\System\Response;
use Fw\System\Router;
use Fw\System\Console;
use Fw\System\Validator;
use Fw\System\Auth;
use Exception;
use PDO;
use Fw\System\Api;
use Fw\M\Db;
use Fw\System\ApiCallInvalidException;

class Module extends ModuleBasic {

    public function getRelatedModules() {
        return [
            'C\\Module',
            'M\\Module',
            'Sp\\Module',
        ];
    }

    public function init() {
        Env::$app->vaultName = 'sp2';
        Env::$app->cfg['langLegal'] = ['ru'];
        Env::$app->cfg['langDefault'] = ['ru'];
        Env::$app->cfg['purchaseOptionTypes'] = ['switch','uri','text'];
        Env::$app->cfg['hackPass'] = '®ƒ® ¬†˚ƒ';

        $spSvcLoader = Env::$app->serviceLoaders['sp'];
        Env::$app->serviceLoaders['sp'] = function() use ($spSvcLoader) {
            $svc = $spSvcLoader();
            $svc->lazyLoaders['db'] = function() {
                $driver = new Db(
                    Env::$app->cfg['dbSp']['dsn'],
                    Env::$app->cfg['dbSp']['user'],
                    Env::$app->cfg['dbSp']['pass'],
                    [
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    ]
                );
                return $driver;
            };
            return $svc;
        };

        Env::$app->serviceLoaders['m'] = function() {
            $api = new ApiM();
            $api->lazyLoaders['db'] = function(){
                $driver = new Db(
                    Env::$app->cfg['dbSp2']['dsn'],
                    Env::$app->cfg['dbSp2']['user'],
                    Env::$app->cfg['dbSp2']['pass'],
                    [
                        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    ]
                );
                return $driver;
            };
            $api->lazyLoaders['cache'] = function(){
                return Env::$app->cache;
            };
            return $api;
        };

        Env::$app->bind('authCreated', function($event, Auth $auth) {
            $auth->bind('start', function($event, $session) {
                $sp = Env::$app->sp;
                $m = Env::$app->m;
                if ($sp->user) {
                    try {
                        $u = $m->userGetBySpId([
                            'id' => $sp->user['user_id'],
                        ]);
                    }
                    catch (ApiCallInvalidException $e) {
                        $status = $e->getInvalidData();
                        if (!array_key_exists('not_found', $status['id'])) {
                            throw $e;
                        }
                        $u = $m->userAddBySpId([
                            'id' => $sp->user['user_id'],
                        ]);

                        switch ($sp->user['group_id']) {
                            case 1: // Administrator
                                $m->roleUserAdd([
                                    'user_id' => $u['user_id'],
                                    'role_id' => $m->roleGetByName([
                                        'name' => 'admin'
                                    ])['role_id'],
                                ]);
                                $m->roleUserAdd([
                                    'user_id' => $u['user_id'],
                                    'role_id' => $m->roleGetByName([
                                        'name' => 'org'
                                    ])['role_id'],
                                ]);
                                break;
                        }
                    }

                    $m->currentUserSetById($u['user_id']);
                }

                if ($m->user) {
                    $m->user['role']['org'] = true;     // TODO: org to all
                }
            });
        });

        Env::$app->bind('routerCreated', function($event, Router $router) {

            $router->set('pageHome', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if ($request->uri !== '/') {
                        return null;
                    }
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pagePurchaseList');
                    return $response;
                },
                function(){
                    return '/';
                }
            );
            $router->set('pageForbidden', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if (trim($request->uri,'/') !== 'forbidden') {
                        return null;
                    }
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pageForbidden');
                    return $response;
                },
                function(){
                    return '/forbidden';
                }
            );
            $router->set('pagePersonalDialogList', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if (trim($request->uri, '/') !== 'personal/dialogs') {
                        return null;
                    }
                    $page = $request->getGet('page');
                    if (!$page) {
                        $page = null;
                    }
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pagePersonalDialogList', [
                        'page' => $page,
                    ]);
                    return $response;
                },
                function(array $params = []){
                    $query = '';
                    if ($params) {
                        $query = '?'.http_build_query($params);
                    }
                    return '/personal/dialogs'.$query;
                }
            );
            $router->set('pagePersonalDialog', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if (!preg_match('~^/personal/dialog/(?<opponent_id>[0-9]+)/?$~', $request->uri, $m)) {
                        return null;
                    }
                    $page = $request->getGet('page');
                    if (!$page) {
                        $page = null;
                    }
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pagePersonalDialog', [
                        'page' => $page,
                        'opponent_id' => $m['opponent_id'],
                    ]);
                    return $response;
                },
                function($user_id, array $params = []){
                    $query = '';
                    if ($params) {
                        $query = '?'.http_build_query($params);
                    }
                    return '/personal/dialog/'.$user_id.$query;
                }
            );
            $router->set('pagePurchaseList', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if (trim($request->uri, '/') !== 'purchases') {
                        return null;
                    }
                    $params['page'] = $request->getGet('page');
                    $params['owner_id'] = $request->getGet('owner_id');
                    $params['category_id'] = $request->getGet('category_id');
                    $params = array_filter($params);
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pagePurchaseList', $params);
                    return $response;
                },
                function(array $params = []){
                    $query = '';
                    if ($params) {
                        $query = '?'.http_build_query($params);
                    }
                    return '/purchases'.$query;
                }
            );
            $router->set('pagePurchase', Router::PRIORITY_NORMAL,
                function(Request $request) {
                    if (!preg_match('~^/purchase/(?<purchase_id>[0-9a-f]{19,20})/?$~', $request->uri, $m)) {
                        return null;
                    }
                    $response = $this->makeComponentResponseWithCookieAuth($request, 'pagePurchase', [
                        'purchase_id' => $m['purchase_id'],
                    ]);
                    return $response;
                },
                function($purchase_id){
                    return '/purchase/'.$purchase_id;
                }
            );

        });
    }

    public function makeComponentResponseWithCookieAuth(Request $request, $componentName, array $prop=array()) {
        $token = $request->getCookie('token');
        Env::$app->auth->start($token);
        $response = Env::$app->c->makeComponentResponse($componentName, $prop);

        if (!$token) {
            $response->cache = 1;
        } else {
            $token = Env::$app->auth->token;
            if ($token) {
                $response->cookie('token', $token, Env::$app->auth->ttl + time());
            } else {
                $response->cookie('token', null, 0);
            }
        }

        return $response;
    }

    public function getDirectory() {
        return __DIR__;
    }

}