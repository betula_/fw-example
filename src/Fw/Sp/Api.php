<?php
namespace Fw\Sp;
use Fw\System\Api as ApiBasic;
use Fw\Fw\Env;
use Fw\System\Request;
use Fw\System\ApiCallInvalidException;


/**
 * @property \PDO $db
 *
 * @method userGetById()
 */

class Api extends ApiBasic {

    public $user = null;

    /**
     * @var EntityAdapter
     */
    public $adapter;


    public function __construct() {
        parent::__construct('Sp');
        $this->adapter = new EntityAdapter();

    }

    public function passwordEncrypt($password, $username) {
        $name1251 = mb_convert_encoding($username,'windows-1251','utf-8');
        $pass1251 = mb_convert_encoding($password,'windows-1251','utf-8');
        return sha1(strtolower($name1251) . $pass1251);
    }

    public function currentUserSetById($id) {
        $user = $this->userGetById(['user_id' => $id]);
        $this->user = $user;
    }


}