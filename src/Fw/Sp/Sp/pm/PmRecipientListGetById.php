<?php
namespace Fw\Sp\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PmRecipientListGetById extends ApiCall {

    public function args() {
        return [
            'pm_id' => [
                'int' => null,
            ]
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['pm_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from smf_pm_recipients where ID_PM = ?');
        $stmt->execute([$args['pm_id']]);
        $list = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($list as &$i) {
            $this->api->adapter->pmRecipient($i);
        }
        unset($i);

        $result = [
            'list' => $list,
        ];

        $this->cacheSet($cacheKey, $result, 10);
        return $result;
    }

}