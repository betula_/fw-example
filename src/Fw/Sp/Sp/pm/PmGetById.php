<?php
namespace Fw\Sp\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PmGetById extends ApiCall {

    public function args() {
        return [
            'pm_id' => [
                'int' => null,
            ]
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['pm_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from smf_personal_messages where ID_PM = ? limit 1');
        $stmt->execute([$args['pm_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, array(
                'pm_id' => array(
                    'not_found' => null,
                ),
            ));
        }
        $this->api->adapter->pm($row);

        $this->cacheSet($cacheKey, $row, 100);
        return $row;
    }

}