<?php
namespace Fw\Sp\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserGetByEmail extends ApiCall {

    public function args() {
        return [
            'email' => [
                'string' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from smf_members where memberName = ? limit 1');
        $stmt->execute([$args['email']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$user) { // @see Sources/LogInOut.php line 214
            $stmt = $this->api->db->prepare('select * from smf_members where emailAddress = ? limit 1');
            $stmt->execute([$args['email']]);
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        if (!$user) {
            throw new ApiCallInvalidException($this, [
                'email' => [
                    'not_found' => null,
                ],
            ]);
        }

        $this->api->adapter->user($user);
        return $user;
    }

}