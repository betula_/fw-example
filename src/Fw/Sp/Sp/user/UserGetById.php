<?php
namespace Fw\Sp\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserGetById extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'int' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['user_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from smf_members where ID_MEMBER = ?');
        $stmt->execute([$args['user_id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$user) {
            throw new ApiCallInvalidException($this, array(
                'user_id' => array(
                    'not_found' => null,
                ),
            ));
        }
        $this->api->adapter->user($user);
        $this->cacheSet($cacheKey, $user, 1);
        return $user;
    }

}