<?php
namespace Fw\Sp\Sp;
use Fw\Sp\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;

class UserGetByEmailPass extends ApiCall {

    public function args() {
        return [
            'email' => [
                'string' => null,
            ],
            'pass' => [
                'string' => null,
            ]
        ];
    }

    public function perform ($args) {
        $user = $this->api->userGetByEmail([
            'email' => $args['email'],
        ]);
        $hash = $this->api->passwordEncrypt($args['pass'],$user['memberName']);
        if ($hash != $user['passwd']) {
            throw new ApiCallInvalidException($this, [
                'email' => [
                    'not_found' => null,
                ],
            ]);
        }

        return $user;
    }

}