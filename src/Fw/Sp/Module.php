<?php
namespace Fw\Sp;
use Exception;
use Fw\Fw\Module as BasicModule;
use Fw\Fw\Env;
use Fw\System\Console;
use Fw\System\Validator;
use Fw\System\Router;
use Fw\System\Auth;

class Module extends BasicModule {

    public function getDirectory() {
        return __DIR__;
    }

    public function getRelatedModules() {
        return [
            'System\\Module',
        ];
    }


    public function init() {

        Env::$app->bind('authCreated', function($event, Auth $auth) {
            $auth->bind('start', function($event, $session) {
                if (isset($session['spUserId'])) {
                    Env::$app->sp->currentUserSetById($session['spUserId']);
                }
            });
            $auth->bind('stop', function() {
                Env::$app->sp->user = null;
            });
        });

        Env::$app->serviceLoaders['sp'] = function() {
            $api = new Api();
            $api->lazyLoaders['db'] = function(){
                //return new PDO();
            };
            $api->cache = Env::$app->cache;
            return $api;
        };

        Env::$app->serviceLoaders['text'] = function() {
            return new TextProcessor();
        };

        Env::$app->bind('build', function(){
            Env::$app->sp->build();
        });

        Env::$app->bind('consoleCreated', function($event, Console $console) {
            Env::$app->sp->registerConsoleCommands('sp', $console);
        });

    }

}