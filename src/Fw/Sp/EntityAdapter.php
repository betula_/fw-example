<?php
namespace Fw\Sp;
use Fw\Fw\Env;


class EntityAdapter {

    public function user(&$ent) {
        $ent['user_id'] = $ent['ID_MEMBER'];
        $ent['signature_html'] = Env::$app->text->bb($ent['signature']);
        $ent['group_id'] = $ent['ID_GROUP'];
    }

    public function pm(&$ent) {
        $ent['body_html'] = Env::$app->text->bb($ent['body']);
        $ent['pm_id'] = $ent['ID_PM'];
        $ent['sender_id'] = $ent['ID_MEMBER_FROM'];
    }

    public function pmRecipient(&$ent) {
        $ent['pm_id'] = $ent['ID_PM'];
        $ent['user_id'] = $ent['ID_MEMBER'];
    }

}