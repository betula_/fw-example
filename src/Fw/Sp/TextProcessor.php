<?php
namespace Fw\Sp;
use Fw\Fw\Env;


class TextProcessor {

    public function hrefs($text) {
        return preg_replace(
            '~(^|[^\w@])([\w\d]+://)?(((\d{1,3}\.){3}\d{1,3}|([\w\-\d]+\.)*[\w\-\d]{2,}\.(?!\d{2})[\w\-\d]{2,})(:\d+)?([?#/]((?![;:\'".,]+(\s|$))[^\s])*)?)~',
            '$1<a href="http://$3" target="_blank">$2$3</a>',
            $text
        );
    }

    public function bb($text) {
        $pattern = [
            '~\[b\](.*?)\[\/b\]~is',
            '~\[i\](.*?)\[\/i\]~is',
            '~\[u\](.*?)\[\/u\]~is',
            '~\[s\](.*?)\[\/s\]~is',

            '~\[color\=(.*?)\](.*?)\[\/color\]~is',
            '~\[size\=(.*?)\](.*?)\[\/size\]~is',

            '~\[ul\](.*?)\[\/ul\]~is',
            '~\[li\](.*?)\[\/li\]~is',

            '~\[img\](.*?)\[\/img\]~is',

            '~\[url\](.*?)\[\/url\]~is',
            '~\[url\=(.*?)\](.*?)\[\/url\]~is',

        ];
        $replacement = [
            '<b>$1</b>',
            '<i>$1</i>',
            '<span style="text-decoration: underline;">$1</span>',
            '<span style="text-decoration: line-through;">$1</span>',

            '<span style="color: $1;">$2</span>',
            '<span style="font-size: $1;">$2</span>',

            '<ul>$1</ul>',
            '<li>$1</li>',

            '<img src="$1" />',

            '<a href="$1" target="_blank">$1</a>',
            '<a href="$1" target="_blank">$2</a>',
        ];
        return preg_replace($pattern, $replacement, $text);
    }

}