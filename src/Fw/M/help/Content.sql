
CREATE  TABLE IF NOT EXISTS `Content` (
  `content_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `name` CHAR(32) NOT NULL ,
  `content` MEDIUMTEXT NULL ,
  `owner_id` CHAR(20) NULL ,
  PRIMARY KEY (`content_id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  UNIQUE INDEX `content_id_UNIQUE` (`content_id` ASC) )
ENGINE = InnoDB;