
CREATE  TABLE IF NOT EXISTS `UserRole` (
  `user_id` CHAR(20) NOT NULL ,
  `role_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  PRIMARY KEY (`user_id`, `role_id`) )
ENGINE = InnoDB;