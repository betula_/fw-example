
CREATE  TABLE IF NOT EXISTS `Role` (
  `role_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `name` CHAR(32) NOT NULL ,
  PRIMARY KEY (`role_id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;
