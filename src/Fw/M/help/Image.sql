
CREATE  TABLE IF NOT EXISTS `Image` (
  `image_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `order` INT NULL ,
  `owner_id` CHAR(20) NULL ,
  `description` TEXT NULL ,
  `title` VARCHAR(255) NULL ,
  `group_id` CHAR(20) NULL ,
  PRIMARY KEY (`image_id`) ,
  UNIQUE INDEX `image_id_UNIQUE` (`image_id` ASC) )
ENGINE = InnoDB;
