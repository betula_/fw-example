
CREATE  TABLE IF NOT EXISTS `Picture` (
  `picture_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `name` CHAR(32) NOT NULL ,
  `image` VARCHAR(255) NULL ,
  `owner_id` CHAR(20) NULL ,
  `title` VARCHAR(255) NULL ,
  PRIMARY KEY (`picture_id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  UNIQUE INDEX `picture_id_UNIQUE` (`picture_id` ASC) )
ENGINE = InnoDB;
