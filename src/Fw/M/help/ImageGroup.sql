
CREATE  TABLE IF NOT EXISTS `ImageGroup` (
  `group_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `owner_id` CHAR(20) NULL ,
  `title` VARCHAR(255) NULL ,
  `name` CHAR(32) NOT NULL ,
  PRIMARY KEY (`group_id`) ,
  UNIQUE INDEX `image_id_UNIQUE` (`group_id` ASC) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
ENGINE = InnoDB;