
CREATE  TABLE IF NOT EXISTS `UserEmailPass` (
  `user_id` CHAR(20) NOT NULL ,
  `dtm` INT NOT NULL ,
  `dtc` INT NOT NULL ,
  `pass` CHAR(150) NOT NULL ,
  `email` VARCHAR(200) NOT NULL ,
  PRIMARY KEY (`user_id`) ,
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
ENGINE = InnoDB;