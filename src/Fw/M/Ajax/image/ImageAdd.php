<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class ImageAdd extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
            ],
            'order' => [
                'string' => null,
                'default' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'description' => [
                'string' => null,
                'default' => null,
            ],
        ];
    }

    public function perform($args) {
        $name = $this->api->createUid().'.jpg';
        $path = Env::$app->storage->path($name, true);

        $image = new Imagick($args['_files']['image']['tmp_name']);
        $image->setImageFormat('jpeg');
        $image->writeImage($path);
        $image->destroy();

        unlink($args['_files']['image']['tmp_name']);

        $image = Env::$app->m->imageAdd([
            'group_id' => $args['group_id'],
            'order' => $args['order'] ? $args['order'] : null,
            'image' => $name,
            'title' => $args['title'],
            'description' => $args['description'],
        ]);
        return [
            'group_id' => $image['group_id'],
            'image_id' => $image['image_id'],
        ];
    }

}
