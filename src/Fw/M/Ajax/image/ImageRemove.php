<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class ImageRemove extends ApiCall {

    public function args() {
        return [
            'image_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform($args) {
        $image = Env::$app->m->imageGetById(['image_id' => $args['image_id']]);
        Env::$app->m->imageRemoveById([
            'image_id' => $image['image_id'],
        ]);
        return [
            'image_id' => $image['image_id'],
            'group_id' => $image['group_id'],
        ];
    }

}
