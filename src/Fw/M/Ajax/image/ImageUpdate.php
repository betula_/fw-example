<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class ImageUpdate extends ApiCall {

    public function args() {
        return [
            'image_id' => [
                'uid' => null,
            ],
            'order' => [
                'string' => null,
                'optional' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ]
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'image_id', 'title', 'description'
        ], true));

        if (isset($args['order']) && ($args['order'] || $args['order'] === '0')) {
            $params['order'] = $args['order'];
        }

        if (isset($args['_files']['image'])) {
            $name = $this->api->createUid().'.jpg';
            $path = Env::$app->storage->path($name, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('jpeg');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $name;
        }

        $image = Env::$app->m->imageUpdateById($params);
        return [
            'image_id' => $image['image_id'],
            'group_id' => $image['group_id'],
        ];
    }

}
