<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;

class ContentUpdate extends ApiCall {

    public function args() {
        return [
            'content_id' => [
                'uid' => null,
            ],
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'content' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'content_id', 'name', 'content'
        ], true));

        Env::$app->m->contentUpdateById($params);
        return [
            'content_id' => $params['content_id'],
        ];
    }

}
