<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class ImageGroupUpdate extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
            ],
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'group_id', 'name', 'title'
        ], true));

        Env::$app->m->imageGroupUpdateById($params);
        return [
            'group_id' => $params['group_id'],
        ];
    }

}
