<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Exception;

class AuthEnd extends ApiCall {

    public function args() {
        return [
        ];
    }

    public function perform($args) {
        Env::$app->auth->stop();
        return [];
    }

}
