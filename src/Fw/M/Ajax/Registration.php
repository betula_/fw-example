<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;

class Registration extends ApiCall {

    public function args() {
        return [
            'email' => [
                'email' => null,
            ],
            'pass' => [
                'pass' => null,
            ],
            'pass_check' => [
                'string' => null,
            ],
        ];
    }

    public function perform($args) {
        $m = Env::$app->m;

        if ($args['pass'] != $args['pass_check']) {
            throw new ApiCallInvalidException($this, array(
                'pass_check' => array(
                    'not_equal' => null,
                ),
            ));
        }

        $user = $m->userAddByEmailPass([
            'email' => $args['email'],
            'pass' => $args['pass'],
        ]);

        if (isset($user['user_id'])) {
            $token = Env::$app->auth->store(['mUserId' => $user['user_id']]);

            return [
                'token' => $token,
                'expires' => Env::$app->auth->ttl + time(),
                'user_id' => $user['user_id'],
            ];
        }

        return [];
    }
}
