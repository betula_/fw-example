<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;

class AuthByEmailPass extends ApiCall {

    public function args() {
        return [
            'email' => [
                'email' => null,
            ],
            'pass' => [
                'string' => null,
            ],
        ];
    }

    public function perform($args) {
        $m = Env::$app->m;

        $user = $m->userGetByEmailPass([
            'email' => $args['email'],
            'pass' => $args['pass'],
        ]);

        if (isset($user['user_id'])) {
            $token = Env::$app->auth->store(['mUserId' => $user['user_id']]);

            return [
                'token' => $token,
                'expires' => Env::$app->auth->ttl + time(),
                'user_id' => $user['user_id'],
            ];
        }

        return [];
    }
}
