<?php
namespace Fw\M\Ajax;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCall;
use Fw\System\DeniedException;
use Imagick;

class PictureUpdate extends ApiCall {

    public function args() {
        return [
            'picture_id' => [
                'uid' => null,
            ],
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            '_files' => [
                'dict' => [
                    'image' => [
                        'file' => [
                            'image' => [
                                'width' => ['max' => 1000],
                                'height' => ['max' => 1000],
                            ]
                        ],
                    ],
                ],
                'optional' => true,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function perform($args) {

        $params = array_intersect_key($args, array_fill_keys([
            'picture_id', 'name', 'title'
        ], true));

        if (isset($args['_files']['image'])) {
            $name = $this->api->createUid().'.png';
            $path = Env::$app->storage->path($name, true);

            $image = new Imagick($args['_files']['image']['tmp_name']);
            $image->setImageFormat('png');
            $image->writeImage($path);
            $image->destroy();

            unlink($args['_files']['image']['tmp_name']);
            $params['image'] = $name;
        }

        Env::$app->m->pictureUpdateById($params);
        return [
            'picture_id' => $params['picture_id'],
        ];
    }

}
