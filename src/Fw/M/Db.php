<?php
namespace Fw\M;
use Exception;
use PDO;
use \Fw\Fw\Env;

class Db extends PDO {
    public function prepare($statement , $options = []) {
        Env::$app->logger->info($statement, 2);
        return parent::prepare($statement, $options);
    }
}