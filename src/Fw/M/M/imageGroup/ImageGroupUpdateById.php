<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ImageGroupUpdateById extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
            ],
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();
        $original = $this->api->imageGroupGetById(['group_id' => $args['group_id']]);

        $values[':group_id'] = $args['group_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';
        if (isset($args['name']) && $original['name'] != $args['name']) {
            $stmt = $this->api->db->prepare('select group_id from ImageGroup where `name` = :name limit 1');
            $stmt->execute([':name' => $args['name']]);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                throw new ApiCallInvalidException($this, [
                    'name' => ['exists' => true],
                ]);
            }
            $values[':name'] = $args['name'];
            $update[] = 'name=:name';
        }
        foreach(['title'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update ImageGroup set %s where group_id=:group_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return $this->api->imageGroupGetById(['group_id' => $args['group_id']]);
    }

}