<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ImageGroupGetById extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from ImageGroup where group_id = ? limit 1');
        $stmt->execute([$args['group_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'group_id' => ['not_found' => null],
            ], 404);
        }
        return $row;
    }

}