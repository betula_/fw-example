<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ContentGetById extends ApiCall {

    public function args() {
        return [
            'content_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from Content where content_id = ? limit 1');
        $stmt->execute([$args['content_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'content_id' => ['not_found' => null],
            ], 404);
        }
        return $row;
    }

}