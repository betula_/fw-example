<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ContentUpdateById extends ApiCall {

    public function args() {
        return [
            'content_id' => [
                'uid' => null,
            ],
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'content' => [
                'string' => null,
                'optional' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();
        $original = $this->api->contentGetById(['content_id' => $args['content_id']]);

        $values[':content_id'] = $args['content_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';
        if (isset($args['name']) && $original['name'] != $args['name']) {
            $stmt = $this->api->db->prepare('select content_id from Content where `name` = :name limit 1');
            $stmt->execute([':name' => $args['name']]);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($row) {
                throw new ApiCallInvalidException($this, [
                    'name' => ['exists' => true],
                ]);
            }
            $values[':name'] = $args['name'];
            $update[] = 'name=:name';
        }
        foreach(['content'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update Content set %s where content_id=:content_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return $this->api->contentGetById(['content_id' => $args['content_id']]);
    }

}