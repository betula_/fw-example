<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ContentAddByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
            'content' => [
                'string' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $stmt = $this->api->db->prepare('select content_id from Content where `name`=:name limit 1');
        $stmt->execute([':name' => $args['name']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            throw new ApiCallInvalidException($this, [
                'name' => ['exists' => true],
            ]);
        }

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Content
            ( content_id, dtc,  dtm,  `name`, content,  owner_id) values
            (:uid,       :time,:time, :name, :content,  :owner_id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':name' => $args['name'],
            ':content' => $args['content'],
            ':owner_id' => $args['owner_id'],
        ]);

        return [
            'content_id' => $uid
        ];
    }

}