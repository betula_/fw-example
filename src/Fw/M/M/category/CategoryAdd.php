<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class CategoryAdd extends ApiCall {

    public function args() {
        return [
            'parent_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
            ],
            'order' => [
                'int' => null,
                'default' => time(),
            ],
            'important' => [
                'flag' => null,
                'default' => 0,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $path = '';
        $depth = 0;
        if (isset($args['parent_id'])) {
            $stmt = $this->api->db->prepare('select * from Category where category_id = ? limit 1');
            $stmt->execute([$args['parent_id']]);
            $row = $stmt->fetch();
            if (!$row) {
                throw new ApiCallInvalidException($this, [
                    'parent_id' => array(
                        'not_found' => null,
                    ),
                ]);
            }
            $path = trim($row['path'].'/'.$row['category_id'],'/');
            $depth = $row['depth']+1;
        }

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Category
            ( category_id,  dtc,  dtm,  path, `order`,  title,  important,  depth,  nesting,  owner_id) values
            (:uid,         :time,:time,:path, :order,  :title, :important, :depth, :nesting, :owner_id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':path' => $path,
            ':order' => $args['order'],
            ':title' => $args['title'],
            ':important' => $args['important'],
            ':depth' => $depth,
            ':nesting' => 0,
            ':owner_id' => $args['owner_id'],
        ]);

        if ($path) {
            $ids = explode('/',$path);
            $stmt = $this->api->db->prepare(sprintf(
                'update Category set nesting = nesting + 1 where category_id in (%s);',
                implode(',', array_fill(0, count($ids), '?'))
                ));
            $stmt->execute($ids);
        }


        return [
            'category_id' => $uid,
        ];

    }

}