<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryParentsGetById extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $row = $this->categoryGetById(['category_id' => $args['category_id']]);

        $list = [];
        if ($row['path']) {
            $ids = explode('/', $row['path']);
            foreach (array_reverse($ids) as $id) {
                $list[] = $this->categoryGetById(['category_id' => $id]);
            }
        }

        return [
            'list' => $list,
        ];
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryGetById($args);
    }

}