<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryTreeGetById extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'depth' => [
                'int' => ['min'=>1],
                'default' => 1,
            ]
        ];
    }

    public function perform ($args) {

        $rows = $this->categoryChildrenGetById([
            'category_id' => $args['category_id'],
            'depth' => $args['depth'],
        ])['list'];

        $tree = [];
        $path = '';
        if (isset($args['category_id'])) {
            $row = $this->categoryGetById(['category_id' => $args['category_id']]);
            $path = trim($row['path'].'/'.$row['category_id'],'/');
        }
        $cut = strlen($path);

        foreach ($rows as $row) {
            $link =& $tree;
            $p = substr($row['path'], $cut);
            if ($p) {
                $piece = explode('/', $p);
                foreach ($piece as $i) {
                    if (!isset($link[$i])) {
                        $link[$i] = ['category'=>null, 'tree'=>[]];
                    }
                    $link =& $link[$i]['tree'];
                }
            }

            $link[$row['category_id']]['category'] = $row;
            unset($link);
        }

        $result = [
            'tree' => $tree,
        ];
        return $result;
    }

    public function categoryChildrenGetById(array $args = []) {
        return $this->api->categoryChildrenGetById($args);
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryGetById($args);
    }

}