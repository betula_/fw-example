<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryChildrenGetById extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
                'default' => null,
            ],
            'depth' => [
                'int' => ['min'=>1],
                'default' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['category_id'].':'.$args['depth'];
        $rows = $this->cacheGet($cacheKey);
        if (!$rows) {
            $path = '';
            $depth = 0;
            if (isset($args['category_id'])) {
                $row = $this->categoryGetById(['category_id' => $args['category_id']]);
                $path = trim($row['path'].'/'.$row['category_id'],'/');
                $depth += 1 + $row['depth'];
            }

            if (isset($args['depth'])) {
                $depth += $args['depth'];
                $stmt = $this->api->db->prepare('select category_id from Category where path like ? and depth < ? order by important desc, `order` asc');
                $stmt->execute(["$path%", $depth]);
            }
            else {
                $stmt = $this->api->db->prepare('select category_id from Category where path like ? order by important desc, `order` asc');
                $stmt->execute(["$path%"]);
            }
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $this->cacheSet($cacheKey, $rows);
        }

        $list = $this->resultPerform($rows);

        $result = [
            'list' => $list,
        ];

        return $result;
    }

    public function resultPerform($rows) {
        $cacheKey = json_encode($rows);
        $list = $this->cacheGet($cacheKey);
        if (!$list) {
            $list = [];
            foreach ($rows as $row) {
                $list[] = $this->categoryGetById(['category_id' => $row['category_id']]);
            }
            $this->cacheSet($cacheKey, $list, 60);
        }

        return $list;
    }

    public function categoryGetById(array $args = []) {
        return $this->api->categoryGetById($args);
    }

}