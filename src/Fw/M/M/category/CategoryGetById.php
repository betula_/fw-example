<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CategoryGetById extends ApiCall {

    public function args() {
        return [
            'category_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cache = $this->cacheGet($args['category_id']);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from Category where category_id = ? limit 1');
        $stmt->execute([$args['category_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'category_id' => [
                    'not_found' => null,
                ],
            ]);
        }

        $this->cacheSet($args['category_id'], $row);
        return $row;
    }

}