<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetUserRole extends ApiCall {

    public function perform ($args) {
        $this->truncate();
        $this->drop();
        $this->create();
        $this->fill();
    }

    public function truncate() {
        $this->api->db->exec('truncate UserRole');
    }

    public function fill() {
        $user = $this->api->userGetByEmailPass([
            'email' => 'a@x.com',
            'pass' => '123456',
        ]);
        $role = $this->api->roleGetByName([
            'name' => 'admin',
        ]);
        $this->api->roleUserAdd([
            'user_id' => $user['user_id'],
            'role_id' => $role['role_id'],
        ]);
    }

    public function drop() {
        $this->api->db->exec('drop table if exists UserRole');
    }

    public function create() {
        $this->api->db->exec('
        CREATE  TABLE IF NOT EXISTS `UserRole` (
          `user_id` CHAR(20) NOT NULL ,
          `role_id` CHAR(20) NOT NULL ,
          `dtm` INT(11) NOT NULL ,
          `dtc` INT(11) NOT NULL ,
          PRIMARY KEY (`user_id`, `role_id`) )
        ENGINE = InnoDB
        ');
    }

}