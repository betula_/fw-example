<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetUser extends ApiCall {


    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function truncate() {
        $this->api->db->exec('truncate User');
        $this->api->db->exec('truncate UserEmailPass');
    }

    public function fill() {
        $this->api->userAddByEmailPass([
            'email' => 'a@x.com',
            'pass' => '123456',
        ]);
    }

    public function drop() {
        $this->api->db->exec('drop table if exists User');
        $this->api->db->exec('drop table if exists UserEmailPass');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `User` (
              `user_id` CHAR(20) NOT NULL ,
              `dtm` INT NOT NULL ,
              `dtc` INT NOT NULL ,
              PRIMARY KEY (`user_id`) ,
              UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) )
            ENGINE = InnoDB
        ');
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `UserEmailPass` (
              `user_id` CHAR(20) NOT NULL ,
              `dtm` INT NOT NULL ,
              `dtc` INT NOT NULL ,
              `pass` CHAR(150) NOT NULL ,
              `email` VARCHAR(200) NOT NULL ,
              PRIMARY KEY (`user_id`) ,
              UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) ,
              UNIQUE INDEX `email_UNIQUE` (`email` ASC) )
            ENGINE = InnoDB
        ');
    }
}