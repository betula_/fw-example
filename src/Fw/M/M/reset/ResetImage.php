<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetImage extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Image');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Image` (
              `image_id` CHAR(20) NOT NULL ,
              `dtm` INT(11) NOT NULL ,
              `dtc` INT(11) NOT NULL ,
              `image` VARCHAR(255) NULL DEFAULT NULL ,
              `order` INT(11) NULL DEFAULT NULL ,
              `owner_id` CHAR(20) NULL DEFAULT NULL ,
              `description` TEXT NULL DEFAULT NULL ,
              `title` VARCHAR(255) NULL DEFAULT NULL ,
              `group_id` CHAR(20) NULL DEFAULT NULL ,
              PRIMARY KEY (`image_id`) ,
              UNIQUE INDEX `image_id_UNIQUE` (`image_id` ASC) )
            ENGINE = InnoDB
        ');
    }

    public function truncate() {
        $this->api->db->exec('truncate Image');
    }

    public function fill() {
    }


}