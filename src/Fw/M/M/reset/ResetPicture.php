<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetPicture extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Picture');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Picture` (
              `picture_id` CHAR(20) NOT NULL ,
              `dtm` INT(11) NOT NULL ,
              `dtc` INT(11) NOT NULL ,
              `name` CHAR(32) NOT NULL ,
              `image` VARCHAR(255) NULL DEFAULT NULL ,
              `owner_id` CHAR(20) NULL DEFAULT NULL ,
              `title` VARCHAR(255) NULL DEFAULT NULL ,
              PRIMARY KEY (`picture_id`) ,
              UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
              UNIQUE INDEX `picture_id_UNIQUE` (`picture_id` ASC) )
            ENGINE = InnoDB
        ');
    }

    public function truncate() {
        $this->api->db->exec('truncate Picture');
    }

    public function fill() {
    }
}