<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetCurrency extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Currency');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Currency` (
              `code` CHAR(3) NOT NULL ,
              `dtm` INT NOT NULL ,
              `dtc` INT NOT NULL ,
              `title` VARCHAR(10) NULL ,
              `order` INT NULL ,
              `important` INT NULL ,
              UNIQUE INDEX `user_id_UNIQUE` (`code` ASC) ,
              PRIMARY KEY (`code`) )
            ENGINE = InnoDB
        ');
    }

    public function truncate() {
        $this->api->db->exec('truncate Currency');
    }

    public function fill() {
        $this->api->currencyAddByCode([
            'code' => 'RUB',
            'important' => 1,
            'order' => 100,
            'title' => 'рубль',
        ]);
        $this->api->currencyAddByCode([
            'code' => 'USD',
            'order' => 200,
            'title' => 'доллар',
        ]);
        $this->api->currencyAddByCode([
            'code' => 'EUR',
            'order' => 300,
            'title' => 'евро'
        ]);
    }


}