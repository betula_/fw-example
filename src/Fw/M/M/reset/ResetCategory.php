<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetCategory extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Category');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Category` (
              `category_id` CHAR(20) NOT NULL ,
              `dtm` INT NOT NULL ,
              `dtc` INT NOT NULL ,
              `path` CHAR(200) NOT NULL ,
              `order` INT NOT NULL ,
              `title` VARCHAR(255) NOT NULL ,
              `important` INT NOT NULL ,
              `depth` INT NOT NULL ,
              `nesting` INT NOT NULL ,
              `owner_id` CHAR(20) NULL ,
              PRIMARY KEY (`category_id`) ,
              UNIQUE INDEX `category_id_UNIQUE` (`category_id` ASC) )
            ENGINE = InnoDB
        ');
    }

    public function truncate() {
        $this->api->db->exec('truncate Category');
    }

    public function fill() {
        $string = $this->getCategoryString();
        if ($string) {
            $lines = explode("\n", trim($string));
            $parents = [null];
            foreach ($lines as $n => $line) {
                $t = mb_strlen($line) - mb_strlen(trim($line));
                $t = (int) $t/4;
                $line = trim($line);
                $p = [
                    'parent_id' => $parents[$t],
                    'title' => trim($line,'*'),
                    'order' => $n*100,
                    'important' => mb_substr($line,0,1) == '*' ? 1 : 0,
                    'unique' => (int)($t == 0),
                ];
                $row = $this->categoryAdd($p);
                $parents[$t+1] = $row['category_id'];
            }
        }
    }

    public function getCategoryString() {
        return null;
    }

    public function categoryAdd(array $args = []) {
        return $this->api->categoryAdd($args);
    }

}