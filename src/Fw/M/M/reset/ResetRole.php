<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetRole extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Role');
    }

    public function truncate() {
        $this->api->db->exec('truncate Role');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Role` (
              `role_id` CHAR(20) NOT NULL ,
              `dtm` INT(11) NOT NULL ,
              `dtc` INT(11) NOT NULL ,
              `name` CHAR(32) NOT NULL ,
              PRIMARY KEY (`role_id`) ,
              UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
            ENGINE = InnoDB
        ');
    }

    public function fill() {
        $this->api->roleAddByName([
            'name' => 'user',
        ]);
        $this->api->roleAddByName([
            'name' => 'admin',
        ]);
    }


}