<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class Reset extends ApiCall {

    public function perform ($args) {
        $this->api->resetContent();
        $this->api->resetRole();
        $this->api->resetUser();
        $this->api->resetUserRole();
        $this->api->resetCategory();
        $this->api->resetCurrency();
        $this->api->resetImage();
        $this->api->resetImageGroup();
        $this->api->resetPicture();
    }

}