<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetImageGroup extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
        $this->fill();
    }

    public function drop() {
        $this->api->db->exec('drop table if exists ImageGroup');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `ImageGroup` (
              `group_id` CHAR(20) NOT NULL ,
              `dtm` INT(11) NOT NULL ,
              `dtc` INT(11) NOT NULL ,
              `owner_id` CHAR(20) NULL DEFAULT NULL ,
              `title` VARCHAR(255) NULL DEFAULT NULL ,
              `name` CHAR(32) NOT NULL ,
              PRIMARY KEY (`group_id`) ,
              UNIQUE INDEX `image_id_UNIQUE` (`group_id` ASC) ,
              UNIQUE INDEX `name_UNIQUE` (`name` ASC) )
            ENGINE = InnoDB
        ');
    }

    public function truncate() {
        $this->api->db->exec('truncate ImageGroup');
    }

    public function fill() {
    }

}