<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ResetContent extends ApiCall {

    public function perform ($args) {
        $this->drop();
        $this->create();
        $this->truncate();
    }

    public function truncate() {
        $this->api->db->exec('truncate Content');
    }

    public function drop() {
        $this->api->db->exec('drop table if exists Content');
    }

    public function create() {
        $this->api->db->exec('
            CREATE  TABLE IF NOT EXISTS `Content` (
              `content_id` CHAR(20) NOT NULL ,
              `dtm` INT NOT NULL ,
              `dtc` INT NOT NULL ,
              `name` CHAR(32) NOT NULL ,
              `content` MEDIUMTEXT NULL DEFAULT NULL ,
              `owner_id` CHAR(20) NULL DEFAULT NULL ,
              PRIMARY KEY (`content_id`) ,
              UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
              UNIQUE INDEX `content_id_UNIQUE` (`content_id` ASC) )
            ENGINE = InnoDB
        ');
    }


}