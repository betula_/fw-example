<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;

class UserGetByEmailPass extends ApiCall {

    public function args() {
        return [
            'email' => [
                'email' => null,
            ],
            'pass' => [
                'string' => null,
            ]
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from UserEmailPass where email = ? limit 1');
        $stmt->execute([$args['email']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$user) {
            throw new ApiCallInvalidException($this, [
                'email' => [
                    'not_found' => null,
                ],
            ]);
        }
        $check = $this->api->passwordCheck($args['pass'],$user['pass']);
        if (!$check) {
            throw new ApiCallInvalidException($this, [
                'email' => [
                    'not_found' => null,
                ],
            ]);
        }
        $user = $this->api->userGetById(['user_id' => $user['user_id']]);
        return $user;
    }

}