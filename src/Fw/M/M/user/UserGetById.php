<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserGetById extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from User where user_id = ? limit 1');
        $stmt->execute([$args['user_id']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$user) {
            throw new ApiCallInvalidException($this, [
                'user_id' => [
                    'not_found' => null,
                ],
            ]);
        }
        return $user;
    }

}