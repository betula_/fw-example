<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserRoleGetById extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from UserRole where user_id = ?');
        $stmt->execute([$args['user_id']]);
        $list = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $role = [];
        foreach($list as $line) {
            $r = $this->api->roleGetById(['role_id' => $line['role_id']]);
            $role[$r['name']] = $r;
        }
        return $role;
    }

}