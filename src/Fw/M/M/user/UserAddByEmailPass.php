<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;

class UserAddByEmailPass extends ApiCall {

    public function args() {
        return [
            'email' => [
                'email' => null,
            ],
            'pass' => [
                'pass' => null,
            ]
        ];
    }

    public function perform ($args) {

        $stmt = $this->api->db->prepare('select * from UserEmailPass where email = ? limit 1');
        $stmt->execute([$args['email']]);
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if (isset($user['user_id'])) {
            throw new ApiCallInvalidException($this, [
                'email' => [
                    'exists' => true,
                ],
            ]);
        }
        $user_id = $this->api->createUid();
        $pass = $this->api->passwordEncrypt($args['pass'], $salt = base_convert(mt_rand(),10,36).base_convert(rand(),10,36));
        $stmt = $this->api->db->prepare('insert into User
            (user_id, dtc, dtm) values
            (?, ?,?)');
        $stmt->execute([
            $user_id,
            $this->api->time,
            $this->api->time,
        ]);

        $stmt = $this->api->db->prepare('insert into UserEmailPass
            (user_id, dtc, dtm, email, pass) values
            (?,?,?,?,?)');
        $stmt->execute([
            $user_id,
            $this->api->time,
            $this->api->time,
            $args['email'],
            $pass,
        ]);

        return [
            'user_id' => $user_id,
        ];
    }

}