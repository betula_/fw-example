<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class UserEmailGetById extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $email = null;
        $stmt = $this->api->db->prepare('select * from UserEmailPass where user_id = ?');
        $stmt->execute([$args['user_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            $email = array_diff_key($row, ['pass'=>true]);
        }
        return $email;
    }

}