<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class PictureAddByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'image' => [
                'string' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $stmt = $this->api->db->prepare('select picture_id from Picture where `name`=:name limit 1');
        $stmt->execute([':name' => $args['name']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            throw new ApiCallInvalidException($this, [
                'name' => ['exists' => true],
            ]);
        }

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Picture
            ( picture_id, dtc,  dtm,  `name`, image,  `title`,  owner_id) values
            (:uid,       :time,:time, :name, :image,  :title,  :owner_id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':name' => $args['name'],
            ':image' => $args['image'],
            ':title' => $args['title'],
            ':owner_id' => $args['owner_id'],
        ]);

        return [
            'picture_id' => $uid,
        ];
    }

}