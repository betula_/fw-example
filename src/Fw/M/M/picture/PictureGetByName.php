<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PictureGetByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from Picture where `name` = ? limit 1');
        $stmt->execute([$args['name']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'name' => ['not_found' => null],
            ], 404);
        }
        return $row;
    }

}