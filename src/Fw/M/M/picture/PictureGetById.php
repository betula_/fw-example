<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PictureGetById extends ApiCall {

    public function args() {
        return [
            'picture_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from Picture where picture_id = ? limit 1');
        $stmt->execute([$args['picture_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'picture_id' => ['not_found' => null],
            ], 404);
        }
        return $row;
    }

}