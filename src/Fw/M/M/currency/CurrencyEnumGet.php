<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CurrencyEnumGet extends ApiCall {

    public function args() {
        return [
        ];
    }

    public function perform ($args) {
        $cacheKey = '';
        $enum = $this->cacheGet($cacheKey);
        if (!$enum) {
            $stmt = $this->api->db->prepare('select code from Currency order by important desc, `order` asc');
            $stmt->execute();
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $enum = [];
            foreach($rows as $row) {
                $enum[] = $row['code'];
            }
            $this->cacheSet($cacheKey, $enum, 3600000);
        }

        return [
            'enum' => $enum,
        ];
    }


}