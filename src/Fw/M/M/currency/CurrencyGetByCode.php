<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CurrencyGetByCode extends ApiCall {

    public function args() {
        return [
            'code' => [
                'string' => ['length'=>['min'=>2,'max'=>3]],
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['code'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from Currency where code = ? limit 1');
        $stmt->execute([$args['code']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'code' => ['not_found' => null],
            ], 404);
        }

        $this->cacheSet($cacheKey, $row, 360000);
        return $row;
    }

}