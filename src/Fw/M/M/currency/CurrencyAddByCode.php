<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class CurrencyAddByCode extends ApiCall {

    public function args() {
        return [
            'code' => [
                'string' => ['length' => ['min'=>2, 'max'=>3]],
            ],
            'title' => [
                'string' => ['length' => ['min'=>2, 'max'=>10]],
                'default' => null,
            ],
            'order' => [
                'int' => null,
                'default' => $this->api->time,
            ],
            'important' => [
                'flag' => null,
                'default' => 0,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();
        $args['code'] = mb_strtoupper($args['code']);

        $stmt = $this->api->db->prepare('select code from Currency where `code`=:code limit 1');
        $stmt->execute([':code' => $args['code']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($row) {
            throw new ApiCallInvalidException($this, [
                'code' => ['exists' => true],
            ]);
        }

        $stmt = $this->api->db->prepare('insert into Currency
            ( code, dtc,  dtm,  `title`,  `order`,  important) values
            (:code,:time,:time, :title,   :order,  :important)');
        $stmt->execute([
            ':code' => $args['code'],
            ':time' => $this->api->time,
            ':title' => $args['title'],
            ':order' => $args['order'],
            ':important' => $args['important'],
        ]);

        return [
            'code' => $args['code'],
        ];
    }

}