<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class CurrencyListGet extends ApiCall {

    public function args() {
        return [
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select code from Currency order by important desc, `order` asc');
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $list = $this->resultPerform($rows);

        $result = [
            'list' => $list,
        ];

        return $result;
    }

    public function resultPerform($rows) {
        $cacheKey = json_encode($rows);
        $list = $this->cacheGet($cacheKey);
        if (!$list) {
            $list = [];
            foreach ($rows as $row) {
                $list[] = $this->currencyGetByCode(['code' => $row['code']]);
            }
            $this->cacheSet($cacheKey, $list, 60);
        }

        return $list;
    }

    public function currencyGetByCode(array $args = []) {
        return $this->api->currencyGetByCode($args);
    }

}