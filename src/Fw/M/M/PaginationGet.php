<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class PaginationGet extends ApiCall {

    public function args() {
        return [
            'count' => [
                'int' => null,
                'default' => 10,
            ],
            'page' => [
                'int' => null,
                'default' => 0,
            ],
            'width' => [
                'int' => null,
                'default' => 7,
            ]
        ];
    }

    public function total($args) {
        return 0;
    }

    public function perform ($args) {

        $total = $this->total($args);

        $min = 0;
        $max = max($min, ceil($total/$args['count'])-1);
        $radius = max(1, floor($args['width']/2));

        $page = $args['page'];

        $left = $page-$radius;
        $right = $page+$radius;

        if ($left < 0) {
            $right -= $left;
        }
        if ($right > $max) {
            $left -= $right-$max;
            $right = $max;
        }
        if ($left < 0) {
            $left = 0;
        }

        $list = [];
        for ($i = $left; $i <= $right; $i++) {
            $list[] = [
                'page' => $i,
                'label' => $i+1,
            ];
        }

        $prev = $page-1 >= 0 ? $page-1 : null;
        $next = $page+1 <= $max ? $page+1 : null;


        $result = [
            'total' => $total,
            'list' => $list,
            'prev' => $prev,
            'next' => $next,
            'start' => $min,
            'end' => $max,
            'page' => $page,
        ];

        return $result;

    }

}