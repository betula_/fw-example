<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ImageUpdateById extends ApiCall {

    public function args() {
        return [
            'image_id' => [
                'uid' => null,
            ],
            'group_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'title' => [
                'string' => null,
                'optional' => null,
            ],
            'description' => [
                'string' => null,
                'optional' => null,
            ],
            'image' => [
                'string' => null,
                'optional' => null,
            ],
            'order' => [
                'int' => null,
                'optional' => null,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();
        $original = $this->api->imageGetById(['image_id' => $args['image_id']]);

        $values[':image_id'] = $args['image_id'];
        $values[':time'] = time();
        $update[] = 'dtm=:time';

        foreach(['image', 'title', 'description', 'order', 'group_id'] as $f) {
            if (isset($args[$f]) && $original[$f] != $args[$f]) {
                $values[":$f"] = $args[$f];
                $update[] = "`$f`=:$f";
            }
        }
        $stmt = $this->api->db->prepare(sprintf(
            'update Image set %s where image_id=:image_id',
            implode(',', $update)
        ));
        $stmt->execute($values);

        return $this->api->imageGetById(['image_id' => $args['image_id']]);
    }

}