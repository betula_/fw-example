<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ImageRemoveById extends ApiCall {

    public function args() {
        return [
            'image_id' => [
                'uid' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();
        $stmt = $this->api->db->prepare('delete from Image where image_id = ?');
        $stmt->execute([$args['image_id']]);
        return [];
    }

}