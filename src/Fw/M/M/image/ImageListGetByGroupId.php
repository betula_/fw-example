<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ImageListGetByGroupId extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select
            image_id, dtm, dtc, group_id, title, `order`, image, owner_id
            from Image
            where group_id=:group_id order by `order`');
        $stmt->execute([
            ':group_id' => $args['group_id'],
        ]);
        $list = $stmt->fetchAll();

        return [
            'list' => $list,
        ];
    }

}