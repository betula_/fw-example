<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class ImageGetById extends ApiCall {

    public function args() {
        return [
            'image_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $stmt = $this->api->db->prepare('select * from Image where image_id = ? limit 1');
        $stmt->execute([$args['image_id']]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$row) {
            throw new ApiCallInvalidException($this, [
                'image_id' => ['not_found' => null],
            ], 404);
        }
        return $row;
    }

}