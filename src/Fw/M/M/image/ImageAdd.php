<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;
use PDO;

class ImageAdd extends ApiCall {

    public function args() {
        return [
            'group_id' => [
                'uid' => null,
                'default' => null,
            ],
            'title' => [
                'string' => null,
                'default' => null,
            ],
            'description' => [
                'string' => null,
                'default' => null,
            ],
            'image' => [
                'string' => null,
                'default' => null,
            ],
            'owner_id' => [
                'uid' => null,
                'default' => null,
            ],
            'order' => [
                'int' => null,
                'default' => $this->api->time,
            ]
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        if (!isset($args['owner_id']) && isset($this->api->user['user_id'])) {
            $args['owner_id'] = $this->api->user['user_id'];
        }

        $uid = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Image
            ( image_id,   dtc,  dtm,  image,  `title`,  owner_id, `order`,  description,  group_id) values
            (:uid,       :time,:time,:image,  :title,  :owner_id, :order,  :description, :group_id)');
        $stmt->execute([
            ':uid' => $uid,
            ':time' => $this->api->time,
            ':image' => $args['image'],
            ':title' => $args['title'],
            ':owner_id' => $args['owner_id'],
            ':order' => $args['order'],
            ':description' => $args['description'],
            ':group_id' => $args['group_id'],
        ]);

        return [
            'image_id' => $uid,
            'group_id' => $args['group_id'],
        ];
    }

}