<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use PDO;
use Fw\System\ApiCallInvalidException;
use Fw\System\DeniedException;


class RoleAddByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
        ];
    }

    public function permit(array $args = []) {
        if (!isset($this->api->user['role']['admin'])) {
            throw new DeniedException();
        }
    }

    public function perform ($args) {
        $this->permit();

        $stmt = $this->api->db->prepare('select * from Role where name = ? limit 1');
        $stmt->execute([$args['name']]);
        $role = $stmt->fetch(PDO::FETCH_ASSOC);

        if (isset($role['role_id'])) {
            throw new ApiCallInvalidException($this, [
                'name' => [
                    'exists' => true,
                ],
            ]);
        }
        $role_id = $this->api->createUid();
        $stmt = $this->api->db->prepare('insert into Role
            (role_id,dtc, dtm,name) values
            (?,?, ?,?)');
        $stmt->execute([
            $role_id,
            $this->api->time,
            $this->api->time,
            $args['name'],
        ]);

        return [
            'role_id' => $role_id,
        ];
    }

}