<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class RoleGetByName extends ApiCall {

    public function args() {
        return [
            'name' => [
                'name' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['name'];
        $roleId = $this->cacheGet($cacheKey);

        if (!$roleId) {
            $stmt = $this->api->db->prepare('select * from Role where name = ? limit 1');
            $stmt->execute([$args['name']]);
            $role = $stmt->fetch(PDO::FETCH_ASSOC);
            if (!$role) {
                throw new ApiCallInvalidException($this, array(
                    'name' => array(
                        'not_found' => null,
                    ),
                ));
            }
            $roleId = $role['role_id'];
            $this->cacheSet($cacheKey, $roleId);
        }

        $role = $this->api->roleGetById(['role_id' => $roleId]);
        return $role;
    }

}