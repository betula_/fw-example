<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class RoleUserAdd extends ApiCall {

    public function args() {
        return [
            'user_id' => [
                'uid' => null,
            ],
            'role_id' => [
                'uid' => null,
            ]
        ];
    }

    public function perform ($args) {
        $user = $this->api->userGetById(['user_id' => $args['user_id']]);
        $role = $this->api->roleGetById(['role_id' => $args['role_id']]);


        $stmt = $this->api->db->prepare('select * from UserRole where user_id = ? and role_id = ? limit 1');
        $stmt->execute([$args['user_id'], $args['role_id']]);
        $userRole = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($userRole) {
            throw new ApiCallInvalidException($this, array(
                'user_role' => [
                    'exists' => true,
                ],
            ));
        }

        $stmt = $this->api->db->prepare('insert into UserRole
            (user_id, role_id, dtc,dtm) values
            (?,?, ?,?)');
        $stmt->execute([
            $user['user_id'],
            $role['role_id'],
            $this->api->time,
            $this->api->time,
        ]);

        return [];
    }

}