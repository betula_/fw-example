<?php
namespace Fw\M\M;
use Fw\M\ApiCall;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use PDO;

class RoleGetById extends ApiCall {

    public function args() {
        return [
            'role_id' => [
                'uid' => null,
            ],
        ];
    }

    public function perform ($args) {
        $cacheKey = $args['role_id'];
        $cache = $this->cacheGet($cacheKey);
        if ($cache) return $cache;

        $stmt = $this->api->db->prepare('select * from Role where role_id = ? limit 1');
        $stmt->execute([$args['role_id']]);
        $role = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$role) {
            throw new ApiCallInvalidException($this, array(
                'role_id' => array(
                    'not_found' => null,
                ),
            ));
        }

        $this->cacheSet($cacheKey, $role);
        return $role;
    }

}