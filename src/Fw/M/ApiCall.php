<?php
namespace Fw\M;
use Fw\System\ApiCall as ApiCallBasic;
use Fw\Fw\Env;
use Fw\System\ApiCallInvalidException;
use Fw\System\ApiCallErrorException;

class ApiCall extends ApiCallBasic {

    /**
     * @var \Fw\M\Api
     */
    protected $api;

}