<?php
namespace Fw\M;
use Fw\System\Api as ApiBasic;
use Fw\Fw\Env;
use Fw\System\Request;
use Fw\System\ApiCallInvalidException;
use PDO;


/**
 * @property \PDO $db
 *
 * @method userGetById()
 * @method userGetByNamePass()
 */

class Api extends ApiBasic {

    public $user = null;

    public function __construct() {
        parent::__construct('M');
    }

    public function passwordCheck($password, $hash) {
        $hash = '$6$rounds=4242$'.$hash;
        return crypt($password, $hash)===$hash;
    }

    public function passwordEncrypt($password, $salt) {
        $hash = explode('$',crypt($password, '$6$rounds=4242$'.$salt.'$'));
        array_splice($hash, 0, 3);
        return implode('$',$hash);
    }

    public function currentUserSetById($user_id) {
        $user = $this->userGetById(['user_id' => $user_id]);
        $user['role'] = $this->userRoleGetById(['user_id' => $user_id]);
        $user['email'] = $this->userEmailGetById(['user_id' => $user_id]);
        $this->user = $user;
    }

    public function registerValidators($validator) {
        $validator->checkers['name'] = function(&$value, &$errors, $params=null, $varName=null) {
            if (!is_string($value) && !is_numeric($value)) {
                $errors[$varName]['name.type'] = gettype($value);
                return 1;
            }
            $value = (string)$value;
            $errorCount = 0;
            if (!isset($params['encoding'])) {
                $params['encoding'] = 'utf-8';
            }
            $len = mb_strlen($value, $params['encoding']);

            if ($len > 32) {
                $errors[$varName]['name.length.max'] = 32;
                $errorCount++;
            }
            if ($len < 1) {
                $errors[$varName]['name.length.min'] = 1;
                $errorCount++;
            }
            if (!preg_match("~^[A-z_][A-z0-9_]*$~", $value)) {
                $errors[$varName]['name.format'] = null;
                $errorCount++;
            }
            return $errorCount;
        };

        $validator->checkers['currency'] = function(&$value, &$errors, $params=null, $varName=null) {
            if (!is_string($value)) {
                $errors[$varName]['currency.type'] = gettype($value);
                return 1;
            }
            $value = mb_strtoupper($value);
            $enum = $this->currencyEnumGet()['enum'];
            if (!in_array($value, $enum)) {
                $errors[$varName]['currency'] = $enum;
                return 1;
            }
        };

        $validator->checkers['price'] = function(&$value, &$errors, $params=null, $varName=null) {
            if (!is_scalar($value)) {
                $errors[$varName]['price.type'] = gettype($value);
                return 1;
            }
            if (!preg_match('~^[0-9]*((\.|,)[0-9]*)?$~', $value)) {
                $errors[$varName]['price'] = null;
                return 1;
            }
            $value = str_replace(',','.',$value);
            $value = round($value, 2);

            return 0;
        };
    }

}