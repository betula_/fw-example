<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class RichText extends Component {

    public function properties() {
        return parent::properties() + [
            'text' => [
                'string' => null,
                'default' => '',
            ]
        ];
    }

    public function exec() {
        parent::exec();
    }

}
