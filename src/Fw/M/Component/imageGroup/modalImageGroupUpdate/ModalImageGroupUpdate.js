var self = this;

this.dom.bind('imageGroupUpdateNeed', function(e, data) {
    self.children('formImageGroupUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('imageGroupUpdateOk', function(e) {
    self.hide();
});