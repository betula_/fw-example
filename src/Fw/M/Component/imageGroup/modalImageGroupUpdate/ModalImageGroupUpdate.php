<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalImageGroupUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formImageGroupUpdate');
    }

}
