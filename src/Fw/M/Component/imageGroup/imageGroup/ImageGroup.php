<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class ImageGroup extends Component {

    public function properties() {
        return parent::properties() + [
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'group_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'name';
        $this->_export[] = 'group_id';
        $this->_export[] = 'managed';

        $group = null;
        if (isset($this->group_id)) {
            $group = Env::$app->m->imageGroupGetById(['group_id' => $this->group_id]);
        }
        elseif (isset($this->name)) {
            $group = Env::$app->m->imageGroupGetByName(['name' => $this->name]);
            $this->group_id = $group['group_id'];
        }
        else {
            $this->_template = 'empty';
            return;
        }

        $this->title = $group['title'];

        $this->insertImageList();
        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalImageGroupUpdate');
        Env::$app->c->trigger('modalAdd', 'modalImageAdd');
    }

    public function insertImageList() {

        $list = Env::$app->m->imageListGetByGroupId(['group_id' => $this->group_id]);

        foreach ($list['list'] as $i) {
            $this->insert('list', 'image', [
                'image_id' => $i['image_id'],
                'managed' => $this->managed,
            ]);
        }
    }

}
