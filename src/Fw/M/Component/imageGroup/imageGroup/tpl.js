var self = this;

this.dom.bind('imageGroupUpdateOk', function(e, data) {
    if (data.group_id == self.prop.group_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="image-group-update"]').click(function() {
        $.event.trigger('imageGroupUpdateNeed', {
            group_id: self.prop.group_id
        });
    });
    this.dom.find('[data-action="image-add"]').click(function() {
        $.event.trigger('imageAddNeed', {
            group_id: self.prop.group_id
        });
    });
}

this.dom.bind('imageAddOk', function(e, data) {
    if (data.group_id == self.prop.group_id) {
        self.refresh();
    }
});
this.dom.bind('imageRemoveOk', function(e, data) {
    if (data.group_id == self.prop.group_id) {
        self.refresh();
    }
});