<div
    class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>"
    id="<?=$E($c->_id)?>">

<?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-info" data-action="image-group-update">
            <i class="icon-pencil icon-white"></i>
        </button>
        <button class="btn btn-success" data-action="image-add">
            <i class="icon-plus icon-white"></i>
        </button>
    </div>
    <?endif;?>
<?$END()?>

<?$BLOCK('title')?>
    <h3><?=$E($c->title)?></h3>
<?$END()?>
<?$BLOCK('body')?>
    <ul>
        <?foreach($c->renderSlotComponents('list') as $i):?>
        <li><?=$i?></li>
        <?endforeach?>
    </ul>
<?$END()?>

</div>