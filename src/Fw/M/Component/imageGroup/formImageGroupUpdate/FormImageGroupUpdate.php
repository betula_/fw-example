<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class FormImageGroupUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'group_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'group_id';

        if (isset($this->group_id)) {
            $item = Env::$app->m->imageGroupGetById(['group_id' => $this->group_id]);
            $this->name = $item['name'];
            $this->title = $item['title'];
        } else {
            $this->_template = 'empty';
        }

    }

}
