<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <form method="post" action="">

        <div class="alert alert-error hide" data-error="">
            <button class="close" data-dismiss="alert">×</button>
            Ошибка
        </div>

        <div class="control-group" data-error="content">
            <div class="row">
                <div class="span6">

                    <div class="control-group" data-error="title">
                        <label>Заголовок</label>
                        <input type="text" class="span6" name="title" value="<?=$E($c->title)?>" />
                    </div>

                </div>
            </div>
        </div>

    </form>

</div>
