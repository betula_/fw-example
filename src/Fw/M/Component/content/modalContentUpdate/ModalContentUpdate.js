var self = this;

this.dom.bind('contentUpdateNeed', function(e, data) {
    self.children('formContentUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('contentUpdateOk', function(e) {
    self.hide();
});