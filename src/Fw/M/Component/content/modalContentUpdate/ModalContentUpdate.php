<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class ModalContentUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formContentUpdate');
    }

}
