var self = this;

this.dom.bind('contentUpdateOk', function(e, data) {
    if (data.content_id == self.prop.content_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="content-update"]').click(function() {
        $.event.trigger('contentUpdateNeed', {
            content_id: self.prop.content_id
        });
    });
}
