<div
    class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>"
    id="<?=$E($c->_id)?>">

    <?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-mini btn-info" data-action="content-update">
            <i class="icon-pencil icon-white"></i>
        </button>
    </div>
    <?endif;?>
    <?$END()?>

    <?$BLOCK('body')?>
    <?=$c->renderSlot('text')?>
    <?$END()?>

</div>

