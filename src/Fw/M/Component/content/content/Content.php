<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Content extends Component {

    public function properties() {
        return parent::properties() + [
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'content_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        $this->_export[] = 'name';
        $this->_export[] = 'content_id';
        $this->_export[] = 'managed';

        $content = null;
        if (isset($this->content_id)) {
            $content = Env::$app->m->contentGetById(['content_id' => $this->content_id]);
        }
        elseif (isset($this->name)) {
            $content = Env::$app->m->contentGetByName(['name' => $this->name]);
            $this->content_id = $content['content_id'];
        }
        else {
            $this->_template = 'empty';
            return;
        }

        $this->insert('text', 'richText', ['text' => $content['content']]);
        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalContentUpdate');
    }

}
