var self = this;

this.contentUpdate = function(options) {
    options.data.content_id = self.prop.content_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.contentUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["contentUpdate"]);?>';

    self.ajax(options);
}

this.contentUpdateOk = function(data) {
    $.event.trigger('contentUpdateOk', data);
}