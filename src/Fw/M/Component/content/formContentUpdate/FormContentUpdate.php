<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class FormContentUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'content_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'content_id';

        if (isset($this->content_id)) {
            $item = Env::$app->m->contentGetById(['content_id' => $this->content_id]);
            $this->name = $item['name'];

            $this->insert('contentEditor', 'richTextEditor', [
                'name' => 'content',
                'rows' => 10,
                'value' => $item['content'],
            ]);
        } else {
            $this->_template = 'empty';
        }

    }

}
