<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <form method="post" action="">

        <div class="alert alert-error hide" data-error="">
            <button class="close" data-dismiss="alert">×</button>
            <p>Ошибка</p>
        </div>

        <div class="control-group" data-error="content">
            <div class="row">
                <div class="span10">
                    <label>Содержимое</label>
                    <?=$c->renderSlot('contentEditor')?>
                </div>
            </div>
        </div>

    </form>

</div>
