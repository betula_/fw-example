<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Picture extends Component {

    public function properties() {
        return parent::properties() + [
            'name' => [
                'name' => null,
                'optional' => null,
            ],
            'picture_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'width' => [
                'int' => null,
                'default' => 0,
            ],
            'height' => [
                'int' => null,
                'default' => 0,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        array_push(
            $this->_export,
            'name',
            'picture_id',
            'width',
            'height',
            'managed'
        );

        $picture = null;
        if (isset($this->picture_id)) {
            $picture = Env::$app->m->pictureGetById(['picture_id' => $this->picture_id]);
        }
        elseif (isset($this->name)) {
            $picture = Env::$app->m->pictureGetByName(['name' => $this->name]);
            $this->picture_id = $picture['picture_id'];
        }
        else {
            $this->_template = 'empty';
            return;
        }

        $this->image = $picture['image'];
        $this->title = $picture['title'];

        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalPictureUpdate');
    }

}
