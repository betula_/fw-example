var self = this;

if (self.prop.managed) {
    this.dom.find('[data-action="picture-update"]').click(function() {
        $.event.trigger('pictureUpdateNeed', {
            picture_id: self.prop.picture_id
        });
    });
};

this.dom.bind('pictureUpdateOk', function(e, data) {
    if (data.picture_id == self.prop.picture_id) {
        self.refresh();
    }
});

