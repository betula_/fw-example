<div id="<?=$E($c->_id)?>" class="<?=$c->getCssClass()?> <?if($c->managed) echo 'managed';?>">

    <?$BLOCK('manage')?>
    <?if($c->managed):?>
    <div class="manage-control">
        <button class="btn btn-info" data-action="picture-update">
            <i class="icon-pencil icon-white"></i>
        </button>
    </div>
    <?endif;?>
    <?$END()?>

    <?$BLOCK('body')?>
    <img src="<?=$c->path('image', $c->image, $c->width, $c->height)?>" alt="<?=$E($c->title)?>" />
    <?$END()?>

</div>