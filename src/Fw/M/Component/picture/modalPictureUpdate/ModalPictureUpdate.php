<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalPictureUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formPictureUpdate');
    }

}
