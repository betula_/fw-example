var self = this;

this.dom.bind('pictureUpdateNeed', function(e, data) {
    self.children('formPictureUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('pictureUpdateOk', function(e) {
    self.hide();
});