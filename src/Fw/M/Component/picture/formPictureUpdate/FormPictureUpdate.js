var self = this;

this.pictureUpdate = function(options) {
    options.data.picture_id = self.prop.picture_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        this.pictureUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["pictureUpdate"]);?>';

    self.ajax(options);
}

this.pictureUpdateOk = function(data) {
    $.event.trigger('pictureUpdateOk', data);
}
