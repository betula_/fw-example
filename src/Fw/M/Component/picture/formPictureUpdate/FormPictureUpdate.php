<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class FormPictureUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'picture_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'picture_id';

        if (isset($this->picture_id)) {
            $item = Env::$app->m->pictureGetById(['picture_id' => $this->picture_id]);
            $this->image = $item['image'];
            $this->name = $item['name'];
            $this->title = $item['title'];
        } else {
            $this->_template = 'empty';
        }

    }

}
