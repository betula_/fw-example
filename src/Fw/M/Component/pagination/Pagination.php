<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Pagination extends Component {

    public function properties() {
        return parent::properties() + [
            'page' => [
                'int' => null,
                'default' => 0,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'page';

        $this->start = null;
        $this->end = null;
        $this->prev = null;
        $this->next = null;
        $this->list = [];

    }

    public function pathPage($page) {
        return '#'.$page;
    }


}
