<div class="pagination <?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <ul>
        <?$BLOCK('start')?>
        <?if($c->page != $c->start):?>
        <li><a href="<?=$c->pathPage($c->start)?>"><?$BLOCK('startLabel')?>«<?$END()?></a></li>
        <?else:?>
        <li class="disabled"><span><?$BLOCK('startLabelDisabled')?>«<?$END()?></span></li>
        <?endif;?>
        <?$END()?>

        <?$BLOCK('prev')?>
        <?if(isset($c->prev)):?>
        <li><a href="<?=$c->pathPage($c->prev)?>"><?$BLOCK('prevLabel')?>‹<?$END()?></a></li>
        <?else:?>
        <li class="disabled"><span><?$BLOCK('prevLabelDisabled')?>‹<?$END()?></span></li>
        <?endif;?>
        <?$END()?>

        <?$BLOCK('list')?>
        <?foreach($c->list as $i):?>
        <?if($c->page == $i['page']):?>
        <li class="active"><span><?=$E($i['label'])?></span></li>
        <?else:?>
        <li><a href="<?=$c->pathPage($i['page'])?>"><?=$E($i['label'])?></a></li>
        <?endif;?>
        <?endforeach;?>
        <?$END()?>

        <?$BLOCK('next')?>
        <?if(isset($c->next)):?>
        <li><a href="<?=$c->pathPage($c->next)?>"><?$BLOCK('nextLabel')?>›<?$END()?></a></li>
        <?else:?>
        <li class="disabled"><span><?$BLOCK('nextLabelDisabled')?>›<?$END()?></span></li>
        <?endif;?>
        <?$END()?>

        <?$BLOCK('end')?>
        <?if($c->page != $c->end):?>
        <li><a href="<?=$c->pathPage($c->end)?>"><?$BLOCK('endLabel')?>»<?$END()?></a></li>
        <?else:?>
        <li class="disabled"><span><?$BLOCK('endLabelDisabled')?>»<?$END()?></span></li>
        <?endif;?>
        <?$END()?>
    </ul>
</div>
