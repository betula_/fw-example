var self = this;

this.dom.find('[data-action="image-remove"]').click(function() {
    self.imageRemove({
        ok: function() {
            self.hide();
        },
        error: function() {},
        data: {
            image_id: self.prop.image_id
        }
    });
});
