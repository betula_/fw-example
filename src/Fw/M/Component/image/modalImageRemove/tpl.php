
<?$BLOCK('header')?>
<h3>Удаление изображения</h3>
<?$END()?>

<?$BLOCK('middle')?>
<p>Подтвердите удаление изображения</p>
<?$END()?>

<?$BLOCK('footer')?>
<button class="btn" data-dismiss="modal">Отмена</button>
<button class="btn btn-primary" data-action="image-remove">Удалить</button>
<?$END()?>
