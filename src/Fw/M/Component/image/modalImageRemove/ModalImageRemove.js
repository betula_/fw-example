var self = this;


this.dom.bind('imageRemoveNeed', function(e, data) {
    self.prop.image_id = data.image_id;
    self.show();
});

this.imageRemove = function(data) {
    var ok = data.ok;
    data.ok = function(response) {
        if (ok) ok.call(this, response);
        self.imageRemoveOk(response.result);
    }
    data.url = '<?=$this->path("ajax", ["imageRemove"]);?>';

    self.ajax(data);
}

this.imageRemoveOk = function(data) {
    $.event.trigger('imageRemoveOk', data);
}