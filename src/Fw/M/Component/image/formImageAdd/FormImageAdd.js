var self = this;

this.imageAdd = function(options) {
    options.data.group_id = self.prop.group_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.imageAddOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["imageAdd"]);?>';

    self.ajax(options);
}

this.imageAddOk = function(data) {
    $.event.trigger('imageAddOk', data);
}