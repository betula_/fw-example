<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class FormImageAdd extends Form {

    public function properties() {
        return parent::properties() + [
            'group_id' => [
                'uid' => null,
                'optional' => null,
            ],
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'group_id';

        if (!isset($this->group_id)) {
            $this->_template = 'empty';
            return;
        }

        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
        ]);
    }

}
