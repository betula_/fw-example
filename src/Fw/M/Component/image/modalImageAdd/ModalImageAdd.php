<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalImageAdd extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formImageAdd');
    }

}
