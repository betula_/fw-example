var self = this;

this.dom.bind('imageAddNeed', function(e, data) {
    self.children('formImageAdd')[0].reset(data);
    self.show();
});

this.dom.bind('imageAddOk', function(e) {
    self.hide();
});