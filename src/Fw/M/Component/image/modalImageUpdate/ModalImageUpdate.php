<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalImageUpdate extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formImageUpdate');
    }

}
