var self = this;

this.dom.bind('imageUpdateNeed', function(e, data) {
    self.children('formImageUpdate')[0].reset(data);
    self.show();
});

this.dom.bind('imageUpdateOk', function(e) {
    self.hide();
});