<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
    <form method="post" action="">

        <div class="alert alert-error hide" data-error="">
            <a class="close" data-dismiss="alert" href="#">×</a>
            Ошибка
        </div>
        <div class="alert alert-warning hide" data-error="_files-image">
            <a class="close" data-dismiss="alert" href="#">×</a>
            Картинка нехорошая
        </div>
        <div class="alert alert-warning hide" data-error="_files-image__file-image-width _files-image__file-image-height">
            <a class="close" data-dismiss="alert" href="#">×</a>
            Разрешение картинки не подходит
        </div>

        <div class="row">
            <div class="span1">
                <div class="img-polaroid hide" data-preview="image">
                    <div class="preview" ></div>
                </div>
            </div>
            <div class="span4">

                <div class="control-group" data-error="_files-image">
                    <input type="file" name="image" />
                </div>

                <div class="control-group" data-error="title">
                    <label>Заголовок</label>
                    <input type="text" class="span4" name="title" value="<?=$E($c->title)?>" />
                </div>

                <div class="control-group" data-error="description">
                    <label>Описание</label>
                    <?=$c->renderSlot('description')?>
                </div>

                <div class="control-group" data-error="order">
                    <label>Порядковый номер</label>
                    <input type="text" class="span2" name="order" value="<?=$E($c->order)?>"/>
                </div>

            </div>
            <?if(isset($c->image)):?>
            <div class="span1">
                <div class="img-polaroid">
                    <img src="<?=$c->path('image', $c->image,60,60)?>" class="image" />
                </div>
            </div>
            <?endif;?>
        </div>

    </form>

</div>