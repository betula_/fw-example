<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class FormImageUpdate extends Form {

    public function properties() {
        return parent::properties() + [
            'image_id' => [
                'uid' => null,
                'optional' => null,
            ]
        ];
    }

    public function exec() {
        parent::exec();
        $this->_export[] = 'image_id';

        $item = null;
        if (isset($this->image_id)) {
            $item = Env::$app->m->imageGetById(['image_id' => $this->image_id]);
        } else {
            $this->_template = 'empty';
            return;
        }

        $this->image = $item['image'];
        $this->title = $item['title'];
        $this->order = $item['order'];

        $this->insert('description', 'richTextEditor', [
            'name' => 'description',
            'rows' => 5,
            'value' => $item['description'],
        ]);

    }

}
