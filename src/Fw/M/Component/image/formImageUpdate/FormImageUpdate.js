var self = this;

this.imageUpdate = function(options) {
    options.data.image_id = self.prop.image_id;
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.imageUpdateOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["imageUpdate"]);?>';

    self.ajax(options);
}

this.imageUpdateOk = function(data) {
    $.event.trigger('imageUpdateOk', data);
}