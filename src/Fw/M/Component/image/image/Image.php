<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class Image extends Component {

    public function properties() {
        return parent::properties() + [
            'image_id' => [
                'uid' => null,
                'optional' => null,
            ],
            'width' => [
                'int' => null,
                'default' => 0,
            ],
            'height' => [
                'int' => null,
                'default' => 0,
            ],
            'managed' => [
                'bool' => null,
                'default' => false,
            ],
        ];
    }

    public function exec() {
        parent::exec();

        array_push(
            $this->_export,
            'image_id',
            'width',
            'height',
            'managed'
        );

        $image = null;
        if (isset($this->image_id)) {
            $image = Env::$app->m->imageGetById(['image_id' => $this->image_id]);
        }
        else {
            $this->_template = 'empty';
            return;
        }

        $this->image = $image['image'];
        $this->title = $image['title'];
        $this->description = $image['description'];

        $this->insertManage();
    }

    public function insertManage() {
        if (!$this->managed) return;

        Env::$app->c->trigger('modalAdd', 'modalImageUpdate');
        Env::$app->c->trigger('modalAdd', 'modalImageRemove');
    }

}
