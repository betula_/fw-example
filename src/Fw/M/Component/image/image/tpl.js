var self = this;

this.dom.bind('imageUpdateOk', function(e, data) {
    if (data.image_id == self.prop.image_id) {
        self.refresh();
    }
});

if (self.prop.managed) {
    this.dom.find('[data-action="image-update"]').click(function() {
        $.event.trigger('imageUpdateNeed', {
            image_id: self.prop.image_id
        });
    });
    this.dom.find('[data-action="image-remove"]').click(function() {
        $.event.trigger('imageRemoveNeed', {
            image_id: self.prop.image_id
        });
    });
}
