<?php
namespace Fw\M\Component;
use Fw\C\Component\Component;
use Fw\Fw\Env;

class RichTextEditor extends Component {

    public function properties() {
        return parent::properties() + [
            'name' => [
                'string' => null,
            ],
            'value' => [
                'string' => null,
                'default' => '',
            ],
            'rows' => [
                'int' => null,
                'default' => 5,
            ],
            'placeholder' => [
                'string' => null,
                'default' => '',
            ],
        ];
    }

    public function exec() {
        parent::exec();

    }

}
