<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">
<?$BLOCK('form')?>
    <form action="#login" class="form-horizontal" method="post">

        <div class="alert alert-error hide" data-error="">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Такого пользователя не существует
        </div>

        <div class="control-group" data-error="email">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" placeholder="Email" />
                <span class="help-block">Адрес вашей электронной почты</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Пароль</label>
            <div class="controls">
                <input type="password" name="pass" placeholder="Пароль" />
                <span class="help-block">Пароль для входа в личный кабинет</span>
            </div>
        </div>
    </form>
<?$END()?>
</div>
