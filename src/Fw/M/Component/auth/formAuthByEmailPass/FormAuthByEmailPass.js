var self = this;

this.authByEmailPass = function(options) {
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.authByEmailPassOk(response.result);
    }
    options.url = '<?=$this->path("ajax", ["authByEmailPass"]);?>';

    self.ajax(options);
}

this.authByEmailPassOk = function(data) {
    $.event.trigger('authByEmailPassOk', data);
    $.event.trigger({
        type: 'tokenChange',
        token: data.token,
        expires: new Date(data.expires*1000)
    });
}