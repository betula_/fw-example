<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalAuthByEmailPass extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formAuthByEmailPass');
    }

}
