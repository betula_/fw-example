var self = this;

this.dom.bind('authEndNeed', function(e, data) {
    self.show();
});

this.authEnd = function(data) {
    var ok = data.ok;
    data.ok = function(response) {
        if (ok) ok.call(this, response);
        self.authEndOk(response.result);
    }
    data.url = '<?=$this->path("ajax", ["authEnd"]);?>';

    self.ajax(data);
}

this.authEndOk = function(data) {
    $.event.trigger('authEndOk', data);
    $.event.trigger({
        type: 'tokenChange',
        token: data.token,
        expires: new Date(data.expires*1000)
    });;
}

