<div class="modal hide fade <?=$c->getCssClass()?>" id="<?=$E($c->_id)?>" tabindex="-1" role="dialog">
    <div class="modal-header">
        <a href="#" class="close" data-dismiss="modal">×</a>
        <?=$BLOCK('header')?>
        <?$END()?>
    </div>
    <div class="modal-body">
        <?$BLOCK('middle')?>
        <?=$c->renderSlot('body')?>
        <?$END()?>
    </div>
    <div class="modal-footer">
        <?$BLOCK('footer')?>
        <button class="btn" data-dismiss="modal">Закрыть</button>
        <?$END()?>
    </div>
</div>
