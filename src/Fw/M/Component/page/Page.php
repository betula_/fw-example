<?php
namespace Fw\M\Component;
use Fw\C\Component\Page as PageBasic;
use Fw\Fw\Env;

class Page extends PageBasic {

    public function properties() {
        return parent::properties() + [
        ];
    }

    public function exec() {
        parent::exec();

        Env::$app->c->bind('modalAdd', function($event, $name){
            static $modal = [];
            if (isset($modal[$name])) {
                return;
            }
            $modal[$name] = true;
            $this->insert('modal', $name);
        });

    }

}
