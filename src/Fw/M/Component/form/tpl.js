var self = this;

this.formInit = function(form, options) {
    form.find('[data-error]').bind('close', function(e) {
        e.preventDefault();
        e.stopPropagation();
        self.formErrorHide($(this));
    });
    form.keydown(function(e) {
        if (e.keyCode == 13) form.submit();
    });
    self.submit = function() {
        form.submit();
    }

    self.formImagePreview(form);

    form.submit(function(e) {
        e.preventDefault();

        var data = {};
        $.map(form.serializeArray(), function(v){
            data[v.name] = v.value;
        });

        var files = form.find('input[type=file]');
        if (files.length) {
            options.html5Method = true;
            files.each(function() {
                data[this.name] = this.files[0];
            });
        }

        if (options.serialize) {
            var values = options.serialize.call(form, data);
            for (var i in values) {
                data[i] = values[i];
            }
        }
        options.data = data;

        var ok = options.ok;
        options.ok = function() {
            self.formShowErrors(form);
            if (ok) ok.apply(this, arguments);
        }
        var invalid = options.invalid;
        options.invalid = function(response) {
            self.formShowErrors(form, response.errors);
            if (invalid) invalid.apply(this, arguments);
        }
        var error = options.error;
        options.error = function() {
            self.formShowErrors(form, {});
            if (error) error.apply(this, arguments);
        }

        if (options.send) {
            options.send.call(this, options);
        } else {
            self.ajax(options);
        }
    });
}

this.formErrorShow = function(element) {
    element.addClass('error');
    element.find('input').focus(function() {
        element.removeClass('error');
    });
    element.keydown(function() {
        element.removeClass('error');
    });
}

this.formErrorHide = function(element) {
    element.removeClass('error');
}

this.formShowErrors = function(form, errors) {
    if (errors === undefined) {
        form.find('[data-error]').each(function() {
            self.formErrorHide($(this));
        });
    } else {
        form.find('[data-error]').each(function() {
            var t = $(this);
            var count = 0;
            for (var field in errors) {
                var fieldPiece = field.split('.');
                for (var i=0, fieldKey = fieldPiece[i]; i < fieldPiece.length; i++, fieldKey += '-'+fieldPiece[i]) {
                    for (var error in errors[field]) {
                        var errorPiece = error.split('.');
                        for (var j=0, errorKey = errorPiece[j]; j < errorPiece.length; j++, errorKey += '-'+errorPiece[j]) {
                            if (t.is('[data-error~="'+fieldKey+'__'+errorKey+'"]')) {
                                self.formErrorShow(t);
                                count ++;
                            }
                        }
                    }
                    if (t.is('[data-error~="'+fieldKey+'"]')) {
                        self.formErrorShow(t);
                        count ++;
                    }
                }
            }
            if (t.is('[data-error=""]')) {
                self.formErrorShow(t);
                count ++;
            }
            if (!count) {
                self.formErrorHide(t);
            }
        });
    }

}

this.formImagePreview = function(form) {
    var files = form.find('input[type="file"]');
    form.find('[data-preview]').each(function() {
        var box = $(this);
        var name = box.attr('data-preview');
        var image = files.filter('[name="'+name+'"]');
        var preview = box.find('.preview');

        preview.css({
            background: 'transparent 50% 50% no-repeat',
            backgroundSize: 'cover'
        });
        image.change(function() {
            var file = this.files[0];
            box.addClass('hide');
            preview.css('background-image','none');

            if (!file) return;
            if (!/^image/.test(file.type)) {
                return true;
            }
            if (window.FileReader !== undefined) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    preview.css('background-image', 'url("'+e.target.result+'")');
                    box.removeClass('hide');
                }
                reader.readAsDataURL(file);
            }
        });

    });
}
