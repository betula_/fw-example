var self = this;

var ajax = this.ajax;
this.ajax = function(options) {
    if (options.html5Method) {
        options.ajax = function(p) {
            var data = new FormData(); // IE10+
            for (var i in p.data) {
                if (p.data[i] !== undefined) {
                    data.append(i, p.data[i]);
                }
            }
            var request = new XMLHttpRequest();
            request.open(p.type, p.url);

            for (var i in p.headers) {
                request.setRequestHeader(i, p.headers[i]);
            }

            var timeout = setTimeout(function() {
                request.abort();
                p.error.call(p.context, 'Timeout');
            }, p.timeout);

            request.onreadystatechange = function() {
                if (request.readyState != 4) return;
                clearTimeout(timeout);

                if (request.status == 200) {
                    var response = JSON.parse(request.responseText);   // IE9+
                    p.success.call(p.context, response);
                } else {
                    p.error.call(p.context, request.statusText);
                }
            }

            request.send(data);
        }
    }
    ajax.call(this, options);
}

this.reset = function(data) {
    for (var i in data) {
        this.prop[i] = data[i];
    }
    this.refresh();
}
