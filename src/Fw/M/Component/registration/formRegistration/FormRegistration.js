var self = this;

this.registration = function(options) {
    var ok = options.ok;
    options.ok = function(response) {
        if (ok) ok.call(this, response);
        self.registration(response.result);
    }
    options.url = '<?=$this->path("ajax", ["registration"]);?>';

    self.ajax(options);
}

this.registrationOk = function(data) {
    $.event.trigger('registrationOk', data);
    $.event.trigger({
        type: 'tokenChange',
        token: data.token,
        expires: new Date(data.expires*1000)
    });
}
