<div class="<?=$c->getCssClass()?>" id="<?=$E($c->_id)?>">

    <form action="" class="form-horizontal" method="post">

        <div class="alert alert-warning hide" data-error="">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Ошибка регистрации
        </div>
        <div class="alert alert-error hide" data-error="email">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Невозможно использовать этот электронный ящик
        </div>
        <div class="alert alert-error hide" data-error="pass_check">
            <button type="button" class="close" data-dismiss="alert">×</button>
            Пароли не совпадают
        </div>

        <div class="control-group" data-error="email">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" placeholder="Email" />
                <span class="help-block">Адрес вашей электронной почты</span>
            </div>
        </div>
        <div class="control-group" data-error="pass">
            <label class="control-label">Пароль</label>
            <div class="controls">
                <input type="password" name="pass" placeholder="Пароль" />
                <span class="help-block">Пароль для входа в личный кабинет</span>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Пароль еще раз</label>
            <div class="controls">
                <input type="password" name="pass_check" placeholder="Пароль" />
            </div>
        </div>
    </form>


</div>
