<?php
namespace Fw\M\Component;
use Fw\Fw\Env;

class ModalRegistration extends Modal {

    public function exec() {
        parent::exec();
        $this->insert('body', 'formRegistration');
    }

}
