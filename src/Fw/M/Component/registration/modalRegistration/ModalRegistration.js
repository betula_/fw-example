var self = this;

this.dom.bind('registrationNeed', function() {
    self.show();
});
this.dom.bind('registrationOk', function() {
    self.hide();
});