<?php
namespace Fw\M;
use Fw\Fw\Module as ModuleBasic;
use Fw\Fw\Env;
use Fw\System\Request;
use Fw\System\Response;
use Fw\System\Router;
use Fw\System\Console;
use Fw\System\Validator;
use Fw\System\Auth;
use Exception;
use PDO;

class Module extends ModuleBasic {

    public function getRelatedModules() {
        return [
            'System\\Module',
        ];
    }

    public function init() {
        Env::$app->cfg['currencyDefault'] = 'RUB';

        Env::$app->bind('authCreated', function($event, Auth $auth) {
            $auth->bind('start', function($event, $session) {
                if (isset($session['mUserId'])) {
                    Env::$app->m->currentUserSetById($session['mUserId']);
                }
            });
            $auth->bind('stop', function() {
                Env::$app->m->user = null;
            });
        });

        Env::$app->serviceLoaders['m'] = function() {
            $api = new Api();
            $api->lazyLoaders['db'] = function(){
                //return new PDO();
            };
            $api->lazyLoaders['cache'] = function(){
                return Env::$app->cache;
            };
            return $api;
        };

        Env::$app->bind('build', function(){
            Env::$app->m->build();
        });

        Env::$app->bind('consoleCreated', function($event, Console $console) {
            Env::$app->m->registerConsoleCommands('m', $console);
            Env::$app->m->user = [                 // TODO: check permission for console
                'role' => ['admin' => true],
                'user_id' => null,
            ];
        });

        Env::$app->bind('validatorCreated', function($event, Validator $validator){
            Env::$app->m->registerValidators($validator);
        });
    }


    public function getDirectory() {
        return __DIR__;
    }

}