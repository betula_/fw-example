<?php
namespace Fw\Fw;

class Env {

    /**
     * @var App
     */
    public static $app;
    private static $apiTable = array();
    private static $apiStack = array();

    public static function useApp($rootModuleName) {
        if (isset(self::$apiTable[$rootModuleName])) {
            self::$app->restoreEnv();
            array_push(self::$apiStack, self::$app);
            self::$app = self::$apiTable[$rootModuleName];
        } else {
            self::$app = new App($rootModuleName);
        }
        self::$app->configureEnv();
    }

    public static function dump() {
        call_user_func_array([self::$app->logger, 'dumpEnv'], func_get_args());
    }

    public static function json() {
        call_user_func_array([self::$app->logger, 'jsonEnv'], func_get_args());
    }

    public static function restoreApp() {
        self::$app = array_pop(self::$apiStack);
    }

}
