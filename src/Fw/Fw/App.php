<?php
namespace Fw\Fw;
use Exception;
use ErrorException;

/**
 * @property Logger $logger;
 */
class App {
    use EventDispatcherTrait;

    /**
     * @var int
     */
    public $timeCreate;

    /**
     * @var \Closure[]
     */
    public $serviceLoaders = array();

    /**
     * @var Module[]
     */
    public $modules = array();
    public $siteName = 'default';
    public $vaultName = false;
    public $dir;
    public $cfg;
    private $uidCounter = 0;
    private $oldExceptionHandler;

    public function __construct($moduleName) {
        Env::$app = $this;

        $this->timeCreate = microtime(true);
        $this->dir['src'] = substr(__DIR__, 0, strrpos(__DIR__, '/', strrpos(__DIR__, '/') - strlen(__DIR__) - 1 ));
        $this->dir['root'] = substr($this->dir['src'], 0, strrpos($this->dir['src'], '/'));

        Env::$app->cfg['logger']['buffered'] = true;

        $this->serviceLoaders['logger'] = function() {
            return new Logger(Env::$app->cfg['logger']['buffered']);
        };

        $this->createModules([$moduleName]);
        $this->modules = array_reverse($this->modules);
        foreach ($this->modules as $module) {
            $module->init();
        }
        if (!$this->vaultName) {
            throw new ConfigException('vaultName');
        }
        $this->dir['vault'] = $this->dir['root'].'/vault/'.$this->vaultName;
        $this->dir['var'] = $this->dir['vault'].'/var';
        $this->dir['res'] = $this->dir['vault'].'/www/res';
        $this->dir['data'] = $this->dir['vault'].'/data';

        $this->trigger('appCreated');
    }

    public function createModules(array $moduleNames) {
        $relatedModules = array();
        foreach ($moduleNames as $moduleName) {
            if (isset($this->modules[$moduleName])) {
                continue;
            }
            $className = 'Fw\\'.$moduleName;
            $this->modules[$moduleName] = new $className;
            $relatedModules = array_merge($relatedModules, array_reverse($this->modules[$moduleName]->getRelatedModules()) );
        }
        if ($relatedModules) {
            $this->createModules($relatedModules);
        }
    }

    public function configureEnv() {
        error_reporting(-1);
        $this->oldExceptionHandler = set_exception_handler([$this, 'exceptionHandler']);
        // $this->trigger('appConfigureEnv');
    }

    public function restoreEnv() {
        set_exception_handler($this->oldExceptionHandler);
        // $this->trigger('appRestoreEnv');
    }

    public function exceptionHandler(Exception $e) {
        $this->logger->ex($e);
        if (isset($_SERVER['SERVER_PROTOCOL'])) {
            header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Error');
            header("X-Error-Code: ".$e->getCode());
        }
    }

    public function __get($serviceName) {
        if (!array_key_exists($serviceName, $this->serviceLoaders)) {
            $trace = debug_backtrace();
            throw new ServiceNotFoundException($serviceName, $trace[0]['file'].':'.$trace[0]['line']);
        }
        $serviceLoader = $this->serviceLoaders[$serviceName];
        $service = $serviceLoader();
        $this->$serviceName = $service;
        $this->trigger($serviceName.'Created', $this->$serviceName);
        return $service;
    }

    public function createToken() {
        return str_replace(['/','+'], ['_','-'], base64_encode(hash('sha256', $this->createUid(), true).mt_rand(0,9)));
    }

    public function createUid() {
        $time = microtime(false);
        $sec  = substr($time, 11);
        $ms   = substr($time, 2, 6);
        $timestamp = (int)$sec*1000000+(int)$ms;
        $uid = dechex($timestamp);
        if (strlen($uid)<14) {
            $uid = '0'.$uid;
        }
        $pid = dechex(getmypid()%65536);
        while (strlen($pid)<4) {
            $pid = '0'.$pid;
        }
        $hex = $uid.$pid.dechex($this->uidCounter++%16);
        return $hex;
    }

}