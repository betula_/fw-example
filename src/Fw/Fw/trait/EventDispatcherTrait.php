<?php
namespace Fw\Fw;

trait EventDispatcherTrait {

    /**
     * @var \Closure[][]
     */
    private $eventListeners;

    public function trigger($event) {
        $args = func_get_args();
        if (isset($this->eventListeners['*'])) {
            foreach ($this->eventListeners['*'] as $callback) {
                call_user_func_array($callback, $args);
            }
        }
        if (isset($this->eventListeners[$event])) {
            foreach ($this->eventListeners[$event] as $callback) {
                call_user_func_array($callback, $args);
            }
        }
    }

    public function bind($event, $callback) {
        $this->eventListeners[$event][] = $callback;
    }

}