<?php
namespace Fw\Fw;
use RecursiveDirectoryIterator;
use FilesystemIterator;
use RecursiveIteratorIterator;

class ClassLoader {

    private $classMap;
    private $rootDir;
    private $mapFileName;
    private $autoRegenerate;

    public function __construct($rootModuleName, $autoRegenerate = false) {
        $this->rootDir = substr(__DIR__, 0, strrpos(__DIR__, '/'));
        $mapFileName = $this->rootDir.'/../../var/classMap_'.str_replace('\\', '_', $rootModuleName).'.php';
        $this->mapFileName = $mapFileName;
        $this->autoRegenerate = $autoRegenerate;
        $this->classMap = include $mapFileName;
        if (!$this->classMap) {
            $this->makeClassMap();
        }
        $this->register();
    }

    public function register() {
        spl_autoload_register([$this, 'loadClass']);
    }

    public function unregister() {
        spl_autoload_unregister([$this, 'loadClass']);
    }

    public function makeClassMap() {
        $search = new RecursiveDirectoryIterator($this->rootDir,
            FilesystemIterator::KEY_AS_PATHNAME|
            FilesystemIterator::FOLLOW_SYMLINKS|
            FilesystemIterator::SKIP_DOTS|
            FilesystemIterator::UNIX_PATHS
        );
        $iterator = new RecursiveIteratorIterator($search);
        $this->classMap = array();
        foreach ($iterator as $file => $info) {
            $name = substr($file, strlen($this->rootDir));
            if (preg_match('~/[A-Z][A-z\d]*\.php$~', $name)) {
                preg_match_all('~[^A-z\d]([A-Z][A-z\d]*)~', $name, $m);
                $className = 'Fw\\'.implode('\\', $m[1]);
//                    error_log($className.' => '.$file);
                $this->classMap[$className] = $file;
            }
        }
        error_log("\033[33mSaving class map (".count($this->classMap).') => '.$this->mapFileName."\033[0m");
        file_put_contents($this->mapFileName, '<?php return '.var_export($this->classMap,true).';');
    }

    public function loadClass($className) {
        if (isset($this->classMap[$className])) {
            $result = include $this->classMap[$className];
            if (!$result && $this->autoRegenerate) {
                $this->makeClassMap();
            }
            return true;
        }
        if ($this->autoRegenerate) {
            $this->makeClassMap();
            if (isset($this->classMap[$className])) {
                include $this->classMap[$className];
                return true;
            }
        }
        return false;
    }
}