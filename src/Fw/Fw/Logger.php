<?php
namespace Fw\Fw;
use ErrorException;
use Exception;

class Logger {

    private $colorTag;
    const FILENAME_LENGTH = 30;
    const ALARM = "\007";
    const COLOR_ERROR = "\007\033[31m";
    const COLOR_WARNING = "\033[33m";
    const COLOR_DUMP = "\033[36m";
    const COLOR_CLEAR = "\033[0m";

    private $buffered;
    private $buffer;
    public $level = 3;

    public function __construct($buffered = false) {
        $this->buffered = $buffered;
        $c = str_pad (base_convert(mt_rand(0,4095), 10, 8), 4, '0', STR_PAD_LEFT);
        $d = ($c[0]+3)%8;
        if ($buffered) {
            $this->buffer = '';
            $this->colorTag = '';
        } else {
            $this->colorTag = "\033[3{$c[0]};4{$c[1]}m{$c[1]}{$c[2]}\033[3{$c[2]};4{$c[3]}m{$c[2]}{$c[3]}\033[0m";
        }
        set_error_handler([$this, 'errorHandler'], -1);
    }

    public function __destruct() {
        if ($this->buffered) {
            error_log(Env::$app->siteName.$this->buffer);
        }
    }

    public function info($string, $trace = 1) {
        if ($this->level > 2) {
            $this->log($string, $trace);
        }
    }

    public function warn ($string, $trace = 1) {
        if ($this->level > 1) {
            $this->log(self::COLOR_WARNING.$string.self::COLOR_CLEAR, $trace);
        }
    }

    public function error ($string, $trace = 1) {
        if ($this->level > 0) {
           $this->log(self::ALARM.self::COLOR_ERROR.$string.self::COLOR_CLEAR, $trace);
        }
    }

    public function ex(Exception $e, $alarm=true) {
        if ($this->level > 0) {
            if ($e instanceof ErrorException) {
                $add = ' '.$this->errorSeverityToString($e->getSeverity());
            } else {
                $add = '';
            }
            $this->log(
                ($alarm?self::ALARM:'').self::COLOR_ERROR.get_class($e).$add.' ['.$e->getCode().']: '.$e->getMessage().self::COLOR_CLEAR,
                $e->getFile().':'.$e->getLine()
            );
        }
    }

    public function log($string, $trace = 0) {
        if (is_string($trace)) {
            $fileName = substr($trace, -self::FILENAME_LENGTH);
        } else {
            $backTrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, $trace+1);
            $fileName = substr($backTrace[$trace]['file'].':'.$backTrace[$trace]['line'], -self::FILENAME_LENGTH);
        }
        $toWrite = sprintf('%'.self::FILENAME_LENGTH.'s %s %7.1f %s', $fileName, $this->colorTag, (microtime(true)-Env::$app->timeCreate)*1000, $string);
        if ($this->buffered) {
            $this->buffer .= "\n".$toWrite;
        } else {
            error_log($toWrite);
        }
    }

    public function json() {
        $json = json_encode(func_get_args(), JSON_PRETTY_PRINT);
        $this->log("json\n".self::COLOR_DUMP.$json.self::COLOR_CLEAR, 1);
    }

    public function jsonEnv() {
        $json = json_encode(func_get_args(), JSON_PRETTY_PRINT);
        $this->log("json\n".self::COLOR_DUMP.$json.self::COLOR_CLEAR, 3);
    }

    public function dumpEnv() {
        $vars = func_get_args();
        ob_start();
        foreach ($vars as $var) {
            var_dump($var);
        }
        $this->log("Env::dump\n".self::COLOR_DUMP.ob_get_clean().self::COLOR_CLEAR, 3);
    }

    public function dump() {
        $vars = func_get_args();
        ob_start();
        foreach ($vars as $var) {
            var_dump($var);
        }
        $this->log("logger->dump\n".self::COLOR_DUMP.ob_get_clean().self::COLOR_CLEAR, 1);
    }

    public function errorSeverityToString($code) {
        $type = array (
            E_ERROR             => 'ERROR',
            E_WARNING           => 'WARNING',
            E_PARSE             => 'PARSING ERROR',
            E_NOTICE            => 'NOTICE',
            E_DEPRECATED        => 'DEPRECATED',
            E_STRICT            => 'STRICT',

            E_CORE_ERROR        => 'CORE ERROR',
            E_CORE_WARNING      => 'CORE WARNING',
            E_COMPILE_ERROR     => 'COMPILE ERROR',
            E_COMPILE_WARNING   => 'COMPILE WARNING',

            E_USER_ERROR        => 'ERROR`',
            E_USER_WARNING      => 'WARNING`',
            E_USER_NOTICE       => 'NOTICE`',
            E_USER_DEPRECATED   => 'DEPRECATED`',

            E_RECOVERABLE_ERROR => 'RECOVERABLE ERROR'
        );
        if (isset($type[$code])) return $type[$code];
        return "UNKNOWN:{$code}";
    }

    public function errorHandler($number, $string, $file, $line) {
        if($number == E_USER_ERROR || $number == E_RECOVERABLE_ERROR) {
            throw new ErrorException($string, 500, $number, $file, $line);
        }
        $this->error($this->errorSeverityToString($number).': '.$string, $file.':'.$line);
    }

}
