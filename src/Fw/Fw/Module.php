<?php
namespace Fw\Fw;

class Module {

    /**
     * @return string[] module names (first highest priority)
     */
    public function getRelatedModules() {
        return array();
    }

    public function init() {
    }

    /**
     * @return string This method MUST be override in each module
     */
    public function getDirectory() {
        return __DIR__;
    }

    public function getNamespace() {
        $className = get_class($this);
        return substr($className, 0, strrpos($className, '\\'));
    }

}