<?php
namespace Fw\Fw;
use Exception;

class ConfigException extends GeneralException {

    public function __construct($parameter, $code=500, Exception $prev=null) {
        parent::__construct('Incorrect config parameter: '.$parameter, (int)$code, $prev);
    }

}