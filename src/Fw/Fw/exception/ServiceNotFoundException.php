<?php
namespace Fw\Fw;
use Exception;

class ServiceNotFoundException extends GeneralException {
    public function __construct($serviceName, $call, $code=500, Exception $prev=null) {
        parent::__construct('Service not found: "'.$serviceName.'" in '.$call, $code, $prev);
    }
}