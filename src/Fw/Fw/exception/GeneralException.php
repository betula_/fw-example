<?php
namespace Fw\Fw;
use Exception;

class GeneralException extends Exception {
    public function __construct($message='Application error', $code=500, Exception $prev=null) {
        parent::__construct($message, (int) $code, $prev);
    }
}