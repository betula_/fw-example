<?php
require __DIR__.'/init.php';
$request = Fw\System\Request::createFromGlobals();
Fw\Fw\Env::$app->router->exec($request)->send();
