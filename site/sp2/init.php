<?php
namespace Fw\Fw;

require_once __DIR__ . '/../../src/Fw/Fw/ClassLoader.php';
new ClassLoader('Sp2\\Module', true);
Env::useApp('Sp2\\Module');
Env::$app->siteName = 'sp2';

include __DIR__.'/config.php';
include __DIR__.'/local/config.php';
